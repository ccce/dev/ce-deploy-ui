export default function configure() {
  // load the configuration variables into the window
  // these are copied from public/config.js
  // but copying is bad
  // maybe we need to rethink how we do config
  window.SERVER_ADDRESS = "";
  window.API_BASE_ENDPOINT = "/api/spec";
  window.TOKEN_RENEW_INTERVAL = 180000;
  window.NAMING_ADDRESS = "https://naming.esss.lu.se";
  window.ENVIRONMENT_TITLE = "";
}
