import { apiHandlers } from "../../src/mocks/mockAPI";

const glue = (requestHandler) => {
  return (req) => {
    try {
      const res = requestHandler(req);
      req.reply({
        body: res.body,
        statusCode: res.status ?? 200,
        headers: res.headers
      });
    } catch (err) {
      console.error("Error in requestHandler:", err);
      req.reply({
        body: { error: "Internal server error" },
        statusCode: 500
      });
    }
  };
};

export function interceptAPI() {
  apiHandlers.forEach((h) => {
    cy.intercept(h.method, h.matcher, glue(h.requestHandler)).as(h.name);
  });
}
