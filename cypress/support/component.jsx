// ***********************************************************
// This example support/component.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";

// Alternatively you can use CommonJS syntax:
// require("./commands")

import { mount } from "cypress/react";
import { interceptAPI } from "./interceptAPI";
import configure from "./configureWindowVars";
import { ReduxProvider } from "../../src/store/ReduxProvider";

Cypress.Commands.add("mount", (component, options = {}) => {
  const wrappedComponent = <ReduxProvider>{component}</ReduxProvider>;
  return mount(wrappedComponent, options);
});

// Example use:
// cy.mount(<MyComponent />)

// Configure window vars
configure();

// intercept API calls in component tests
beforeEach(() => {
  interceptAPI();
});
