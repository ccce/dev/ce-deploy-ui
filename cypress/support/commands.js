// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// Import React Testing Library commands (select by a11y role etc)
import "@testing-library/cypress/add-commands";
import { mount } from "cypress/react18";

// import custom auth commands
Cypress.Commands.add("login", () => {
  cy.setCookie("ce-deploy-auth", "DEADBEEF");
});

Cypress.Commands.add("mount", (component, options) => {
  return mount(component, options);
});
