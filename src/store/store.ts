import { configureStore } from "@reduxjs/toolkit";
import { deployApi } from "./deployApi";

export const store = configureStore({
  reducer: {
    // Add the generated reducer as a specific top-level slice
    [deployApi.reducerPath]: deployApi.reducer
  },
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(deployApi.middleware)
});
