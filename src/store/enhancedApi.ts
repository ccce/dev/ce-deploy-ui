import { deployApi } from "./deployApi";

const enhancedApiWithTags = deployApi.enhanceEndpoints({
  addTagTypes: [
    "IocDeployJob",
    "IocUndeployJob",
    "IocStartJob",
    "IocStopJob",
    "UpdateIOC",
    "UpdateActiveDeploymentHost",
    "UndeployInDB",
    "DeleteIOC"
  ],
  endpoints: {
    getIoc: {
      providesTags: [
        "IocDeployJob",
        "IocUndeployJob",
        "IocStartJob",
        "IocStopJob",
        "UpdateIOC",
        "UpdateActiveDeploymentHost",
        "UndeployInDB",
        "DeleteIOC"
      ]
    },
    listJobs: {
      providesTags: [
        "IocDeployJob",
        "IocUndeployJob",
        "IocStartJob",
        "IocStopJob"
      ]
    },
    iocDeployJob: {
      invalidatesTags: ["IocDeployJob"]
    },
    iocUndeployJob: {
      invalidatesTags: ["IocUndeployJob"]
    },
    iocStartJob: {
      invalidatesTags: ["IocStartJob"]
    },
    iocStopJob: {
      invalidatesTags: ["IocStopJob"]
    },
    updateIoc: { invalidatesTags: ["UpdateIOC"] },
    updateActiveDeploymentHost: {
      invalidatesTags: ["UpdateActiveDeploymentHost"]
    },
    unDeployInDb: { invalidatesTags: ["UndeployInDB"] },
    deleteIoc: { invalidatesTags: ["DeleteIOC"] }
  }
});

export const {
  useGetIocQuery,
  useListJobsQuery,
  useIocDeployJobMutation,
  useIocUndeployJobMutation,
  useIocStartJobMutation,
  useIocStopJobMutation,
  useUpdateIocMutation,
  useUpdateActiveDeploymentHostMutation,
  useUnDeployInDbMutation,
  useDeleteIocMutation
} = enhancedApiWithTags;
