import { emptySplitApi as api } from "./emptyApi";
const injectedRtkApi = api.injectEndpoints({
  endpoints: (build) => ({
    setMaintenanceMode: build.mutation<
      SetMaintenanceModeApiResponse,
      SetMaintenanceModeApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/maintenance_mode`,
        method: "PUT",
        body: queryArg.setMaintenance
      })
    }),
    iocUndeployJob: build.mutation<
      IocUndeployJobApiResponse,
      IocUndeployJobApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/jobs/undeploy/${queryArg.iocId}`,
        method: "POST"
      })
    }),
    iocStopJob: build.mutation<IocStopJobApiResponse, IocStopJobApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/jobs/stop/${queryArg.iocId}`,
        method: "POST"
      })
    }),
    iocStartJob: build.mutation<IocStartJobApiResponse, IocStartJobApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/jobs/start/${queryArg.iocId}`,
        method: "POST"
      })
    }),
    iocDeployJob: build.mutation<IocDeployJobApiResponse, IocDeployJobApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/jobs/deploy/${queryArg.iocId}`,
        method: "POST",
        body: queryArg.deployJobRequest
      })
    }),
    startBatchUndeployment: build.mutation<
      StartBatchUndeploymentApiResponse,
      StartBatchUndeploymentApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/jobs/batch/undeploy`,
        method: "POST",
        body: queryArg.batchUndeploymentRequest
      })
    }),
    startBatchDeployment: build.mutation<
      StartBatchDeploymentApiResponse,
      StartBatchDeploymentApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/jobs/batch/deploy`,
        method: "POST",
        body: queryArg.batchDeploymentRequest
      })
    }),
    listIocs: build.query<ListIocsApiResponse, ListIocsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/iocs`,
        params: {
          deployment_status: queryArg.deploymentStatus,
          created_by: queryArg.createdBy,
          network: queryArg.network,
          network_scope: queryArg.networkScope,
          host_type: queryArg.hostType,
          host_name: queryArg.hostName,
          query: queryArg.query,
          order_by: queryArg.orderBy,
          order_asc: queryArg.orderAsc,
          page: queryArg.page,
          limit: queryArg.limit,
          list_all: queryArg.listAll
        }
      })
    }),
    createIoc: build.mutation<CreateIocApiResponse, CreateIocApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/iocs`,
        method: "POST",
        body: queryArg.createIoc
      })
    }),
    updateJob: build.mutation<UpdateJobApiResponse, UpdateJobApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/awx/jobs`,
        method: "POST",
        body: queryArg.awxJobMeta
      })
    }),
    tokenRenew: build.mutation<TokenRenewApiResponse, TokenRenewApiArg>({
      query: () => ({ url: `/api/v1/authentication/renew`, method: "POST" })
    }),
    login: build.mutation<LoginApiResponse, LoginApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/authentication/login`,
        method: "POST",
        body: queryArg.login
      })
    }),
    getIoc: build.query<GetIocApiResponse, GetIocApiArg>({
      query: (queryArg) => ({ url: `/api/v1/iocs/${queryArg.iocId}` })
    }),
    deleteIoc: build.mutation<DeleteIocApiResponse, DeleteIocApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/iocs/${queryArg.iocId}`,
        method: "DELETE"
      })
    }),
    updateIoc: build.mutation<UpdateIocApiResponse, UpdateIocApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/iocs/${queryArg.iocId}`,
        method: "PATCH",
        body: queryArg.iocUpdateRequest
      })
    }),
    unDeployInDb: build.mutation<UnDeployInDbApiResponse, UnDeployInDbApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/iocs/${queryArg.iocId}/undeploy_in_db`,
        method: "PATCH"
      })
    }),
    updateActiveDeploymentHost: build.mutation<
      UpdateActiveDeploymentHostApiResponse,
      UpdateActiveDeploymentHostApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/iocs/${queryArg.iocId}/active_deployment/host`,
        method: "PATCH",
        body: queryArg.updateHostRequest
      })
    }),
    findAllRecords: build.query<
      FindAllRecordsApiResponse,
      FindAllRecordsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/records`,
        params: {
          text: queryArg.text,
          ioc_name: queryArg.iocName,
          pv_status: queryArg.pvStatus,
          page: queryArg.page,
          limit: queryArg.limit
        }
      })
    }),
    fetchRecordAlerts: build.query<
      FetchRecordAlertsApiResponse,
      FetchRecordAlertsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/records/${queryArg.recordName}/alerts`
      })
    }),
    getRecord: build.query<GetRecordApiResponse, GetRecordApiArg>({
      query: (queryArg) => ({ url: `/api/v1/records/${queryArg.name}` })
    }),
    fetchIocByName: build.query<
      FetchIocByNameApiResponse,
      FetchIocByNameApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/names/ioc_names_by_name`,
        params: { ioc_name: queryArg.iocName }
      })
    }),
    checkNamingUuId: build.query<
      CheckNamingUuIdApiResponse,
      CheckNamingUuIdApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/names/check_naming_uuid/${queryArg.namingUuid}`
      })
    }),
    hostDeviance: build.query<HostDevianceApiResponse, HostDevianceApiArg>({
      query: () => ({ url: `/api/v1/migration/deviance` })
    }),
    getCurrentMode: build.query<
      GetCurrentModeApiResponse,
      GetCurrentModeApiArg
    >({
      query: () => ({ url: `/api/v1/maintenance_mode/current` })
    }),
    endCurrentMaintenanceMode: build.mutation<
      EndCurrentMaintenanceModeApiResponse,
      EndCurrentMaintenanceModeApiArg
    >({
      query: () => ({
        url: `/api/v1/maintenance_mode/current`,
        method: "DELETE"
      })
    }),
    listJobs: build.query<ListJobsApiResponse, ListJobsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/jobs`,
        params: {
          ioc_id: queryArg.iocId,
          type: queryArg["type"],
          status: queryArg.status,
          user: queryArg.user,
          query: queryArg.query,
          start_date: queryArg.startDate,
          end_date: queryArg.endDate,
          time_zone: queryArg.timeZone,
          host_id: queryArg.hostId,
          order_by: queryArg.orderBy,
          order_asc: queryArg.orderAsc,
          page: queryArg.page,
          limit: queryArg.limit
        }
      })
    }),
    fetchJob: build.query<FetchJobApiResponse, FetchJobApiArg>({
      query: (queryArg) => ({ url: `/api/v1/jobs/${queryArg.jobId}` })
    }),
    fetchAwxJobStatus: build.query<
      FetchAwxJobStatusApiResponse,
      FetchAwxJobStatusApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/jobs/${queryArg.jobId}/awx/status`
      })
    }),
    fetchAwxJobLog: build.query<
      FetchAwxJobLogApiResponse,
      FetchAwxJobLogApiArg
    >({
      query: (queryArg) => ({ url: `/api/v1/jobs/${queryArg.jobId}/awx/log` })
    }),
    fetchIocStatus: build.query<
      FetchIocStatusApiResponse,
      FetchIocStatusApiArg
    >({
      query: (queryArg) => ({ url: `/api/v1/iocs/${queryArg.iocId}/status` })
    }),
    getIocDescription: build.query<
      GetIocDescriptionApiResponse,
      GetIocDescriptionApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/iocs/${queryArg.iocId}/description`
      })
    }),
    fetchIocAlerts: build.query<
      FetchIocAlertsApiResponse,
      FetchIocAlertsApiArg
    >({
      query: (queryArg) => ({ url: `/api/v1/iocs/${queryArg.iocId}/alerts` })
    }),
    fetchProcServLogLines: build.query<
      FetchProcServLogLinesApiResponse,
      FetchProcServLogLinesApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/iocs/${queryArg.hostName}/${queryArg.iocName}/log`,
        params: { time_range: queryArg.timeRange }
      })
    }),
    checkGitRepoId: build.query<
      CheckGitRepoIdApiResponse,
      CheckGitRepoIdApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/iocs/check_git_repo_id/${queryArg.repoId}`
      })
    }),
    fetchInventory: build.query<
      FetchInventoryApiResponse,
      FetchInventoryApiArg
    >({
      query: () => ({ url: `/api/v1/inventory` })
    }),
    listHosts: build.query<ListHostsApiResponse, ListHostsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/hosts`,
        params: {
          text: queryArg.text,
          filter: queryArg.filter,
          page: queryArg.page,
          limit: queryArg.limit,
          list_all: queryArg.listAll
        }
      })
    }),
    fetchSyslogLines: build.query<
      FetchSyslogLinesApiResponse,
      FetchSyslogLinesApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/hosts/${queryArg.hostName}/log`,
        params: { time_range: queryArg.timeRange }
      })
    }),
    findHostStatus: build.query<
      FindHostStatusApiResponse,
      FindHostStatusApiArg
    >({
      query: (queryArg) => ({ url: `/api/v1/hosts/${queryArg.hostId}/status` })
    }),
    findAssociatedIocsByHostId: build.query<
      FindAssociatedIocsByHostIdApiResponse,
      FindAssociatedIocsByHostIdApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/hosts/${queryArg.hostId}/iocs`,
        params: {
          ioc_name: queryArg.iocName,
          order_by: queryArg.orderBy,
          order_asc: queryArg.orderAsc,
          page: queryArg.page,
          limit: queryArg.limit
        }
      })
    }),
    findNetBoxHostByHostId: build.query<
      FindNetBoxHostByHostIdApiResponse,
      FindNetBoxHostByHostIdApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/hosts/${queryArg.hostId}/info_by_host_id`
      })
    }),
    checkHostExists: build.query<
      CheckHostExistsApiResponse,
      CheckHostExistsApiArg
    >({
      query: (queryArg) => ({ url: `/api/v1/hosts/${queryArg.hostId}/exists` })
    }),
    findHostAlerts: build.query<
      FindHostAlertsApiResponse,
      FindHostAlertsApiArg
    >({
      query: (queryArg) => ({ url: `/api/v1/hosts/${queryArg.hostId}/alerts` })
    }),
    findNetBoxHostByFqdn: build.query<
      FindNetBoxHostByFqdnApiResponse,
      FindNetBoxHostByFqdnApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/hosts/${queryArg.fqdn}/info_by_fqdn`
      })
    }),
    listTagsAndCommitIds: build.query<
      ListTagsAndCommitIdsApiResponse,
      ListTagsAndCommitIdsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/git_helper/${queryArg.projectId}/tags_and_commits`,
        params: {
          reference: queryArg.reference,
          search_method: queryArg.searchMethod
        }
      })
    }),
    gitReferenceType: build.query<
      GitReferenceTypeApiResponse,
      GitReferenceTypeApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/git_helper/${queryArg.projectId}/reference_type/${queryArg.gitReference}`
      })
    }),
    infoFromUserName: build.query<
      InfoFromUserNameApiResponse,
      InfoFromUserNameApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/git_helper/user_info`,
        params: { user_name: queryArg.userName }
      })
    }),
    listProjects: build.query<ListProjectsApiResponse, ListProjectsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/git_helper/projects`,
        params: { query: queryArg.query }
      })
    }),
    gitProjectDetails: build.query<
      GitProjectDetailsApiResponse,
      GitProjectDetailsApiArg
    >({
      query: (queryArg) => ({
        url: `/api/v1/git_helper/project_info/${queryArg.projectId}`
      })
    }),
    getToolUsers: build.query<GetToolUsersApiResponse, GetToolUsersApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/authentication/users`,
        params: { query: queryArg.query }
      })
    }),
    getUserRoles: build.query<GetUserRolesApiResponse, GetUserRolesApiArg>({
      query: () => ({ url: `/api/v1/authentication/roles` })
    }),
    removeIocs: build.mutation<RemoveIocsApiResponse, RemoveIocsApiArg>({
      query: (queryArg) => ({
        url: `/api/v1/migration/${queryArg.networkScopeToRemove}/remove_iocs`,
        method: "DELETE",
        params: { ioc_name_prefix: queryArg.iocNamePrefix }
      })
    }),
    logout: build.mutation<LogoutApiResponse, LogoutApiArg>({
      query: () => ({ url: `/api/v1/authentication/logout`, method: "DELETE" })
    })
  }),
  overrideExisting: false
});
export { injectedRtkApi as deployApi };
export type SetMaintenanceModeApiResponse =
  /** status 201 Maintenance Mode Changed */ MaintenanceMode;
export type SetMaintenanceModeApiArg = {
  setMaintenance: SetMaintenance;
};
export type IocUndeployJobApiResponse = /** status 201 IOC job initiated */ Job;
export type IocUndeployJobApiArg = {
  /** The ID of the IOC to start a job on */
  iocId: number;
};
export type IocStopJobApiResponse = /** status 201 IOC job initiated */ Job;
export type IocStopJobApiArg = {
  /** The ID of the IOC to start a job on */
  iocId: number;
};
export type IocStartJobApiResponse = /** status 201 IOC job initiated */ Job;
export type IocStartJobApiArg = {
  /** The ID of the IOC to start a job on */
  iocId: number;
};
export type IocDeployJobApiResponse = /** status 201 IOC job initiated */ Job;
export type IocDeployJobApiArg = {
  /** The ID of the IOC to start a job on */
  iocId: number;
  deployJobRequest: DeployJobRequest;
};
export type StartBatchUndeploymentApiResponse =
  /** status 201 IOC undeployment initiated */ Job;
export type StartBatchUndeploymentApiArg = {
  batchUndeploymentRequest: BatchUndeploymentRequest;
};
export type StartBatchDeploymentApiResponse =
  /** status 201 IOC job initiated */ Job;
export type StartBatchDeploymentApiArg = {
  batchDeploymentRequest: BatchDeploymentRequest;
};
export type ListIocsApiResponse =
  /** status 200 A paged array of IOCs */ PagedIocResponse;
export type ListIocsApiArg = {
  /** IOC's current deployment status */
  deploymentStatus?: "ALL" | "DEPLOYED" | "NOT_DEPLOYED";
  /** Search by user-name who created the ioc */
  createdBy?: string;
  /** Network */
  network?: string;
  /** Network scope */
  networkScope?: string;
  /** Host type */
  hostType?: string;
  /** Host name */
  hostName?: string;
  /** Search text (Search for Naming name) */
  query?: string;
  /** Order by */
  orderBy?: "ID" | "CREATED_BY" | "IOC_NAME";
  /** Order Ascending */
  orderAsc?: boolean;
  /** Page offset */
  page?: number;
  /** Page size */
  limit?: number;
  /** Use this parameter to list all IOCs in the response! Setting this parameter to true will override the Page and Limit parameters! */
  listAll?: boolean;
};
export type CreateIocApiResponse = /** status 201 IOC created */ Ioc;
export type CreateIocApiArg = {
  createIoc: CreateIoc;
};
export type UpdateJobApiResponse =
  /** status 200 Job updated */ GeneralException;
export type UpdateJobApiArg = {
  awxJobMeta: AwxJobMeta;
};
export type TokenRenewApiResponse = /** status 200 Ok */ LoginResponse;
export type TokenRenewApiArg = void;
export type LoginApiResponse = /** status 200 Ok */ LoginResponse;
export type LoginApiArg = {
  login: Login;
};
export type GetIocApiResponse = /** status 200 Found IOC */ IocDetails;
export type GetIocApiArg = {
  /** Unique ID of IOC */
  iocId: number;
};
export type DeleteIocApiResponse = /** status 204 IOC deleted */ string;
export type DeleteIocApiArg = {
  /** Id of IOC to delete */
  iocId: number;
};
export type UpdateIocApiResponse = /** status 200 IOC updated */ Ioc;
export type UpdateIocApiArg = {
  /** Unique ID of IOC */
  iocId: number;
  iocUpdateRequest: IocUpdateRequest;
};
export type UnDeployInDbApiResponse =
  /** status 200 Undeployment created */
  Deployment[] | /** status 204 No Content */ void;
export type UnDeployInDbApiArg = {
  /** The id of the IOC to undeploy */
  iocId: number;
};
export type UpdateActiveDeploymentHostApiResponse =
  /** status 200 Successful host update */ Ioc;
export type UpdateActiveDeploymentHostApiArg = {
  /** The ID of the IOC */
  iocId: number;
  updateHostRequest: UpdateHostRequest;
};
export type FindAllRecordsApiResponse =
  /** status 200 A paged array of Channel Finder records */ PagedRecordResponse;
export type FindAllRecordsApiArg = {
  /** Search text (Search for record name or description) */
  text?: string;
  /** Part of the IOC name that has to be searched */
  iocName?: string;
  /** PV status */
  pvStatus?: "ACTIVE" | "INACTIVE";
  /** Page offset */
  page?: number;
  /** Page size */
  limit?: string;
};
export type FetchRecordAlertsApiResponse =
  /** status 200 Alerts of the Record */ RecordAlertResponse;
export type FetchRecordAlertsApiArg = {
  /** Name of the Record */
  recordName: string;
};
export type GetRecordApiResponse = /** status 200 Found record */ RecordDetails;
export type GetRecordApiArg = {
  /** Unique (part of the) name of the record */
  name: string;
};
export type FetchIocByNameApiResponse =
  /** status 200 Naming names, and UUIDs from the Names service */ NameResponse[];
export type FetchIocByNameApiArg = {
  /** IOC name */
  iocName?: string;
};
export type CheckNamingUuIdApiResponse = /** status 200 Ok */ IdUsedByIoc;
export type CheckNamingUuIdApiArg = {
  /** Naming UUID */
  namingUuid: string;
};
export type HostDevianceApiResponse =
  /** status 200 Compares the stored host information about the Jobs against NetBox */ MigrationDeviance;
export type HostDevianceApiArg = void;
export type GetCurrentModeApiResponse =
  /** status 200 Found Mode */ MaintenanceMode;
export type GetCurrentModeApiArg = void;
export type EndCurrentMaintenanceModeApiResponse =
  /** status 204 Maintenance Mode ended */ void;
export type EndCurrentMaintenanceModeApiArg = void;
export type ListJobsApiResponse =
  /** status 200 A paged array of deployments */ PagedJobResponse;
export type ListJobsApiArg = {
  /** IOC ID */
  iocId?: number;
  /** Type */
  type?: "DEPLOYMENT" | "COMMAND";
  /** Status */
  status?:
    | "QUEUED"
    | "RUNNING"
    | "SUCCESSFUL"
    | "FAILED"
    | "FINISHED"
    | "ONGOING";
  /** User name */
  user?: string;
  /** Search text (Search for IOC name, Created by) */
  query?: string;
  /** Start date */
  startDate?: string;
  /** End date */
  endDate?: string;
  /** Time zone ID */
  timeZone?: string;
  /** Host ID for the host */
  hostId?: string;
  /** Order by */
  orderBy?: "USER" | "TIME" | "IOC_NAME" | "GIT_REFERENCE";
  /** Order Ascending */
  orderAsc?: boolean;
  /** Page offset */
  page?: number;
  /** Page size */
  limit?: number;
};
export type FetchJobApiResponse = /** status 200 Job details */ JobDetails;
export type FetchJobApiArg = {
  /** Unique ID of the job */
  jobId: number;
};
export type FetchAwxJobStatusApiResponse =
  /** status 200 Job details */ AwxJobDetails;
export type FetchAwxJobStatusApiArg = {
  /** Unique job ID */
  jobId: number;
};
export type FetchAwxJobLogApiResponse = /** status 200 Job log */ AwxJobLog;
export type FetchAwxJobLogApiArg = {
  /** Unique job ID */
  jobId: number;
};
export type FetchIocStatusApiResponse =
  /** status 200 Log lines */ IocStatusResponse;
export type FetchIocStatusApiArg = {
  /** Unique ID of the IOC */
  iocId: number;
};
export type GetIocDescriptionApiResponse =
  /** status 200 Found IOC description */ IocDescriptionResponse;
export type GetIocDescriptionApiArg = {
  /** Unique ID of IOC */
  iocId: number;
};
export type FetchIocAlertsApiResponse =
  /** status 200 Alerts of the IOC */ IocAlertResponse;
export type FetchIocAlertsApiArg = {
  /** Unique ID of the IOC */
  iocId: number;
};
export type FetchProcServLogLinesApiResponse =
  /** status 200 Log lines */ LokiResponse;
export type FetchProcServLogLinesApiArg = {
  /** Host name (without network part) */
  hostName: string;
  /** Name of the IOC to get procServ logs */
  iocName: string;
  /** Time range (in minutes) */
  timeRange?: number;
};
export type CheckGitRepoIdApiResponse = /** status 200 Ok */ IdUsedByIoc;
export type CheckGitRepoIdApiArg = {
  /** Git repository Id */
  repoId: number;
};
export type FetchInventoryApiResponse =
  /** status 200 Inventory response */ InventoryResponse;
export type FetchInventoryApiArg = void;
export type ListHostsApiResponse =
  /** status 200 A paged array of hosts */ PagedNetBoxHostResponse;
export type ListHostsApiArg = {
  /** Search text (Search for host name or description) */
  text?: string;
  /** Filtering depending on deployments */
  filter?: "ALL" | "IOCS_DEPLOYED" | "NO_IOCS" | "OWN";
  /** Page offset */
  page?: number;
  /** Page size */
  limit?: string;
  /** Use this parameter to list all Hosts in the response! Setting this parameter to true will override the Page and Limit parameters! */
  listAll?: boolean;
};
export type FetchSyslogLinesApiResponse =
  /** status 200 Log lines */ LokiResponse;
export type FetchSyslogLinesApiArg = {
  /** Host name (without network part) */
  hostName: string;
  /** Time range (in minutes) */
  timeRange?: number;
};
export type FindHostStatusApiResponse =
  /** status 200 Host status */ HostStatusResponse;
export type FindHostStatusApiArg = {
  /** Host identifier */
  hostId: string;
};
export type FindAssociatedIocsByHostIdApiResponse =
  /** status 200 Found Host */ PagedAssociatedIocs;
export type FindAssociatedIocsByHostIdApiArg = {
  /** Host identifier */
  hostId: string;
  /** IOC Name */
  iocName?: string;
  /** Order by */
  orderBy?: "ID" | "CREATED_BY" | "IOC_NAME";
  /** Order Ascending */
  orderAsc?: boolean;
  /** Page offset */
  page?: number;
  /** Page size */
  limit?: string;
};
export type FindNetBoxHostByHostIdApiResponse =
  /** status 200 Found Host */ HostInfoWithId;
export type FindNetBoxHostByHostIdApiArg = {
  /** The host's Host ID */
  hostId: string;
};
export type CheckHostExistsApiResponse =
  /** status 200 Hosts exists in NetBox */ void;
export type CheckHostExistsApiArg = {
  /** Host identifier */
  hostId: string;
};
export type FindHostAlertsApiResponse =
  /** status 200 Host alerts */ HostAlertResponse;
export type FindHostAlertsApiArg = {
  /** Host identifier */
  hostId: string;
};
export type FindNetBoxHostByFqdnApiResponse =
  /** status 200 Found Host */ HostInfoWithId;
export type FindNetBoxHostByFqdnApiArg = {
  /** The host's FQDN */
  fqdn: string;
};
export type ListTagsAndCommitIdsApiResponse =
  /** status 200 List of Tags and CommitIds for a specific GitLab repo */ GitReference[];
export type ListTagsAndCommitIdsApiArg = {
  /** Git repo project ID */
  projectId: number;
  /** Git reference */
  reference?: string;
  /** Search method - optional. Default value: filtering ref by EQUALS method */
  searchMethod?: "EQUALS" | "CONTAINS";
};
export type GitReferenceTypeApiResponse =
  /** status 200 Git reference type */ ReferenceTypeResponse;
export type GitReferenceTypeApiArg = {
  /** Git repo project ID */
  projectId: number;
  /** Git reference */
  gitReference: string;
};
export type InfoFromUserNameApiResponse =
  /** status 200 Information about the current user */ UserInfoResponse;
export type InfoFromUserNameApiArg = {
  /** The username to retrieve info from - optional. If no user name provided the backend will look up for currently logged in user name */
  userName?: string;
};
export type ListProjectsApiResponse =
  /** status 200 List of Gitlab projects of allowed groups */ GitProject[];
export type ListProjectsApiArg = {
  /** Search text (Search for Git repository name) */
  query?: string;
};
export type GitProjectDetailsApiResponse =
  /** status 200 Git project details */ GitProjectDto;
export type GitProjectDetailsApiArg = {
  /** Git repo project ID */
  projectId: number;
};
export type GetToolUsersApiResponse = /** status 200 Ok */ string[];
export type GetToolUsersApiArg = {
  /** User name query */
  query?: string;
};
export type GetUserRolesApiResponse = /** status 200 Ok */ string[];
export type GetUserRolesApiArg = void;
export type RemoveIocsApiResponse =
  /** status 204 Removes all IOCs that has been ever deployed to the network scope */ void;
export type RemoveIocsApiArg = {
  /** Removing all IOCs that has even been deployed to this network scope */
  networkScopeToRemove: string;
  /** Remove IOCs that's CURRENT name starts like the parameter AND was NEVER deployed
     -- leave empty if don't want to delete such IOCs

    This is independent from networkScope parameter!
     */
  iocNamePrefix?: string;
};
export type LogoutApiResponse = /** status 200 Ok */ object;
export type LogoutApiArg = void;
export type MaintenanceMode = {
  startAt?: string;
  endAt?: string;
  message?: string;
};
export type GeneralException = {
  error?: string;
  description?: string;
};
export type SetMaintenance = {
  startAt?: string;
  endAt?: string;
  message?: string;
};
export type DeploymentHostInfo = {
  hostId?: string;
  hostName?: string;
  network?: string;
  fqdn?: string;
  netBoxHostFromCache?: boolean;
};
export type JobEntry = {
  iocName?: string;
  iocId?: number;
  deploymentId?: number;
  gitProjectId?: number;
  gitReference?: string;
  host?: DeploymentHostInfo;
};
export type Job = {
  id?: number;
  createdBy?: string;
  startTime?: string;
  createdAt?: string;
  finishedAt?: string;
  action?:
    | "DEPLOY"
    | "UNDEPLOY"
    | "START"
    | "STOP"
    | "BATCH_DEPLOY"
    | "BATCH_UNDEPLOY";
  status?: "UNKNOWN" | "QUEUED" | "RUNNING" | "FAILED" | "SUCCESSFUL" | "ERROR";
  iocName?: string;
  iocId?: number;
  deploymentId?: number;
  gitProjectId?: number;
  gitReference?: string;
  host?: DeploymentHostInfo;
  jobs?: JobEntry[];
};
export type DeployJobRequest = {
  sourceVersion?: string;
  hostId?: string;
};
export type UndeploymentRequest = {
  iocId?: number;
  hostId?: string;
};
export type BatchUndeploymentRequest = {
  undeployments?: UndeploymentRequest[];
};
export type DeploymentRequest = {
  iocId: number;
  sourceVersion?: string;
  hostId: string;
};
export type BatchDeploymentRequest = {
  deployments: DeploymentRequest[];
};
export type ActiveDeployment = {
  host?: DeploymentHostInfo;
  sourceVersion?: string;
  sourceVersionShort?: string;
};
export type IocInfo = {
  id?: number;
  namingName?: string;
  createdBy?: string;
  gitProjectId?: number;
  deployedWithOldPlaybook?: boolean;
  activeDeployment?: ActiveDeployment;
};
export type PagedIocResponse = {
  totalCount?: number;
  listSize?: number;
  pageNumber?: number;
  limit?: number;
  iocList?: IocInfo[];
};
export type Host = {
  hostId?: string;
  externalIdValid?: boolean;
  fqdn?: string;
  hostName?: string;
  network?: string;
};
export type Deployment = {
  id?: number;
  createdBy?: string;
  startDate?: string;
  createdAt?: string;
  undeployment?: boolean;
  namingName?: string;
  gitProjectId?: number;
  sourceVersion?: string;
  jobId?: number;
  endDate?: string;
  iocName?: string;
  host?: Host;
  awxJobId?: number;
  sourceUrl?: string;
  sourceVersionShort?: string;
  operationStatus?:
    | "UNKNOWN"
    | "QUEUED"
    | "RUNNING"
    | "FAILED"
    | "SUCCESSFUL"
    | "ERROR";
};
export type Ioc = {
  id?: number;
  description?: string;
  createdBy?: string;
  namingName?: string;
  namingUuid?: string;
  gitProjectId?: number;
  sourceUrl?: string;
  activeDeployment?: Deployment;
  deployedWithOldPlaybook?: boolean;
};
export type CreateIoc = {
  namingUuid?: string;
  gitProjectId?: number;
  repository_name?: string;
};
export type AwxJobMeta = {
  id?: number;
  started?: string;
  finished?: string;
  status?:
    | "UNKNOWN"
    | "CREATED"
    | "QUEUED_BY_BACKEND"
    | "new"
    | "pending"
    | "waiting"
    | "running"
    | "failed"
    | "canceled"
    | "error"
    | "successful";
};
export type LoginResponse = {
  token?: string;
};
export type Login = {
  userName?: string;
  password?: string;
};
export type IocDetails = {
  id?: number;
  description?: string;
  createdBy?: string;
  namingName?: string;
  namingUuid?: string;
  gitProjectId?: number;
  sourceUrl?: string;
  activeDeployment?: Deployment;
  deployedWithOldPlaybook?: boolean;
  operationInProgress?: boolean;
};
export type IocUpdateRequest = {
  namingUuid?: string;
  gitProjectId?: number;
};
export type UpdateHostRequest = {
  hostId?: string;
};
export type Record = {
  name?: string;
  iocName?: string;
  iocId?: number;
  hostName?: string;
  pvStatus?: "ACTIVE" | "INACTIVE";
  iocVersion?: string;
  description?: string;
  recordType?: string;
  aliasFor?: string;
};
export type PagedRecordResponse = {
  totalCount?: number;
  listSize?: number;
  pageNumber?: number;
  limit?: number;
  recordList?: Record[];
};
export type Alert = {
  type?: "ERROR" | "WARNING" | "INFO";
  message?: string;
  link?: string;
};
export type RecordAlertResponse = {
  alertSeverity?: "ERROR" | "WARNING" | "INFO";
  alerts?: Alert[];
  recordName?: string;
};
export type RecordDetails = {
  name?: string;
  iocName?: string;
  iocId?: number;
  hostName?: string;
  pvStatus?: "ACTIVE" | "INACTIVE";
  iocVersion?: string;
  description?: string;
  recordType?: string;
  aliasFor?: string;
  properties?: {
    [key: string]: string;
  };
  hostId?: string;
  aliases?: string[];
};
export type NameResponse = {
  uuid?: string;
  name?: string;
  description?: string;
};
export type IdUsedByIoc = {
  iocId?: number;
};
export type MigrationDeviance = {
  not_found?: number;
  network_name_changed?: number;
  hostname_changed?: number;
  both_changed?: number;
  all_jobs?: number;
  unresolvable_iocs?: string[];
  network_scopes?: string[];
};
export type PagedJobResponse = {
  totalCount?: number;
  listSize?: number;
  pageNumber?: number;
  limit?: number;
  jobs?: Job[];
};
export type HostWithFqdn = {
  hostId?: string;
  externalIdValid?: boolean;
  fqdn?: string;
};
export type JobDetailsEntry = {
  iocName?: string;
  iocId?: number;
  deploymentId?: number;
  gitProjectId?: number;
  gitReference?: string;
  gitProjectUrl?: string;
  gitShortReference?: string;
  host?: HostWithFqdn;
};
export type JobDetails = {
  id?: number;
  createdBy?: string;
  startTime?: string;
  createdAt?: string;
  finishedAt?: string;
  action?:
    | "DEPLOY"
    | "UNDEPLOY"
    | "START"
    | "STOP"
    | "BATCH_DEPLOY"
    | "BATCH_UNDEPLOY";
  status?: "UNKNOWN" | "QUEUED" | "RUNNING" | "FAILED" | "SUCCESSFUL" | "ERROR";
  iocName?: string;
  iocId?: number;
  deploymentId?: number;
  gitProjectId?: number;
  gitReference?: string;
  gitProjectUrl?: string;
  gitShortReference?: string;
  host?: HostWithFqdn;
  awxJobId?: number;
  jobUrl?: string;
  alerts?: Alert[];
  jobs?: JobDetailsEntry[];
};
export type AwxJobDetails = {
  id?: number;
  status?: "UNKNOWN" | "QUEUED" | "RUNNING" | "FAILED" | "SUCCESSFUL" | "ERROR";
  started?: string;
  finished?: string;
};
export type AwxJobLog = {
  jobId?: number;
  stdoutHtml?: string;
  elapsed?: number;
};
export type IocStatusResponse = {
  iocId?: number;
  isActive?: boolean;
};
export type IocDescriptionResponse = {
  description?: string;
};
export type IocAlertResponse = {
  alertSeverity?: "ERROR" | "WARNING" | "INFO";
  alerts?: Alert[];
  iocId?: number;
};
export type LokiMessage = {
  logDate?: string;
  logMessage?: string;
};
export type LokiResponse = {
  lines?: LokiMessage[];
  warning?: string;
};
export type IocInventory = {
  name?: string;
  git?: string;
  version?: string;
};
export type IocInventoryList = {
  iocs?: IocInventory[];
};
export type MetaStructure = {
  hostvars?: {
    [key: string]: IocInventoryList;
  };
};
export type HostInventoryList = {
  hosts?: string[];
};
export type InventoryResponse = {
  _meta?: MetaStructure;
  all?: HostInventoryList;
};
export type HostInfoWithId = {
  id?: number;
  fqdn?: string;
  name?: string;
  scope?: string;
  description?: string;
  network?: string;
  deviceType?: string;
  tags?: string[];
  hostId?: string;
  vm?: boolean;
};
export type PagedNetBoxHostResponse = {
  totalCount?: number;
  listSize?: number;
  pageNumber?: number;
  limit?: number;
  netBoxHosts?: HostInfoWithId[];
};
export type HostStatusResponse = {
  hostId?: string;
  assigned?: boolean;
  status?: "AVAILABLE" | "UNREACHABLE";
};
export type PagedAssociatedIocs = {
  totalCount?: number;
  listSize?: number;
  pageNumber?: number;
  limit?: number;
  deployedIocs?: IocInfo[];
};
export type HostAlertResponse = {
  hostId?: string;
  alertSeverity?: "ERROR" | "WARNING" | "INFO";
  alerts?: Alert[];
};
export type GitReference = {
  reference?: string;
  description?: string;
  type?: "TAG" | "COMMIT" | "UNKNOWN";
  short_reference?: string;
  commit_date?: string;
};
export type ReferenceTypeResponse = {
  reference_type?: "TAG" | "COMMIT" | "UNKNOWN";
};
export type UserInfoResponse = {
  fullName?: string;
  loginName?: string;
  avatar?: string;
  email?: string;
  gitlabUserName?: string;
};
export type GitProject = {
  id?: number;
  url?: string;
};
export type GitProjectDto = {
  id?: number;
  name?: string;
  description?: string;
  projectUrl?: string;
};
export const {
  useSetMaintenanceModeMutation,
  useIocUndeployJobMutation,
  useIocStopJobMutation,
  useIocStartJobMutation,
  useIocDeployJobMutation,
  useStartBatchUndeploymentMutation,
  useStartBatchDeploymentMutation,
  useListIocsQuery,
  useLazyListIocsQuery,
  useCreateIocMutation,
  useUpdateJobMutation,
  useTokenRenewMutation,
  useLoginMutation,
  useGetIocQuery,
  useLazyGetIocQuery,
  useDeleteIocMutation,
  useUpdateIocMutation,
  useUnDeployInDbMutation,
  useUpdateActiveDeploymentHostMutation,
  useFindAllRecordsQuery,
  useLazyFindAllRecordsQuery,
  useFetchRecordAlertsQuery,
  useLazyFetchRecordAlertsQuery,
  useGetRecordQuery,
  useLazyGetRecordQuery,
  useFetchIocByNameQuery,
  useLazyFetchIocByNameQuery,
  useCheckNamingUuIdQuery,
  useLazyCheckNamingUuIdQuery,
  useHostDevianceQuery,
  useLazyHostDevianceQuery,
  useGetCurrentModeQuery,
  useLazyGetCurrentModeQuery,
  useEndCurrentMaintenanceModeMutation,
  useListJobsQuery,
  useLazyListJobsQuery,
  useFetchJobQuery,
  useLazyFetchJobQuery,
  useFetchAwxJobStatusQuery,
  useLazyFetchAwxJobStatusQuery,
  useFetchAwxJobLogQuery,
  useLazyFetchAwxJobLogQuery,
  useFetchIocStatusQuery,
  useLazyFetchIocStatusQuery,
  useGetIocDescriptionQuery,
  useLazyGetIocDescriptionQuery,
  useFetchIocAlertsQuery,
  useLazyFetchIocAlertsQuery,
  useFetchProcServLogLinesQuery,
  useLazyFetchProcServLogLinesQuery,
  useCheckGitRepoIdQuery,
  useLazyCheckGitRepoIdQuery,
  useFetchInventoryQuery,
  useLazyFetchInventoryQuery,
  useListHostsQuery,
  useLazyListHostsQuery,
  useFetchSyslogLinesQuery,
  useLazyFetchSyslogLinesQuery,
  useFindHostStatusQuery,
  useLazyFindHostStatusQuery,
  useFindAssociatedIocsByHostIdQuery,
  useLazyFindAssociatedIocsByHostIdQuery,
  useFindNetBoxHostByHostIdQuery,
  useLazyFindNetBoxHostByHostIdQuery,
  useCheckHostExistsQuery,
  useLazyCheckHostExistsQuery,
  useFindHostAlertsQuery,
  useLazyFindHostAlertsQuery,
  useFindNetBoxHostByFqdnQuery,
  useLazyFindNetBoxHostByFqdnQuery,
  useListTagsAndCommitIdsQuery,
  useLazyListTagsAndCommitIdsQuery,
  useGitReferenceTypeQuery,
  useLazyGitReferenceTypeQuery,
  useInfoFromUserNameQuery,
  useLazyInfoFromUserNameQuery,
  useListProjectsQuery,
  useLazyListProjectsQuery,
  useGitProjectDetailsQuery,
  useLazyGitProjectDetailsQuery,
  useGetToolUsersQuery,
  useLazyGetToolUsersQuery,
  useGetUserRolesQuery,
  useLazyGetUserRolesQuery,
  useRemoveIocsMutation,
  useLogoutMutation
} = injectedRtkApi;
