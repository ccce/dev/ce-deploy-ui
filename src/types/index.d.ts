export {};

declare global {
  interface Window {
    SERVER_ADDRESS: string;
    ENVIRONMENT_TITLE: string;
    API_BASE_ENDPOINT: string;
    TOKEN_RENEW_INTERVAL: number;
    FRONTEND_VERSION: string;
    SUPPORT_URL: string;
    NETBOX_ADDRESS: string;
    NAMING_ADDRESS: string;
  }
}
