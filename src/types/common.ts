import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { SerializedError } from "@reduxjs/toolkit";

// TODO: Add these types from common when that is implemented
export interface GlobalAppBarContext {
  title: string;
  setTitle: (title: string) => void;
  setButton: () => void;
}

export interface User {
  fullName: string;
  loginName: string;
  avatar: string;
  email: string | null;
  gitlabUserName: string;
  roles: string[];
}

export interface UserContext {
  user: User | null;
  userRoles: string[] | null;
  loginError: string | null;
  loginLoading: boolean | null;
  login: (userName: string, password: string) => void;
  logout: () => void;
  resetLoginError: () => void;
}

export interface OnPageParams {
  page: number;
  rows: number;
  rowsPerPageOptions?: number[];
}

export type ApiError = FetchBaseQueryError | SerializedError | undefined;

export interface ErrorMessage {
  error: string;
  description: string;
}

export interface ApiResult<T> {
  isLoading: boolean;
  error?: ApiError;
  data?: T;
}

export type FormElements<U extends string> = HTMLFormControlsCollection &
  Record<U, HTMLInputElement>;

export interface Pagination {
  page: number;
  rows: number;
  query?: string;
  orderAsc?: boolean;
  totalCount?: number;
  rowsPerPageOptions?: number[];
}

export interface DeployIocFormDefaults {
  name: string;
  description: string;
  git: string;
  gitProjectId?: number;
  reference?: string;
  short_reference?: string;
  netBoxHost?: {
    fqdn: string;
    hostId: string;
  };
}
