import { ComponentProps } from "react";
import BatchUndeployIconSvg from "./resources/batch/batch_undeploy_icon.svg?react";
import { BatchUndeployIcon } from "../components/Job/JobIcons";

export function BatchUndeploySymbol(
  props: ComponentProps<typeof BatchUndeployIcon>
) {
  return <BatchUndeployIconSvg {...props} />;
}
