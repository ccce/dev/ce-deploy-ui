import CcceControlSymbolGray from "./resources/control/ccce-control-symbol_757575.svg?react";

export function CCCEControlSymbol(props: { width: string; height: string }) {
  return <CcceControlSymbolGray {...props} />;
}
