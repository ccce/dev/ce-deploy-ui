import { ComponentProps } from "react";
import BatchDeployIconSvg from "./resources/batch/batch_deploy_icon.svg?react";
import { BatchDeployIcon } from "../components/Job/JobIcons";

export function BatchDeploySymbol(
  props: ComponentProps<typeof BatchDeployIcon>
) {
  return <BatchDeployIconSvg {...props} />;
}
