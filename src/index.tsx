import { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import { LicenseInfo } from "@mui/x-license-pro";
import { AppErrorBoundary } from "@ess-ics/ce-ui-common";
import { env } from "./config/env";
import App from "./App";

// Set license key
LicenseInfo.setLicenseKey(env.VITE_APP_MUI_PRO_LICENSE_KEY ?? "");

// Render app
const root = createRoot(document.getElementById("root") as HTMLElement);

root.render(
  <StrictMode>
    <AppErrorBoundary supportHref={window.SUPPORT_URL}>
      <App />
    </AppErrorBoundary>
  </StrictMode>
);
