import { createTheme } from "@mui/material/styles";
import { theme as ceuiTheme } from "@ess-ics/ce-ui-common";

declare module "@mui/material/styles" {
  interface Theme {
    palette: {
      status: {
        ok: {
          main: string;
        };
        progress: {
          main: string;
        };
        fail: {
          main: string;
        };
        disabled: {
          main: string;
        };
        icons: string;
      };
      error: {
        main: string;
      };
      warning: {
        main: string;
      };
      primary: {
        inconsistency: string;
        hoverBackground: string;
        selectionBackground: string;
        selectionFont: string;
        borderColor: string;
      };
      primaryContrastText: { main: string };
    };
  }
}

// add customizations
export const theme = createTheme(ceuiTheme, {
  components: {
    // TODO: Move to common theme?
    MuiAlert: {
      styleOverrides: {
        standardInfo: {
          backgroundColor: ceuiTheme.palette.grey[200]
        }
      }
    }
  },
  palette: {
    status: {
      ...ceuiTheme.palette.status,
      icons: ceuiTheme.palette.grey[800]
    }
  }
});
