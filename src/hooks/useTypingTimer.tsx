import { useState, KeyboardEvent } from "react";

interface UseTypingTimerReturn {
  value: string;
  onKeyUp: (event: KeyboardEvent<HTMLInputElement>) => void;
}

export function useTypingTimer({
  init = "",
  interval = 750
} = {}): UseTypingTimerReturn {
  const [typingTimer, setTypingTimer] = useState<number | undefined>(undefined);
  const [value, setValue] = useState(init);
  const doneTypingInterval = interval; // ms

  const onKeyUp = (event: KeyboardEvent<HTMLInputElement>): void => {
    const target = event.target as HTMLInputElement;
    const { key } = event;
    clearTimeout(typingTimer);
    if (key === "Enter") {
      setValue(target.value);
      return;
    } else {
      const timer = setTimeout(
        () => setValue(target.value),
        doneTypingInterval
      );
      setTypingTimer(timer);
    }
  };

  return { value, onKeyUp };
}
