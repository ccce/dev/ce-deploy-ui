import { useCallback } from "react";
// import { useHistory } from "react-router-dom";
import { useNavigate } from "react-router-dom";

export function useRedirect() {
  const navigate = useNavigate();
  // const history = useHistory();
  const redirect = useCallback(
    (destination: string, state: object, replace: boolean = false) => {
      /* const method = replace ? history.replace : history.push;
    method(destination, { ...state, from: history.location.pathname });*/
      navigate(destination, { replace: replace, state: state });
    },
    [/* history.location.pathname, history.push, history.replace*/ navigate]
  );
  return redirect;
}
