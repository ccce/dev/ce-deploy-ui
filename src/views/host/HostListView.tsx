import { useState, useEffect, useCallback, useContext } from "react";
import { Container, Grid, Tabs, Tab } from "@mui/material";
import {
  GlobalAppBarContext,
  RootPaper,
  usePagination,
  SearchBar
} from "@ess-ics/ce-ui-common";
import { useSearchParams } from "react-router-dom";
import { HostTable } from "../../components/host/HostTable";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { initRequestParams } from "../../api/initRequestParams";
import { ROWS_PER_PAGE } from "../../constants";
import { ListHostsApiArg, useLazyListHostsQuery } from "../../store/deployApi";
import {
  GlobalAppBarContext as GlobalAppBarContextType,
  OnPageParams
} from "../../types/common";

export function HostListView() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("IOC hosts")), [setTitle]);

  const [callListHostsQuery, { data: hosts, isFetching }] =
    useLazyListHostsQuery();

  const [searchParams, setSearchParams] = useSearchParams({ query: "" });
  const [tabIndex, setTabIndex] = useState(0);
  const [hostFilter, setHostFilter] =
    useState<ListHostsApiArg["filter"]>("ALL");

  const handleTabChange = (tab: number) => {
    if (tab === 0) {
      setHostFilter("ALL");
    } else if (tab === 1) {
      setHostFilter("IOCS_DEPLOYED");
    } else if (tab === 2) {
      setHostFilter("NO_IOCS");
    }
    setTabIndex(tab);
  };

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: hosts?.totalCount ?? 0 });
  }, [setPagination, hosts?.totalCount]);

  // Request new search results whenever search or pagination changes
  useEffect(() => {
    const requestParams = initRequestParams({ pagination });
    callListHostsQuery({
      filter: hostFilter,
      text: searchParams.get("query") || "",
      limit: requestParams.limit.toString(),
      page: requestParams.page
    });
  }, [callListHostsQuery, hostFilter, searchParams, pagination]);

  // Callback for searchbar, called whenever user updates search
  const setSearch = useCallback(
    (query: string) => {
      setSearchParams({ query }, { replace: true });
    },
    [setSearchParams]
  );

  // Invoked by Table on change to pagination
  const onPage = (params: OnPageParams) => {
    setPagination(params);
  };

  const content = (
    <SearchBar
      search={setSearch}
      query={searchParams.get("query")}
      loading={isFetching}
    >
      <HostTable
        hosts={hosts?.netBoxHosts ?? []}
        loading={isFetching || !hosts}
        pagination={pagination}
        onPage={onPage}
      />
    </SearchBar>
  );

  return (
    <RootPaper>
      <Grid
        container
        spacing={1}
      >
        <Grid
          container
          spacing={1}
          component={Container}
          justifyContent="space-between"
          alignItems="center"
          style={{ display: "flex" }}
        >
          <Grid item>
            <Tabs
              value={tabIndex}
              onChange={(_, tab) => handleTabChange(tab)}
            >
              <Tab label="All" />
              <Tab label="Only hosts with IOCs" />
              <Tab label="Only hosts without IOCs" />
            </Tabs>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {content}
        </Grid>
      </Grid>
    </RootPaper>
  );
}
