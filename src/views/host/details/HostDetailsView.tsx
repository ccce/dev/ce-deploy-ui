import { useEffect, useContext, useState, ChangeEvent } from "react";
import { Box, IconButton, Typography, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
  KeyValueTable,
  SimpleAccordion,
  GlobalAppBarContext,
  AlertBannerList,
  ExternalLink
} from "@ess-ics/ce-ui-common";
import { useNavigate } from "react-router-dom";
import { HostDetailsTable } from "./HostDetailsTable";
import { HostJobsSection } from "./HostJobsSection";
import { HostIocSection } from "./HostIocSection";
import { LokiPanel } from "../../../components/common/Loki/LokiPanel";
import { applicationTitle } from "../../../components/navigation/applicationTitle";
import { AccessControl } from "../../../components/auth/AccessControl";
import { HostStatus } from "../../../components/host/HostStatus";
import { env } from "../../../config/env";
import { HostAlertResponse, HostInfoWithId } from "../../../store/deployApi";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../../types/common";

interface HostDetailsViewProps {
  hostId: string;
  host: HostInfoWithId;
  alert: HostAlertResponse;
}

export function HostDetailsView({ hostId, host, alert }: HostDetailsViewProps) {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  const [accordionState, setAccordionState] = useState({
    detailsOpen: false,
    logStreamOpen: false
  });

  useEffect(() => {
    if (host && host.name) {
      setTitle(applicationTitle("Host Details: " + host.name));
    }
  }, [host, setTitle]);

  const navigate = useNavigate();

  return (
    <Stack gap={2}>
      <Box>
        <IconButton
          color="inherit"
          onClick={() => navigate(-1)}
          size="large"
        >
          <ArrowBackIcon />
        </IconButton>
      </Box>
      {host && (
        <>
          <AlertBannerList alerts={alert.alerts ?? []} />
          <Stack
            flexDirection="row"
            alignItems="center"
            justifyContent="flex-start"
          >
            <Box
              sx={{
                marginRight: (theme) => theme.spacing(4),
                marginLeft: (theme) => theme.spacing(6)
              }}
            >
              <HostStatus
                hostId={hostId}
                hideAlerts
              />
            </Box>
            <Box sx={{ flex: 1 }}>
              <Typography
                noWrap
                variant="subtitle1"
                component="p"
                color="textPrimary"
              >
                {host.fqdn}
              </Typography>
              <Typography
                noWrap
                component="p"
                variant="subtitle2"
              >
                {host.network || "---"}
              </Typography>
            </Box>
          </Stack>
          <Stack gap={2}>
            <HostIocSection hostId={hostId} />
          </Stack>
          <HostJobsSection hostId={hostId} />

          <AccessControl
            allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
            renderNoAccess={() => <></>}
          >
            <SimpleAccordion
              summary={
                <Typography
                  variant="h3"
                  component="h2"
                >
                  Host details
                </Typography>
              }
              expanded={accordionState.detailsOpen}
              onChange={(_: ChangeEvent, expanded: boolean) => {
                setAccordionState((prevState) => ({
                  ...prevState,
                  detailsOpen: expanded
                }));
              }}
            >
              <HostDetailsTable host={host} />
            </SimpleAccordion>

            <SimpleAccordion
              summary={
                <Typography
                  variant="h3"
                  component="h2"
                >
                  Host log stream
                </Typography>
              }
              expanded={accordionState.logStreamOpen}
              onChange={(_: ChangeEvent, expanded: boolean) => {
                setAccordionState((prevState) => ({
                  ...prevState,
                  logStreamOpen: expanded
                }));
              }}
            >
              <LokiPanel
                hostName={host.name}
                isSyslog
                isExpanded={accordionState.logStreamOpen}
              />
            </SimpleAccordion>
          </AccessControl>
          <KeyValueTable
            obj={{
              "Host Configuration": (
                <ExternalLink
                  href={
                    host.vm
                      ? `${env.NETBOX_ADDRESS}/virtualization/virtual-machines/${host.id}`
                      : `${env.NETBOX_ADDRESS}/dcim/devices/${host.id}`
                  }
                  aria-label="Host Configuration"
                >
                  {" "}
                  {host.vm
                    ? `${env.NETBOX_ADDRESS}/virtualization/virtual-machines/${host.id}`
                    : `${env.NETBOX_ADDRESS}/dcim/devices/${host.id}`}
                </ExternalLink>
              ),
              "Host Metrics": (
                <ExternalLink
                  href={`https://grafana.tn.esss.lu.se/d/5zJT23xWz/node-exporter-full?orgId=1&var-node=${host.fqdn}`}
                  aria-label="Host Metrics"
                >
                  {`https://grafana.tn.esss.lu.se/d/5zJT23xWz/node-exporter-full?orgId=1&var-node=${host.fqdn}`}
                </ExternalLink>
              )
            }}
            variant="overline"
          />
        </>
      )}
    </Stack>
  );
}
