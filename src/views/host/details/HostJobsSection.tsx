import { useEffect, useMemo, useCallback, useState, ChangeEvent } from "react";
import { string } from "prop-types";
import { SimpleAccordion, usePagination } from "@ess-ics/ce-ui-common";
import { Typography } from "@mui/material";
import { JobTable } from "../../../components/Job";
import { ROWS_PER_PAGE } from "../../../constants";
import { useLazyListJobsQuery } from "../../../store/deployApi";
import { ApiAlertError } from "../../../components/common/Alerts/ApiAlertError";
import { OnPageParams } from "../../../types/common";

const propTypes = {
  hostId: string.isRequired
};

export const HostJobsSection = ({ hostId }: { hostId: string }) => {
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });
  const [expanded, setExpanded] = useState(false);

  const [getHostLog, { data: hostLog, error, isLoading }] =
    useLazyListJobsQuery();

  const params = useMemo(
    () => ({ hostId, ...pagination }),
    [hostId, pagination]
  );

  useEffect(() => {
    if (expanded) {
      getHostLog(params);
    }
  }, [expanded, pagination, getHostLog, params]);

  const onPage = useCallback(
    (params: OnPageParams) => {
      setPagination(params);
    },
    [setPagination]
  );

  useEffect(() => {
    setPagination({ totalCount: hostLog?.totalCount ?? 0 });
  }, [setPagination, hostLog]);

  return (
    <SimpleAccordion
      expanded={expanded}
      summary={
        <Typography
          variant="h3"
          component="h2"
        >
          Job log
        </Typography>
      }
      onChange={(_: ChangeEvent, expanded: boolean) => setExpanded(expanded)}
    >
      {error && <ApiAlertError error={error} />}
      {hostLog ? (
        <JobTable
          jobs={!error && hostLog ? hostLog?.jobs : undefined}
          customColumns={[
            { field: "status" },
            { field: "job" },
            { field: "user" }
          ]}
          loading={isLoading || !hostLog}
          pagination={pagination}
          onPage={onPage}
        />
      ) : null}
    </SimpleAccordion>
  );
};

HostJobsSection.propTypes = propTypes;
