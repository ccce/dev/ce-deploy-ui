import { useParams } from "react-router-dom";
import { HostDetailsContainer } from "./HostDetailsContainer";
import { AccessControl } from "../../../components/auth/AccessControl";

export function HostDetailsAccessControl() {
  const { id } = useParams();

  return (
    <AccessControl allowedRoles={[]}>
      <HostDetailsContainer hostId={id} />
    </AccessControl>
  );
}
