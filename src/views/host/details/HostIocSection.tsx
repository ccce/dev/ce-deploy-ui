import { useEffect, useCallback } from "react";
import { string } from "prop-types";
import { Typography } from "@mui/material";
import { usePagination } from "@ess-ics/ce-ui-common";
import { IOCTable } from "../../../components/IOC/IOCTable";
import { initRequestParams } from "../../../api/initRequestParams";
import { ROWS_PER_PAGE } from "../../../constants";
import { useLazyFindAssociatedIocsByHostIdQuery } from "../../../store/deployApi";
import { OnPageParams } from "../../../types/common";

const propTypes = {
  hostId: string
};

export const HostIocSection = ({ hostId }: { hostId: string }) => {
  const { pagination, setPagination, setTotalCount } = usePagination({
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  const [callGetIocs, { data: iocs, isLoading }] =
    useLazyFindAssociatedIocsByHostIdQuery();

  const onPage = useCallback(
    (params: OnPageParams) => {
      setPagination(params);
    },
    [setPagination]
  );

  const getIocs = useCallback(() => {
    const requestParams = initRequestParams({ pagination });
    callGetIocs({
      hostId,
      limit: requestParams.limit.toString(),
      page: requestParams.page
    });
  }, [callGetIocs, hostId, pagination]);

  // update pagination whenever search result total pages change
  useEffect(() => {
    setTotalCount(iocs?.totalCount ?? 0);
  }, [setTotalCount, iocs?.totalCount]);

  useEffect(() => {
    getIocs();
  }, [getIocs]);

  return (
    <>
      <Typography
        variant="h3"
        component="h2"
      >
        IOCs
      </Typography>
      <IOCTable
        iocs={iocs?.deployedIocs || []}
        loading={isLoading || !iocs}
        rowType="host"
        pagination={pagination}
        onPage={onPage}
      />
    </>
  );
};

HostIocSection.propTypes = propTypes;
