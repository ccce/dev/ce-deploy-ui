import { LinearProgress } from "@mui/material";
import { RootPaper } from "@ess-ics/ce-ui-common";
import { HostDetailsView } from "./HostDetailsView";
import { NotFoundView } from "../../../components/navigation/NotFoundView/NotFoundView";
import {
  useFindHostAlertsQuery,
  useFindNetBoxHostByHostIdQuery
} from "../../../store/deployApi";
import { getErrorState } from "../../../components/common/Alerts/AlertsData";
import { isNotFound } from "../../../components/navigation/NotFoundView/isNotFound";

interface HostDetailsContainerProps {
  hostId?: string;
}

export function HostDetailsContainer({ hostId }: HostDetailsContainerProps) {
  const { data: host, error: fetchError } = useFindNetBoxHostByHostIdQuery(
    {
      hostId: hostId ?? ""
    },
    { skip: !hostId }
  );
  const { data: alert, error: alertError } = useFindHostAlertsQuery(
    {
      hostId: hostId ?? ""
    },
    { skip: !hostId }
  );

  const { status, message } = getErrorState(fetchError || alertError);

  if (isNotFound(status)) {
    return (
      <NotFoundView
        message={message}
        status={status}
      />
    );
  }

  if (!hostId || !host || !alert) {
    return (
      <RootPaper>
        <LinearProgress color="primary" />
      </RootPaper>
    );
  }

  return (
    <RootPaper data-testid="host-details-container">
      <HostDetailsView
        host={host}
        alert={alert}
        hostId={hostId}
      />
    </RootPaper>
  );
}
