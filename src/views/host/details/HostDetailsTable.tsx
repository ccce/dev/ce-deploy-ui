import { KeyValueTable, EmptyValue } from "@ess-ics/ce-ui-common";
import { Chip, Stack } from "@mui/material";
import { HostInfoWithId } from "../../../store/deployApi";

const Tags = ({ tags }: { tags: string[] }) => (
  <Stack
    direction="row"
    gap={1.5}
    flexWrap="wrap"
  >
    {tags.map((tag) => (
      <Chip
        key={tag}
        label={tag}
        sx={{ minWidth: "50px" }}
      />
    ))}
  </Stack>
);

const getTableData = (host: HostInfoWithId) => ({
  "device type": host.vm ? "Virtual machine" : host.deviceType,
  description: host.description ? host.description : <EmptyValue />,
  scope: host.scope,
  tags: host.tags ? <Tags tags={host.tags} /> : <EmptyValue />
});

export const HostDetailsTable = ({ host }: { host: HostInfoWithId }) => (
  <KeyValueTable
    obj={getTableData(host)}
    variant="overline"
  />
);
