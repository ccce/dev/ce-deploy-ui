import { useEffect, useCallback, useContext } from "react";
import { IconButton, Typography, LinearProgress, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {
  RootPaper,
  KeyValueTable,
  GlobalAppBarContext,
  InternalLink,
  formatDateAndTime,
  EmptyValue,
  AlertBannerList
} from "@ess-ics/ce-ui-common";
import { useParams, useNavigate } from "react-router-dom";
import { Alias } from "./Alias";
import { RecordBadge } from "../../components/records/RecordBadge";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { NotFoundView } from "../../components/navigation/NotFoundView/NotFoundView";

import {
  RecordDetails,
  useFetchRecordAlertsQuery,
  useGetRecordQuery
} from "../../store/deployApi";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";
import { getErrorState } from "../../components/common/Alerts/AlertsData";
import { isNotFound } from "../../components/navigation/NotFoundView/isNotFound";

const LAST_UPDATED = "Last updated";
const RECORD_TYPE = "Record type";
const DESCRIPTION = "Description";
const IOC_REVISION = "IOC Revision";

export function RecordDetailsView() {
  const { name } = useParams();
  const decodedName = decodeURIComponent(name ?? "");
  const encodedName = encodeURIComponent(decodedName);

  const {
    data: record,
    error: fetchError,
    isLoading: recordLoading
  } = useGetRecordQuery({
    name: encodedName
  });

  const { data: alert } = useFetchRecordAlertsQuery(
    {
      recordName: encodedName
    },
    { skip: !encodedName }
  );
  const navigate = useNavigate();

  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  useEffect(() => {
    if (record) {
      setTitle(applicationTitle(`Record Details: ${record.name}`));
    }
  }, [setTitle, record]);

  const handleClick = () => {
    navigate(-1);
  };

  const getSubset = useCallback((record: RecordDetails) => {
    const subset = {
      "Alias for": (
        <Alias aliases={record?.aliasFor ? [record?.aliasFor] : []} />
      ),
      Aliases: <Alias aliases={record?.aliases} />,
      [DESCRIPTION]: record?.description || <EmptyValue />,
      Version: record?.iocVersion || <EmptyValue />,
      [RECORD_TYPE]: record.recordType || <EmptyValue />,
      Host: record.hostId ? (
        <Typography>
          <InternalLink
            to={`/hosts/${record.hostId}`}
            label={`Host details, ${record?.hostName}`}
          >
            {record?.hostName}
          </InternalLink>
        </Typography>
      ) : (
        record?.hostName
      ),
      [LAST_UPDATED]: "",
      [IOC_REVISION]: ""
    };

    if (record?.properties) {
      for (const [key, value] of Object.entries(record.properties)) {
        if (key.toLowerCase().includes("time")) {
          subset[LAST_UPDATED] = formatDateAndTime(value);
        } else if (key.toLowerCase().includes("recordtype")) {
          subset[RECORD_TYPE] = value;
        } else if (key.toLowerCase().includes("recorddesc")) {
          subset[DESCRIPTION] = value;
        } else if (key.toLowerCase() === "iocversion") {
          subset[IOC_REVISION] = value;
        }
      }
      return subset;
    }
  }, []);

  const { status } = getErrorState(fetchError);

  if (decodedName.includes(";") || isNotFound(status)) {
    return <NotFoundView />;
  }

  if (recordLoading || !record) {
    return (
      <RootPaper>
        <LinearProgress color="primary" />
      </RootPaper>
    );
  }

  return (
    <RootPaper>
      <IconButton
        color="inherit"
        onClick={handleClick}
        size="large"
      >
        <ArrowBackIcon />
      </IconButton>
      {record && (
        <Stack spacing={2}>
          <AlertBannerList alerts={alert?.alerts ?? []} />
          <RecordBadge record={record} />
          <KeyValueTable
            obj={getSubset(record)}
            variant="overline"
            sx={{ border: 0 }}
            valueOptions={{ headerName: "" }}
          />
        </Stack>
      )}
    </RootPaper>
  );
}
