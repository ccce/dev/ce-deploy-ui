import { EmptyValue, InternalLink, useUniqueKeys } from "@ess-ics/ce-ui-common";
import { Stack } from "@mui/material";

export const Alias = ({ aliases }: { aliases?: string[] }) => {
  const itemsKeys = useUniqueKeys(aliases);

  if (!aliases || aliases.length === 0) {
    return <EmptyValue />;
  }

  return (
    <Stack gap={0.5}>
      {aliases.map((alias, index) => (
        <InternalLink
          key={itemsKeys[index]}
          to={`/records/${encodeURIComponent(alias)}`}
          label={alias}
        >
          {alias}
        </InternalLink>
      ))}
    </Stack>
  );
};
