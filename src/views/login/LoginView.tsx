import { Card, CardContent, Grid } from "@mui/material";
import { useContext, useEffect } from "react";
import { useLocation } from "react-router-dom";
import {
  GlobalAppBarContext,
  userContext,
  LoginForm
} from "@ess-ics/ce-ui-common";
import { useRedirect } from "../../hooks/Redirect";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import {
  GlobalAppBarContext as GlobalAppBarContextType,
  UserContext
} from "../../types/common";

export function LoginView() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);

  useEffect(() => setTitle(applicationTitle("Login")), [setTitle]);

  const redirect = useRedirect();

  const { user, login, loginError, loginLoading } =
    useContext<UserContext>(userContext);
  const location = useLocation();

  useEffect(() => {
    if (user) {
      const goTo = location.state?.from ?? "/iocs";
      redirect(goTo, {}, true);
    }
  }, [location.state?.from, redirect, user]);

  return (
    <Grid
      container
      spacing={0}
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: "80vh" }}
    >
      <Grid
        item
        xs={8}
        md={5}
        xl={3}
      >
        <Card>
          <CardContent>
            <LoginForm
              login={login}
              error={loginError}
              loading={loginLoading}
            />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}
