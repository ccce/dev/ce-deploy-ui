import { useContext, useEffect } from "react";
import { Grid } from "@mui/material";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { UserProfile } from "../../components/common/User/UserProfile";
import { UserOperationList } from "../../components/common/User/UserOperationList";
import { UserInfoResponse } from "../../store/deployApi";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

interface UserPageViewProps {
  userName?: string;
  userInfo: UserInfoResponse;
}

export function UserPageView({ userName, userInfo }: UserPageViewProps) {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle(userName)), [setTitle, userName]);

  return (
    <Grid
      container
      spacing={1}
    >
      <Grid
        item
        xs={12}
      >
        <UserProfile userInfo={userInfo} />
      </Grid>
      <Grid
        item
        xs={12}
      >
        <UserOperationList userName={userName} />
      </Grid>
    </Grid>
  );
}
