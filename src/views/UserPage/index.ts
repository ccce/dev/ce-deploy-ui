import { UserPageView } from "./UserPageView";
import { UserDetailsAccessControl } from "./UserDetailsAccessControl";
import { UserDetailsContainer } from "./UserDetailsContainer";

export { UserPageView, UserDetailsAccessControl, UserDetailsContainer };
export default UserPageView;
