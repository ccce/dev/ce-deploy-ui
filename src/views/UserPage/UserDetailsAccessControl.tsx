import { UserDetailsContainer } from "./UserDetailsContainer";
import { AccessControl } from "../../components/auth/AccessControl";

export function UserDetailsAccessControl() {
  return (
    <AccessControl allowedRoles={[]}>
      <UserDetailsContainer />
    </AccessControl>
  );
}
