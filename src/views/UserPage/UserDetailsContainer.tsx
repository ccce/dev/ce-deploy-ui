import { useContext, useEffect, useState } from "react";
import { LinearProgress } from "@mui/material";
import { useParams } from "react-router-dom";
import { userContext, RootPaper } from "@ess-ics/ce-ui-common";
import { UserPageView } from "./UserPageView";
import { NotFoundView } from "../../components/navigation/NotFoundView/NotFoundView";
import { useInfoFromUserNameQuery } from "../../store/deployApi";
import { UserContext } from "../../types/common";
import { getErrorState } from "../../components/common/Alerts/AlertsData";

export function UserDetailsContainer() {
  const { userName } = useParams();
  const { user } = useContext<UserContext>(userContext);
  const [error, setError] = useState<
    { message: string; status: string } | undefined
  >(undefined);

  const {
    data: userInfo,
    isLoading,
    error: userInfoResponseError
  } = useInfoFromUserNameQuery({ userName });

  useEffect(() => {
    if (userInfoResponseError) {
      const { status } = getErrorState(userInfoResponseError);
      if (status === 404) {
        setError({ message: "Page not found", status: `${status}` });
      }

      // user doesn't have permission to fetch userInfo
      if (status === 401) {
        setError({ message: "Unauthorized", status: `${status}` });
      }
    }
  }, [userInfoResponseError]);

  // If there is an error, show it; highest priority
  if (error) {
    return (
      <NotFoundView
        status={error?.status ?? "404"}
        message={error?.message}
      />
    );
  }

  if (isLoading || !userInfo) {
    return (
      <RootPaper>
        <LinearProgress color="primary" />
      </RootPaper>
    );
  }

  // If the user is logged in and the userInfo is available
  // Then show the user page
  if (user && userInfo) {
    return (
      <RootPaper>
        <UserPageView
          userName={userName}
          userInfo={userInfo}
        />
      </RootPaper>
    );
  }
}
