import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { useContext, useEffect } from "react";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { CreateIOC } from "../../components/IOC/CreateIOC";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export const CreateIOCView = () => {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Create IOC")), [setTitle]);

  return <CreateIOC />;
};
