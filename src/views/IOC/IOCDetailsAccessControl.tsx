import { useParams } from "react-router-dom";
import { IOCDetailsContainer } from "./IOCDetailsContainer";
import { AccessControl } from "../../components/auth/AccessControl";

export function IOCDetailsAccessControl() {
  const { id } = useParams();

  return (
    <AccessControl allowedRoles={[]}>
      <IOCDetailsContainer id={id} />
    </AccessControl>
  );
}
