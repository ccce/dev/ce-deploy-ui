import { useState, useEffect, useCallback } from "react";
import {
  useGlobalAppBarContext,
  RootPaper,
  usePagination,
  SearchBar
} from "@ess-ics/ce-ui-common";
import { useSearchParams } from "react-router-dom";
import { Container, Grid, Tabs, Tab } from "@mui/material";
import { useLazyListIocsQuery, ListIocsApiArg } from "../../store/deployApi";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { initRequestParams } from "../../api/initRequestParams";
import { GlobalAppBarContext, OnPageParams } from "../../types/common";
import { ApiAlertError } from "../../components/common/Alerts/ApiAlertError";
import { IOCTable } from "../../components/IOC/IOCTable";
import { ROWS_PER_PAGE } from "../../constants";

export const IOCListView = () => {
  const [deploymentStatus, setDeploymentStatus] =
    useState<ListIocsApiArg["deploymentStatus"]>("ALL");
  const [tabIndex, setTabIndex] = useState(0);
  const [listIocs, { data: iocs, isFetching, error }] = useLazyListIocsQuery();
  const [searchParams, setSearchParams] = useSearchParams({ query: "" });
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();

  const handleTabChange = (tab: number) => {
    if (tab === 0) {
      setDeploymentStatus("ALL");
    } else if (tab === 1) {
      setDeploymentStatus("DEPLOYED");
    } else if (tab === 2) {
      setDeploymentStatus("NOT_DEPLOYED");
    }
    setTabIndex(tab);
  };

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  // Invoked by Table on change to pagination
  const onPage = (params: OnPageParams) => {
    setPagination(params);
  };

  // Callback for searchbar, called whenever user updates search
  const setSearch = useCallback(
    (query: string) => {
      setSearchParams({ query }, { replace: true });
    },
    [setSearchParams]
  );

  useEffect(() => setTitle(applicationTitle("IOCs")), [setTitle]);

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: iocs?.totalCount ?? 0 });
  }, [setPagination, iocs?.totalCount]);

  useEffect(() => {
    const requestParams = Object.assign(
      {},
      initRequestParams({
        pagination,
        filter: searchParams.get("query") || ""
      }),
      { deploymentStatus: deploymentStatus }
    );

    listIocs(requestParams);
  }, [listIocs, deploymentStatus, searchParams, pagination]);

  return (
    <RootPaper>
      <Grid
        container
        spacing={1}
      >
        <Grid
          container
          spacing={1}
          component={Container}
          justifyContent="space-between"
          alignItems="center"
          style={{ display: "flex" }}
        >
          <Grid item>
            <Tabs
              value={tabIndex}
              onChange={(_, tab) => handleTabChange(tab)}
            >
              <Tab label="All" />
              <Tab label="Only Deployed" />
              <Tab label="Only Not Deployed" />
            </Tabs>
          </Grid>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          <SearchBar
            search={setSearch}
            query={searchParams.get("query")}
            loading={isFetching || !iocs}
          >
            {error ? (
              <ApiAlertError error={error} />
            ) : (
              <IOCTable
                iocs={iocs?.iocList || []}
                loading={isFetching || !iocs}
                rowType="explore"
                pagination={pagination}
                onPage={onPage}
              />
            )}
          </SearchBar>
        </Grid>
      </Grid>
    </RootPaper>
  );
};
