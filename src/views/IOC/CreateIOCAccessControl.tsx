import { AccessControl } from "@ess-ics/ce-ui-common";
import { Navigate } from "react-router-dom";
import { CreateIOCView } from "./CreateIOCView";

export const CreateIOCAccessControl = () => {
  return (
    <AccessControl
      allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
      renderNoAccess={() => (
        <Navigate
          to="/"
          replace
        />
      )}
    >
      <CreateIOCView />
    </AccessControl>
  );
};
