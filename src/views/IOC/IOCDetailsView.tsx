import { ReactNode, useEffect, useState } from "react";
import { Grid, IconButton, Stack } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useNavigate } from "react-router-dom";
import {
  useGlobalAppBarContext,
  useIsCurrentUserPermitted,
  TabPanel
} from "@ess-ics/ce-ui-common";
import { IOCLiveStatus } from "../../components/IOC/IOCLiveStatus";
import { IOCManage } from "../../components/IOC/IOCManage";
import { IOCAdmin } from "../../components/IOC/IOCAdmin";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { GlobalAppBarContext } from "../../types/common";
import { IocDetails } from "../../store/deployApi";

interface IOCDetailsViewProps {
  ioc: IocDetails;
}

interface Tab {
  label: string;
  content: ReactNode;
}

const getTabs = (ioc: IocDetails): Tab[] => [
  {
    label: "Status",
    content: <IOCLiveStatus ioc={ioc} />
  },
  {
    label: "Management",
    content: <IOCManage ioc={ioc} />
  },
  {
    label: "Admin",
    content: <IOCAdmin ioc={ioc} />
  }
];

export const IOCDetailsView = ({ ioc }: IOCDetailsViewProps) => {
  const [tabIndex, setTabIndex] = useState(0);
  const [tabs, setTabs] = useState<Tab[] | null>(null);
  const { setTitle }: GlobalAppBarContext = useGlobalAppBarContext();
  const navigate = useNavigate();

  const isPermittedAdmin = useIsCurrentUserPermitted({
    allowedRoles: ["DeploymentToolAdmin"]
  });

  const isPermittedManagement = useIsCurrentUserPermitted({
    allowedRoles: ["DeploymentToolAdmin", "DeploymentToolIntegrator"]
  });

  useEffect(() => {
    if (ioc) {
      setTitle(applicationTitle(`IOC Details: ${ioc.namingName}`));
    }
  }, [ioc, setTitle]);

  useEffect(() => {
    if (ioc) {
      if (isPermittedAdmin) {
        setTabs(getTabs(ioc));
      } else if (isPermittedManagement) {
        setTabs(getTabs(ioc).filter((tab) => tab.label !== "Admin"));
      } else {
        setTabs(getTabs(ioc).filter((tab) => tab.label === "Status"));
      }
    }
  }, [ioc, isPermittedAdmin, isPermittedManagement]);

  return (
    <Grid
      container
      spacing={1}
    >
      <Grid
        item
        xs={12}
        style={{ paddingBottom: 0 }}
      >
        {tabs && (
          <TabPanel
            tabs={tabs}
            initialIndex={tabIndex}
            onTabChange={(index: number) => setTabIndex(index)}
            TabsProps={{ centered: true, sx: { flex: 1 } }}
            renderTabs={(tabs: ReactNode) => (
              <Stack
                flexDirection="row"
                justifyContent="space-between"
              >
                <IconButton
                  color="inherit"
                  onClick={() => navigate(-1)}
                  size="large"
                >
                  <ArrowBackIcon />
                </IconButton>
                {tabs}
              </Stack>
            )}
          />
        )}
      </Grid>
    </Grid>
  );
};
