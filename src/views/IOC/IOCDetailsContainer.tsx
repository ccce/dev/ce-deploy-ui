import { LinearProgress } from "@mui/material";
import { RootPaper } from "@ess-ics/ce-ui-common";

import { IOCDetailsView } from "./IOCDetailsView";
import { NotFoundView } from "../../components/navigation/NotFoundView/NotFoundView";
import { useGetIocQuery } from "../../store/enhancedApi";
import { ApiAlertError } from "../../components/common/Alerts/ApiAlertError";
import { getErrorState } from "../../components/common/Alerts/AlertsData";
import { isNotFound } from "../../components/navigation/NotFoundView/isNotFound";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../constants";

interface IOCDetailsContainerProps {
  id?: string;
}

export function IOCDetailsContainer({ id }: IOCDetailsContainerProps) {
  const {
    data: ioc,
    isLoading,
    error
  } = useGetIocQuery(
    { iocId: Number(id) },
    { skip: !id, pollingInterval: DEFAULT_POLLING_INTERVAL_MILLIS }
  );

  const { status } = getErrorState(error);

  if (isNotFound(status)) {
    return <NotFoundView />;
  }

  if (error) {
    return <ApiAlertError error={error} />;
  }

  if (isLoading || !ioc) {
    return (
      <RootPaper>
        <LinearProgress color="primary" />
      </RootPaper>
    );
  }

  return (
    <RootPaper>
      {ioc ? <IOCDetailsView ioc={ioc} /> : <LinearProgress color="primary" />}
    </RootPaper>
  );
}
