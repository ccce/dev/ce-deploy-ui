import { useContext, useEffect } from "react";
import { RootPaper, Help, GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { Stack, Typography } from "@mui/material";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { env } from "../../config/env";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export function HelpView() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Help")), [setTitle]);

  return (
    <RootPaper>
      <Help
        summary={
          <Stack gap={1.5}>
            <Typography>
              CE Deploy & Monitor is the deployment and monitoring parts of the
              IOC toolchain in the Controls Ecosystem.{" "}
            </Typography>
            <Typography>
              It is a tool that manages IOCs on host machines, using Ansible to
              perform the necessary configuration management. This tool will set
              up IOC hosts and deploy IOCs in a consistent manner, and will
              configure all necessary services needed for this purpose. It also
              integrates with various other systems to enable monitoring of IOCs
              and hosts, and further enables some limited remote execution
              features.
            </Typography>
            <Typography fontStyle="italic">
              Be aware that the tool does{" "}
              <Typography
                component="span"
                fontWeight="bold"
              >
                not
              </Typography>{" "}
              set up your host (and OS) - this needs to be done in NetBox.
            </Typography>
          </Stack>
        }
        docsHref="https://confluence.esss.lu.se/x/CVGQFg"
        supportHref={env.SUPPORT_URL}
        version={env.FRONTEND_VERSION}
        apiHref="/api/swagger-ui/index.html"
      />
    </RootPaper>
  );
}
