import { useState, useEffect } from "react";
import { LinearProgress } from "@mui/material";
import { RootPaper } from "@ess-ics/ce-ui-common";
import { JobDetailsView } from "./JobDetailsView";
import { NotFoundView } from "../../components/navigation/NotFoundView/NotFoundView";
import { useFetchJobQuery } from "../../store/deployApi";
import { getErrorState } from "../../components/common/Alerts/AlertsData";
import { isNotFound } from "../../components/navigation/NotFoundView/isNotFound";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../constants";

export function JobDetailsContainer({ id }: { id?: string }) {
  const [jobFinished, setJobFinished] = useState(false);

  const {
    data: job,
    isLoading,
    error: jobError
  } = useFetchJobQuery(
    { jobId: Number(id) },
    {
      skip: !id,
      pollingInterval: !jobFinished ? DEFAULT_POLLING_INTERVAL_MILLIS : 0
    }
  );

  useEffect(() => {
    if (job?.finishedAt) {
      setJobFinished(true);
    }
  }, [job?.finishedAt]);

  const { status, message } = getErrorState(jobError);

  if (isNotFound(status)) {
    return (
      <NotFoundView
        message={message}
        status={status}
      />
    );
  }

  if (isLoading || !job) {
    return (
      <RootPaper>
        <LinearProgress color="primary" />
      </RootPaper>
    );
  }

  return (
    <RootPaper>
      <JobDetailsView job={job} />
    </RootPaper>
  );
}
