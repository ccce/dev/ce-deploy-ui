import { useCallback, useEffect } from "react";
import { Box } from "@mui/material";
import { RootPaper, usePagination } from "@ess-ics/ce-ui-common";
import { initRequestParams } from "../../api/initRequestParams";
import { JobTable } from "../../components/Job/JobTable";
import { ROWS_PER_PAGE } from "../../constants";
import { useLazyListJobsQuery } from "../../store/deployApi";
import { OnPageParams } from "../../types/common";

export function JobListView() {
  const [getJobs, { data: jobs, isLoading }] = useLazyListJobsQuery();

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: jobs?.totalCount ?? 0 });
  }, [setPagination, jobs?.totalCount]);

  const callGetOperations = useCallback(() => {
    const requestParams = initRequestParams({ pagination });
    getJobs(requestParams);
  }, [getJobs, pagination]);

  // Request new search results whenever search or pagination changes
  useEffect(() => {
    callGetOperations();
  }, [callGetOperations, pagination]);

  // Invoked by Table on change to pagination
  const onPage = (params: OnPageParams) => {
    setPagination(params);
  };

  return (
    <RootPaper>
      <Box paddingY={2}>
        <JobTable
          jobs={jobs?.jobs}
          pagination={pagination}
          onPage={onPage}
          loading={isLoading || !jobs}
        />
      </Box>
    </RootPaper>
  );
}
