import { useContext, useEffect } from "react";
import { IconButton } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { useNavigate } from "react-router-dom";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { JobsDetails } from "../../components/Job/JobDetails";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";
import { JobDetails } from "../../store/deployApi";

export function JobDetailsView({ job }: { job: JobDetails }) {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(-1);
  };

  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(
    () => job && setTitle(applicationTitle(`Job Details: #${job.id}`)),
    [setTitle, job]
  );

  return (
    <>
      <IconButton
        color="inherit"
        onClick={handleClick}
        size="large"
      >
        <ArrowBackIcon />
      </IconButton>
      <JobsDetails jobDetail={job} />
    </>
  );
}
