import { useParams } from "react-router-dom";
import { JobDetailsContainer } from "./JobDetailsContainer";
import { AccessControl } from "../../components/auth/AccessControl";

export function JobDetailsAccessControl() {
  const { id } = useParams();

  return (
    <AccessControl allowedRoles={[]}>
      <JobDetailsContainer id={id} />
    </AccessControl>
  );
}
