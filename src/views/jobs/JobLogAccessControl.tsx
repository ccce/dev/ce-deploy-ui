import { useContext, useEffect } from "react";
import { GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { JobListView } from "./JobListView";
import { AccessControl } from "../../components/auth/AccessControl";
import { applicationTitle } from "../../components/navigation/applicationTitle";
import { GlobalAppBarContext as GlobalAppBarContextType } from "../../types/common";

export function JobLogAccessControl() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle("Log")), [setTitle]);

  return (
    <AccessControl allowedRoles={[]}>
      <JobListView />
    </AccessControl>
  );
}
