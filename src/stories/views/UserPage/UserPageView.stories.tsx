import { UserDetailsContainer } from "../../../views/UserPage";
import { AppHarness } from "../../../mocks/AppHarness";
import { userImpersonatorArgs } from "../../utils/common-args";
import { User } from "../../../types/common";

export default {
  title: "Views/UserPage/UserPageView"
};

interface Args {
  user: User;
  userRoles: string[];
}

const Template = (args: Args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <UserDetailsContainer />
  </AppHarness>
);

export const Default = (args: Args) => <Template {...args} />;
Default.args = {
  ...userImpersonatorArgs
};
