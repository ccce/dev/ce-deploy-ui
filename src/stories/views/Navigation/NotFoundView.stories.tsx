import { AppHarness } from "../../../mocks/AppHarness";
import { NotFoundView } from "../../../components/navigation/NotFoundView";

export default {
  title: "Views/Navigation/NotFoundView"
};

const Template = () => (
  <AppHarness>
    <NotFoundView />
  </AppHarness>
);

export const Default = () => <Template />;
