import { AppHarness } from "../../../mocks/AppHarness";
import { LoginView } from "../../../views/login/LoginView";

export default {
  title: "Views/Login/LoginView"
};

export const Default = () => (
  <AppHarness
    initialHistory={["/login"]}
    useTestUser
  >
    <LoginView />
  </AppHarness>
);
