import { http, delay, HttpResponse } from "msw";
import { AppHarness } from "../../../mocks/AppHarness";
import { CreateIOCView } from "../../../views/IOC/CreateIOCView";
import { handlers } from "../../../mocks/handlers";
import { userImpersonatorArgs } from "../../utils/common-args";
import general_exception from "../../../mocks/fixtures/GeneralException.json";
import { User } from "../../../types/common";

export default {
  title: "Views/IOC/CreateIOCView"
};

interface Args {
  user: User;
  userRoles: string[];
}

const Template = (args: Args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <CreateIOCView />
  </AppHarness>
);

export const Default = (args: Args) => <Template {...args} />;
Default.args = {
  ...userImpersonatorArgs
};

export const CreateLoading = (args: Args) => <Template {...args} />;
CreateLoading.args = {
  ...Default.args
};
CreateLoading.parameters = {
  msw: {
    handlers: [
      http.post("*/iocs", async () => await delay("infinite")),
      ...handlers
    ]
  }
};

export const CreateWithError = (args: Args) => <Template {...args} />;
CreateWithError.args = {
  ...Default.args
};
CreateWithError.parameters = {
  msw: {
    handlers: [
      http.post("*/iocs", () =>
        HttpResponse.json({ message: general_exception }, { status: 400 })
      ),
      ...handlers
    ]
  }
};
