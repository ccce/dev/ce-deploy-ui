import { AppHarness } from "../../../mocks/AppHarness";
import { IOCDetailsContainer } from "../../../views/IOC/IOCDetailsContainer";
import { userImpersonatorArgs } from "../../utils/common-args";

export default {
  title: "Views/IOC/IOCDetailsView"
};

interface Args {
  id: string;
}

const Template = (args: Args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <IOCDetailsContainer id={args.id} />
  </AppHarness>
);

const config = {
  id: 1,
  ...userImpersonatorArgs
};

export const NotDeployed = (args: Args) => <Template {...args} />;
NotDeployed.args = {
  ...config,
  id: "18"
};

export const Deployed = (args: Args) => <Template {...args} />;
Deployed.args = {
  ...config,
  id: "14"
};

export const DeployedErrors = (args: Args) => <Template {...args} />;
DeployedErrors.args = {
  ...config,
  id: "39"
};
