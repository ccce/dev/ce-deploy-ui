import { http, delay } from "msw";
import { AppHarness } from "../../../mocks/AppHarness";
import { handlers } from "../../../mocks/handlers";
import { IOCListView } from "../../../views/IOC/IOCListView";

export default {
  title: "Views/IOC/IocListView"
};

const Template = () => (
  <AppHarness>
    <IOCListView />
  </AppHarness>
);

export const Default = () => <Template />;

export const LoadingAsyncCells = () => <Template />;
LoadingAsyncCells.parameters = {
  msw: {
    handlers: [
      http.get("*/iocs/*", async () => await delay("infinite")),
      ...handlers
    ]
  }
};
