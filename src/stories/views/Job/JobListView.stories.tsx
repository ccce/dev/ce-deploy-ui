import { AppHarness } from "../../../mocks/AppHarness";
import { JobListView } from "../../../views/jobs/JobListView";

export default {
  title: "Views/Job/JobListView"
};

const Template = () => (
  <AppHarness>
    <JobListView />
  </AppHarness>
);

export const Default = () => <Template />;
