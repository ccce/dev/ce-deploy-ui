import { JobDetailsView } from "../../../views/jobs/JobDetailsView";
import { AppHarness } from "../../../mocks/AppHarness";
import jobs from "../../../mocks/fixtures/Jobs.json";
import { JobDetails } from "../../../store/deployApi";

const STATUS_OPTIONS = ["SUCCESSFUL", "FAILED", "QUEUED", "RUNNING", "UNKNOWN"];
const JOB_ACTION_OPTIONS = [
  "DEPLOY",
  "UNDEPLOY",
  "BATCH_DEPLOY",
  "BATCH_UNDEPLOY",
  "START",
  "STOP"
];

export default {
  title: "Views/Job/JobDetailsView",
  argTypes: {
    actionType: {
      options: JOB_ACTION_OPTIONS,
      control: { type: "radio" }
    },
    status: {
      options: STATUS_OPTIONS,
      control: { type: "radio" }
    }
  }
};

interface Args {
  actionType: JobDetails["action"];
  status: JobDetails["status"];
}

const Template = (args: Args) => {
  const currentJob = jobs.jobs.find((job) => job.action === args.actionType);
  return (
    <AppHarness>
      <JobDetailsView
        job={Object.assign({}, currentJob, {
          action: args.actionType,
          status: args.status
        })}
      />
    </AppHarness>
  );
};

export const Default = (args: Args) => <Template {...args} />;

Default.args = {
  actionType: JOB_ACTION_OPTIONS[0],
  status: STATUS_OPTIONS[0]
};
