import { AppHarness } from "../../../mocks/AppHarness";
import { User } from "../../../types/common";
import { HostDetailsContainer } from "../../../views/host/details/HostDetailsContainer";
import { userImpersonatorArgs } from "../../utils/common-args";

export default {
  title: "Views/Host/HostDetailsView"
};

interface Args {
  user: User;
  userRoles: string[];
  hostId: string;
}

const Template = (args: Args) => (
  <AppHarness
    useTestUser
    {...args}
  >
    <HostDetailsContainer hostId={args.hostId} />
  </AppHarness>
);

export const Active = (args: Args) => <Template {...args} />;
Active.args = {
  ...userImpersonatorArgs,
  hostId: "MjE5MV9mYWxzZQ"
};

export const InActive = (args: Args) => <Template {...args} />;
InActive.args = {
  ...userImpersonatorArgs,
  hostId: "MjEwOF9mYWxzZQ"
};

export const ActiveWithError = (args: Args) => <Template {...args} />;
ActiveWithError.args = {
  ...userImpersonatorArgs,
  hostId: "NDkzN190cnVl"
};

export const InActiveWithError = (args: Args) => <Template {...args} />;
InActiveWithError.args = {
  ...userImpersonatorArgs,
  hostId: "NTU4MF90cnVl"
};
