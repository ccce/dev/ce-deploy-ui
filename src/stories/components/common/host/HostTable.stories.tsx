import { Meta, StoryFn } from "@storybook/react";
import { Container } from "@mui/material";
import { http, delay } from "msw";
import hosts from "../../../../mocks/fixtures/Hosts.json";
import { HostTable } from "../../../../components/host/HostTable";
import { RouterHarness } from "../../../../mocks/AppHarness";
import {
  hideStorybookControls,
  paginationNoResults
} from "../../../utils/common-args";

export default {
  title: "Host/HostTable",
  argTypes: {
    hosts: hideStorybookControls,
    totalCount: hideStorybookControls,
    rowsPerPage: hideStorybookControls,
    lazyParams: hideStorybookControls,
    columnSort: hideStorybookControls,
    setLazyParams: hideStorybookControls,
    setColumnSort: hideStorybookControls
  }
} as Meta<typeof HostTable>;

type HostTableStory = StoryFn<typeof HostTable>;

const Template: HostTableStory = (args) => (
  <RouterHarness>
    <Container>
      <HostTable {...args} />
    </Container>
  </RouterHarness>
);

export const Empty: HostTableStory = (args) => <Template {...args} />;

Empty.args = {
  loading: false,
  hosts: []
};

export const EmptyLoading: HostTableStory = (args) => <Template {...args} />;

EmptyLoading.args = {
  ...Empty.args,
  loading: true
};

export const BeforeAsync: HostTableStory = (args) => <Template {...args} />;

BeforeAsync.args = {
  ...Empty.args,
  pagination: { ...paginationNoResults, totalCount: hosts.totalCount },
  hosts: hosts.netBoxHosts
};
BeforeAsync.argTypes = {
  loading: hideStorybookControls
};
BeforeAsync.parameters = {
  msw: {
    handlers: [
      http.get("*/hosts/*", async () => await delay("infinite")),
      http.get("*/hosts/:host_id/status", async () => await delay("infinite")),
      http.get("*/hosts/:host_id/alerts", async () => delay("infinite"))
    ]
  }
};

export const AfterAsync: HostTableStory = (args) => <Template {...args} />;
AfterAsync.args = { ...BeforeAsync.args };
AfterAsync.argTypes = { ...BeforeAsync.argTypes };
