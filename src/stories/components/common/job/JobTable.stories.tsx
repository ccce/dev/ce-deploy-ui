import { Box } from "@mui/material";
import { Meta, StoryFn } from "@storybook/react";
import { RouterHarness } from "../../../../mocks/AppHarness";
import { JobTable } from "../../../../components/Job/JobTable";
import operationList from "../../../../mocks/fixtures/Jobs.json";
import {
  hideStorybookControls,
  paginationNoResults
} from "../../../utils/common-args";
import { Job } from "../../../../store/deployApi";

export default {
  title: "Jobs/JobTable",
  argTypes: {
    rowType: {
      options: ["jobLog", "userPageJobLog"],
      control: { type: "radio" }
    },
    jobs: hideStorybookControls,
    pagination: hideStorybookControls,
    onPage: hideStorybookControls
  }
} as Meta<typeof JobTable>;

type JobTableStory = StoryFn<typeof JobTable>;

const Template: JobTableStory = (args) => (
  <RouterHarness>
    <Box height="90vh">
      <JobTable {...args} />
    </Box>
  </RouterHarness>
);

export const Empty: JobTableStory = (args) => <Template {...args} />;

Empty.args = {
  loading: false,
  jobs: [],
  pagination: paginationNoResults,
  onPage: () => {}
};

export const EmptyLoading: JobTableStory = (args) => <Template {...args} />;

EmptyLoading.args = {
  ...Empty.args,
  loading: true
};

export const Populated: JobTableStory = (args) => <Template {...args} />;

Populated.args = {
  ...Empty.args,
  jobs: operationList?.jobs as Job[],
  pagination: { ...paginationNoResults, totalCount: operationList.totalCount }
};

Populated.argTypes = {
  loading: hideStorybookControls
};
