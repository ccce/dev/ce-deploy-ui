import { Meta } from "@storybook/react";
import { IOCLiveStatus } from "../../../../components/IOC/IOCLiveStatus";
import { AppHarness } from "../../../../mocks/AppHarness";
import { userImpersonatorArgs } from "../../../utils/common-args";
import iocs from "../../../../mocks/fixtures/PagedIOCResponse.json";
import { IocDetails } from "../../../../store/deployApi";
import { User } from "../../../../types/common";

export default {
  title: "IOC/IOCLiveStatus"
} as Meta<typeof IOCLiveStatus>;

interface Args {
  ioc: IocDetails;
  isLoggedIn: boolean;
  user?: User;
  userRoles?: string[];
}

const Template = (args: Args) => {
  return (
    <AppHarness
      useTestUser={args.isLoggedIn}
      {...args}
    >
      <IOCLiveStatus ioc={args.ioc} />
    </AppHarness>
  );
};

export const Default = (args: Args) => <Template {...args} />;
Default.args = {
  ioc: iocs.iocList.find((ioc) => ioc.id === 15),
  isLoggedIn: false
};

export const LoggedIn = (args: Args) => <Template {...args} />;
LoggedIn.args = {
  ...userImpersonatorArgs,
  ...Default.args,
  isLoggedIn: true
};
