import { Meta, StoryFn } from "@storybook/react";
import { http, delay } from "msw";
import { Box } from "@mui/material";
import { IOCTable } from "../../../../components/IOC/IOCTable";
import iocs from "../../../../mocks/fixtures/PagedIOCResponse.json";
import { RouterHarness } from "../../../../mocks/AppHarness";
import {
  hideStorybookControls,
  paginationNoResults
} from "../../../utils/common-args";

type IOCTableStory = StoryFn<typeof IOCTable>;

export default {
  title: "IOC/IOCTable",
  argTypes: {
    rowType: {
      options: ["explore", "host"],
      control: { type: "radio" }
    },
    iocs: hideStorybookControls,
    pagination: hideStorybookControls,
    onPage: hideStorybookControls
  }
} as Meta<typeof IOCTable>;

const Template: IOCTableStory = (args) => {
  return (
    <RouterHarness>
      <Box height="90vh">
        <IOCTable {...args} />
      </Box>
    </RouterHarness>
  );
};

export const Empty: IOCTableStory = (args) => <Template {...args} />;

Empty.args = {
  rowType: "explore",
  loading: false,
  iocs: [],
  pagination: paginationNoResults,
  onPage: () => {}
};

export const EmptyLoading: IOCTableStory = (args) => <Template {...args} />;

EmptyLoading.args = {
  ...Empty.args,
  loading: true
};

export const BeforeAsync: IOCTableStory = (args) => <Template {...args} />;

BeforeAsync.args = {
  ...Empty.args,
  pagination: { ...paginationNoResults, totalCount: iocs.totalCount },
  iocs: iocs.iocList
};
BeforeAsync.argTypes = {
  loading: hideStorybookControls
};

BeforeAsync.parameters = {
  msw: {
    handlers: [
      http.get("*/iocs/*", async () => await delay("infinite")),
      http.get("*/iocs/:ioc_id/status", async () => await delay("infinite"))
    ]
  }
};

export const AfterAsync: IOCTableStory = (args) => <Template {...args} />;
AfterAsync.args = { ...BeforeAsync.args };
AfterAsync.argTypes = { ...BeforeAsync.argTypes };
