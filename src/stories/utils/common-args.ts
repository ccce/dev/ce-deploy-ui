import testUser from "../../mocks/fixtures/User.json";

export const defaultUser = testUser;
export const defaultUserRoles = [
  "DeploymentToolAdmin",
  "DeploymentToolIntegrator"
];

// see https://storybook.js.org/docs/react/essentials/controls#disable-controls-for-specific-properties
export const hideStorybookControls = {
  table: {
    disable: true
  }
};

export const paginationNoResults = {
  totalCount: 0,
  rowsPerPageOptions: [5, 10, 20, 50, 100],
  rows: 10,
  page: 0
};

export const userImpersonatorArgs = {
  user: { ...defaultUser },
  userRoles: [...defaultUserRoles]
};
