import { useContext, useEffect } from "react";
import { Navigate, Route, Routes, BrowserRouter } from "react-router-dom";
import { StyledEngineProvider, CssBaseline } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { SnackbarProvider } from "notistack";
import { AppErrorBoundary, GlobalAppBarContext } from "@ess-ics/ce-ui-common";
import { theme } from "./style/Theme";
import { IOCListView } from "./views/IOC/IOCListView";
import { NavigationMenu } from "./components/navigation/NavigationMenu";
import { IOCDetailsAccessControl } from "./views/IOC/IOCDetailsAccessControl";
import { JobDetailsAccessControl } from "./views/jobs/JobDetailsAccessControl";
import { UserProvider } from "./api/UserProvider";
import { HostListView } from "./views/host/HostListView";
import { HostDetailsAccessControl } from "./views/host/details/HostDetailsAccessControl";
import { HelpView } from "./views/help/HelpView";
import { TokenRenew } from "./components/auth/TokenRenew";
import { NotFoundView } from "./components/navigation/NotFoundView";
import { LoginView } from "./views/login/LoginView";
import { JobLogAccessControl } from "./views/jobs/JobLogAccessControl";
import { RecordListView } from "./views/records/RecordListView";
import { RecordDetailsView } from "./views/records/RecordDetailsView";
import { TestErrorView } from "./views/TestErrorView";
import { applicationTitle } from "./components/navigation/applicationTitle";
import { CreateIOCAccessControl } from "./views/IOC/CreateIOCAccessControl";
import { UserDetailsAccessControl } from "./views/UserPage/UserDetailsAccessControl";
import { ReduxProvider } from "./store/ReduxProvider";
import { env } from "./config/env";
import { GlobalAppBarContext as GlobalAppBarContextType } from "./types/common";
import { MAX_SNACK } from "./components/common/snackbar";

// setting up the application (TAB)title
function App() {
  const { setTitle } = useContext<GlobalAppBarContextType>(GlobalAppBarContext);
  useEffect(() => setTitle(applicationTitle()), [setTitle]);

  return (
    <AppErrorBoundary supportHref={env.SUPPORT_URL}>
      <ReduxProvider>
        <BrowserRouter>
          <SnackbarProvider
            preventDuplicate
            maxSnack={MAX_SNACK}
          >
            <StyledEngineProvider injectFirst>
              <ThemeProvider theme={theme}>
                <CssBaseline />
                <UserProvider>
                  <TokenRenew />
                  <NavigationMenu>
                    <Routes>
                      <Route
                        path="/"
                        element={<Navigate to="/iocs" />}
                      />
                      <Route
                        path="/records"
                        element={<RecordListView />}
                      />
                      <Route
                        path="/records/:name"
                        element={<RecordDetailsView />}
                      />
                      <Route
                        path="/iocs/create"
                        element={<CreateIOCAccessControl />}
                      />
                      <Route
                        path="/iocs/:id"
                        element={<IOCDetailsAccessControl />}
                      />
                      <Route
                        path="/iocs"
                        element={<IOCListView />}
                      />
                      <Route
                        path="/jobs"
                        element={<JobLogAccessControl />}
                      />
                      <Route
                        path="/jobs/:id"
                        element={<JobDetailsAccessControl />}
                      />
                      <Route
                        path="/hosts/:id"
                        element={<HostDetailsAccessControl />}
                      />
                      <Route
                        path="/hosts"
                        element={<HostListView />}
                      />
                      <Route
                        path="/help"
                        element={<HelpView />}
                      />
                      <Route
                        path="/login"
                        element={<LoginView />}
                      />
                      <Route
                        path="/error-test"
                        element={<TestErrorView />}
                      />
                      <Route
                        path="/user/:userName"
                        element={<UserDetailsAccessControl />}
                      />
                      <Route
                        path="*"
                        element={<NotFoundView />}
                      />
                    </Routes>
                  </NavigationMenu>
                </UserProvider>
              </ThemeProvider>
            </StyledEngineProvider>
          </SnackbarProvider>
        </BrowserRouter>
      </ReduxProvider>
    </AppErrorBoundary>
  );
}

export default App;
