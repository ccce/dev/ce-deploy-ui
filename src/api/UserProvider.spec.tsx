import { useContext, useEffect } from "react";
import { userContext } from "@ess-ics/ce-ui-common";
import { SnackbarProvider } from "notistack";
import { UserProvider } from "./UserProvider";
import { ReduxProvider } from "../store/ReduxProvider";
import { UserContext } from "../types/common";
import { MAX_SNACK } from "../components/common/snackbar";

function DisplayUserContextValue() {
  const contextValue = useContext(userContext);
  return <pre id="display">{JSON.stringify(contextValue, null, 2)}</pre>;
}

const LoginTrigger = () => {
  const { login } = useContext<UserContext>(userContext);
  useEffect(() => {
    login("", "");
  }, [login]);

  return <></>;
};

describe("UserProvider", () => {
  context("when logged in", () => {
    beforeEach(() => {
      cy.login();
    });

    it("provides the user", () => {
      cy.mount(
        <ReduxProvider>
          <SnackbarProvider
            preventDuplicate
            maxSnack={MAX_SNACK}
          >
            <UserProvider>
              <LoginTrigger />
              <DisplayUserContextValue />
            </UserProvider>
          </SnackbarProvider>
        </ReduxProvider>
      );

      cy.wait("@getUserRoles");
      cy.wait("@infoFromUserName");
      cy.get("#display").contains("John Smith").contains("DeploymentToolAdmin");

      cy.get("@infoFromUserName.all").should("have.length", 1);
    });
  });
});
