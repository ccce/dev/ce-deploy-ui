import { Pagination } from "../types/common";

interface initRequestParamsProps {
  pagination: Pagination;
  filter?: string;
}

export function initRequestParams({
  pagination,
  filter
}: initRequestParamsProps) {
  const requestParams = {
    page: pagination.page,
    limit: pagination.rows,
    query: ""
  };
  if (filter != null && filter) {
    requestParams.query = filter;
  }

  return requestParams;
}
