import {
  JobDetails,
  Deployment,
  AwxJobDetails,
  Alert
} from "../store/deployApi";

export interface TypeMap {
  DEPLOY: string;
  UNDEPLOY: string;
  BATCH_DEPLOY: string;
  BATCH_UNDEPLOY: string;
  START: string;
  STOP: string;
  UNKNOWN: string;
}

export type Status = Alert["type"] | "SUCCESS" | "DEPLOY";

interface JobMessages {
  queued: string;
  running: string;
  failed: string;
  successful: string;
  unknown: string;
}

const jobMessages = {
  queued: "is queued",
  running: "is running",
  failed: "has failed",
  successful: "was successful",
  unknown: "status is unknown"
};

const typeMap = {
  DEPLOY: "Deployment",
  UNDEPLOY: "Undeployment",
  BATCH_DEPLOY: "Batch deployment",
  BATCH_UNDEPLOY: "Batch undeployment",
  START: "Start command",
  STOP: "Stop command",
  UNKNOWN: "Unknown"
};

export interface AWXJob {
  isRunning: () => boolean;
  isFinished: () => boolean;
  wasSuccessful: () => boolean;
  typeLabel: () => string;
  status: () => string;
  message: () => string;
  severity: () => Status;
}

export class AWXJobDetails {
  job: JobDetails | undefined;
  jobType: string | undefined;
  constructor(job: JobDetails | undefined, jobType: string | undefined) {
    this.job = job;
    this.jobType = jobType || typeMap.UNKNOWN;
  }
  isRunning() {
    return !this.job?.finishedAt;
  }
  isFinished() {
    return !this.isRunning();
  }
  wasSuccessful(): boolean {
    return this.job?.status?.toLowerCase() === "successful";
  }
  typeLabel(): string {
    return typeMap[this.jobType as keyof TypeMap];
  }
  status(): string {
    return this.job?.status?.toString().toLowerCase() || "Unknown";
  }
  message(): string {
    if (this.job) {
      const stem = this.typeLabel();
      const info =
        jobMessages[this.job?.status?.toLowerCase() as keyof JobMessages];
      if (stem && info) {
        return `${stem} ${info}`;
      }
      return "";
    }
    return "";
  }
  severity(): Status {
    const status = this.job?.status?.toLowerCase();
    if (status) {
      if (status === "failed") {
        return "ERROR";
      } else if (status === "successful") {
        return "SUCCESS";
      } else {
        return "INFO";
      }
    }
    return "ERROR";
  }
}

interface DeploymentInfo {
  type: () => string | null;
}

export class DeploymentInfoDetails {
  deployment: Deployment | undefined;
  constructor(deployment: Deployment | undefined) {
    this.deployment = deployment;
  }
  type(): string | null {
    if (this.deployment) {
      return this.deployment.undeployment ? "undeployment" : "deployment";
    }
    return null;
  }
}
export class DeploymentStatus {
  deployment: Deployment | undefined;
  deploymentJob: AwxJobDetails | undefined;
  deploymentHelper: DeploymentInfo;
  jobHelper: AWXJob;

  constructor(
    deployment: Deployment | undefined,
    deploymentJob: AwxJobDetails | undefined
  ) {
    this.deployment = deployment;
    this.deploymentJob = deploymentJob;
    this.deploymentHelper = new DeploymentInfoDetails(deployment);
    this.jobHelper = new AWXJobDetails(
      deploymentJob,
      deployment?.undeployment ? "UNDEPLOY" : "DEPLOY"
    );
  }
  isFinished(): boolean {
    return this.jobHelper.isFinished();
  }
  wasSuccessful(): boolean {
    return this.jobHelper.wasSuccessful();
  }
  status(): string | undefined {
    return this.deploymentJob?.status?.toLowerCase();
  }
  message(): string | null {
    const stem = `The ${this.deploymentHelper.type()} `;
    const info = this.deploymentJob
      ? jobMessages[
          this.deploymentJob?.status?.toLowerCase() as keyof JobMessages
        ]
      : ": fetching data";
    const message = stem + info;
    return message;
  }
  severity(): string {
    const status = this.deploymentJob?.status?.toLowerCase();
    if (status === "failed") {
      return "error";
    } else if (status === "successful") {
      return "success";
    } else {
      return "info";
    }
  }
}
