import { useCallback, useEffect, useState, ReactNode } from "react";
import { userContext } from "@ess-ics/ce-ui-common";
import {
  useLoginMutation,
  useLogoutMutation,
  useInfoFromUserNameQuery,
  useGetUserRolesQuery,
  UserInfoResponse,
  GetUserRolesApiResponse
} from "../store/deployApi";
import { getErrorState } from "../components/common/Alerts/AlertsData";
import { ApiError } from "../types/common";

export function UserProvider({ children }: { children: ReactNode }) {
  const [user, setUser] = useState<UserInfoResponse | undefined>(undefined);
  const [userRoles, setUserRoles] = useState<
    GetUserRolesApiResponse | undefined
  >(undefined);
  const [error, setError] = useState<ApiError>(undefined);

  const [callLogin, { isLoading: loginLoading, reset: resetLogin }] =
    useLoginMutation();
  const [callLogout, { isLoading: logoutLoading }] = useLogoutMutation();
  const {
    data: userResponse,
    isLoading: userLoading,
    isFetching: userFetching,
    refetch: refetchUserInfo
  } = useInfoFromUserNameQuery({});
  const {
    data: userRolesResponse,
    isLoading: userRolesLoading,
    isFetching: userRolesFetching,
    refetch: refetchUserRoles
  } = useGetUserRolesQuery(undefined);

  const initialized = Boolean(
    !loginLoading || !logoutLoading || !userLoading || !userRolesLoading
  );

  const login = useCallback(
    async (username: string, password: string) => {
      callLogin({ login: { userName: username, password } })
        .unwrap()
        .then(() => {
          refetchUserInfo()
            .unwrap()
            .then(() => setUser(userResponse));
          refetchUserRoles()
            .unwrap()
            .then(() => setUserRoles(userRolesResponse));
        })
        .catch((error) => setError(error));
    },
    [
      callLogin,
      refetchUserInfo,
      refetchUserRoles,
      userResponse,
      userRolesResponse
    ]
  );

  useEffect(() => {
    setUser(userResponse);
    setUserRoles(userRolesResponse);
  }, [userRolesResponse, userResponse]);

  const logout = useCallback(() => {
    callLogout();
    setUser(undefined);
    setUserRoles(undefined);
  }, [callLogout, setUser, setUserRoles]);

  return (
    initialized && (
      <userContext.Provider
        value={{
          user,
          userRoles,
          login,
          loginError: getErrorState(error).message,
          loginLoading: userFetching || userRolesFetching || loginLoading,
          logout,
          resetLoginError: resetLogin
        }}
      >
        {children}
      </userContext.Provider>
    )
  );
}
