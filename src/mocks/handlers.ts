import { http, HttpResponse } from "msw";
import { apiHandlers } from "./mockAPI";

const glue = (requestHandler: any) => {
  return (info: any) => {
    const d = requestHandler(info);
    let cookie;
    // the only cookie the mockAPI will set is the authorization cookie
    const cookieHeader = d.headers?.["Set-Cookie"];
    if (cookieHeader) {
      const [name, value] = cookieHeader.split(";")[0].split("=");

      cookie = name + "=" + value;
      if (cookieHeader.includes("Max-Age")) {
        cookie += "Max-Age=" + new Date(0);
      }
    }
    let headers = {};
    if (cookie) {
      headers = {
        "Set-Cookie": cookie
      };
    }
    return HttpResponse.json(d.body || {}, {
      status: d.status || 200,
      headers: headers
    });
  };
};

const restMap = {
  GET: http.get,
  POST: http.post,
  DELETE: http.delete
};

// define our MSW handlers to mock our API endpoints
export const handlers = apiHandlers.map((h: any) => {
  // @ts-ignore
  const method = restMap[h.method];
  return method(h.matcher, glue(h.requestHandler));
});

//autohandlers.forEach((h) => handlers.push(h));
