import { ReactNode } from "react";
import { SnackbarProvider } from "notistack";
import { Container, CssBaseline, StyledEngineProvider } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { MemoryRouter } from "react-router-dom";
import { TestAuthContextProvider, UserImpersonator } from "./UserImpersonator";
import { theme } from "../style/Theme";
import { UserProvider } from "../api/UserProvider";
import { NavigationMenu } from "../components/navigation/NavigationMenu";
import { ReduxProvider } from "../store/ReduxProvider";
import { User } from "../types/common";
import { MAX_SNACK } from "../components/common/snackbar";

interface RouterHarnessProps {
  children: ReactNode;
  initialHistory?: string[];
}

export function RouterHarness({
  children,
  initialHistory = ["/"]
}: RouterHarnessProps) {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <MemoryRouter initialEntries={initialHistory}>{children}</MemoryRouter>
      </ThemeProvider>
    </StyledEngineProvider>
  );
}

interface AppHarnessProps {
  children: ReactNode;
  initialHistory?: string[];
  useTestUser?: boolean;
  user?: User;
  userRoles?: string[];
}

export function AppHarness({
  children,
  initialHistory = ["/"],
  useTestUser = false,
  user,
  userRoles
}: AppHarnessProps) {
  const SelectedUserProvider = useTestUser
    ? ({ children }: { children: ReactNode }) => (
        <TestAuthContextProvider>
          <UserImpersonator
            user={user}
            userRoles={userRoles}
          >
            {children}
          </UserImpersonator>
        </TestAuthContextProvider>
      )
    : UserProvider;

  return (
    <ReduxProvider>
      <SnackbarProvider
        preventDuplicate
        maxSnack={MAX_SNACK}
      >
        <RouterHarness initialHistory={initialHistory}>
          <SelectedUserProvider>
            <NavigationMenu>
              <Container>{children}</Container>
            </NavigationMenu>
          </SelectedUserProvider>
        </RouterHarness>
      </SnackbarProvider>
    </ReduxProvider>
  );
}
