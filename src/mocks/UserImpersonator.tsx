import {
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState
} from "react";
import { userContext } from "@ess-ics/ce-ui-common";
import { defaultUser, defaultUserRoles } from "../stories/utils/common-args";
import { User } from "../types/common";

const defaultLogin = "";
const defaultLogout = "";
const defaultLoginError = "";
const defaultLoginLoading = "";

interface TestAuthContext {
  user: User;
  setUser: (user: User) => void;
  userRoles: string[];
  setUserRoles: (roles: string[]) => void;
  login: string;
  setLogin: (login: string) => void;
  loginError: string;
  setLoginError: (error: string) => void;
  loginLoading: boolean;
  setLoginLoading: (loading: boolean) => void;
  logout: string;
  setLogout: (logout: string) => void;
  resetLoginError: () => void;
}

export const TestAuthContextProvider = ({
  children
}: {
  children: ReactNode;
}) => {
  const [user, setUser] = useState(defaultUser);
  const [userRoles, setUserRoles] = useState(defaultUserRoles);
  const [login, setLogin] = useState(defaultLogin);
  const [logout, setLogout] = useState(defaultLogout);
  const [loginError, setLoginError] = useState(defaultLoginError);
  const [loginLoading, setLoginLoading] = useState(defaultLoginLoading);

  const resetLoginError = useCallback(() => {
    setLoginError(defaultLoginError);
  }, [setLoginError]);

  const value = useMemo(
    () => ({
      user,
      setUser,
      userRoles,
      setUserRoles,
      login,
      setLogin,
      loginError,
      setLoginError,
      loginLoading,
      setLoginLoading,
      logout,
      setLogout,
      resetLoginError
    }),
    [
      user,
      setUser,
      userRoles,
      setUserRoles,
      login,
      setLogin,
      loginError,
      setLoginError,
      loginLoading,
      setLoginLoading,
      logout,
      setLogout,
      resetLoginError
    ]
  );

  return <userContext.Provider value={value}>{children}</userContext.Provider>;
};

interface UserImpersonatorProps {
  user?: User;
  userRoles?: string[];
  children: ReactNode;
}

export const UserImpersonator = ({
  user,
  userRoles,
  children
}: UserImpersonatorProps) => {
  const { setUser, setUserRoles } = useContext<TestAuthContext>(userContext);

  useEffect(() => {
    if (user && userRoles) {
      setUser(user);
      setUserRoles(userRoles);
    }
  }, [setUser, setUserRoles, user, userRoles]);

  return <>{children}</>;
};
