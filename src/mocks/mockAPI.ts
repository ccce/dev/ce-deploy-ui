import { matchPath } from "react-router-dom";

import specData from "./fixtures/ccce-api.json";
import userData from "./fixtures/User.json";
import gitProjectsData from "./fixtures/GitProjects.json";
import jobsData from "./fixtures/Jobs.json";
import pagedResponseData from "./fixtures/PagedIOCResponse.json";
import createIocData from "./fixtures/CreateIocResult.json";
import iocDetailsData from "./fixtures/IOCDetails.json";
import recordResponseData from "./fixtures/RecordResponse.json";
import iocDescriptionData from "./fixtures/IOCDescription.json";
import awxDetailsData from "./fixtures/AwxJobDetails.json";
import hostsData from "./fixtures/Hosts.json";
import hostStatusData from "./fixtures/HostStatus.json";
import hostAlertsData from "./fixtures/HostAlerts.json";
import hostSyslogsData from "./fixtures/HostSysLogs.json";
import iocProcLogsData from "./fixtures/IOCProcServLogs.json";
import iocStatusData from "./fixtures/IOCStatus.json";
import iocAlertsData from "./fixtures/IOCAlerts.json";
import namingNamesData from "./fixtures/NamingNames.json";
import maintenanceData from "./fixtures/MaintenanceMode.json";

// Utility to extract search params from the
// String representation of the request url
// (Swagger client's request is a fetch Request,
// so the url is a String and not a URL object)
function searchParams({ request }: { request: any }) {
  if (typeof request.url === "string") {
    const url = new URL(request.url);
    return url.searchParams;
  } else {
    return request.url.searchParams;
  }
}

function getParameters(req: any, pattern: any) {
  const url = req.request ? req.request.url : req.url;
  const match = matchPath({ path: "/api/v1" + pattern }, new URL(url).pathname);
  return match?.params;
}

function withMockPagination({
  req,
  data,
  key
}: {
  req: any;
  data: any;
  key: string;
}) {
  const params = searchParams(req);
  const page = Number(params.get("page")) || 0;
  const limit = Number(params.get("limit")) || 20;

  const startIndex = page * limit;
  const pagedData = data[key].slice(startIndex, startIndex + limit);

  const results = {
    limit,
    listSize: pagedData.length,
    pageNumber: page,
    totalCount: data[key].length,
    [key]: pagedData,
    DEVELOPER_NOTE: "THIS IS MOCKED PAGING!"
  };

  return results;
}

function spec() {
  return {
    body: specData
  };
}

function login() {
  const token = "DEADBEEF";
  return {
    body: { token },
    headers: {
      "Set-Cookie": `ce-deploy-auth=${token}`
    }
  };
}

function logout() {
  return {
    headers: {
      "Set-Cookie": "ccce-auth=; Max-Age=0"
    }
  };
}

function isLoggedIn(req: any) {
  const { cookies } = req;
  if (cookies) {
    return Boolean(cookies["ce-deploy-auth"]);
  } else {
    const { headers } = req;
    return (headers.cookie ?? "").includes("ce-deploy-auth=");
  }
}

function auth(routeHandler: any) {
  return function (req: any) {
    if (isLoggedIn(req)) {
      return routeHandler(req);
    } else {
      return { status: 401 };
    }
  };
}

function getUserRoles() {
  const body = ["DeploymentToolAdmin", "DeploymentToolIntegrator"];
  return { body };
}

// Git
function infoFromUserName() {
  const body = [userData];
  return { body };
}
function listProjects() {
  return { gitProjectsData };
}

function gitProjectDetails(req: any) {
  const params = getParameters(req, "/git_helper/project_info/:project_id");
  const jobs = jobsData.jobs;
  const job = jobs.find(
    (job) => job.gitProjectId === Number(params?.project_id)
  );
  const body = job ? job.git_project_info : null;
  return { body };
}

function listTagsAndCommitIds(req: any) {
  const params = getParameters(req, "/git_helper/:project_id/tags_and_commits");
  const jobs = jobsData.jobs;
  const job = jobs.find(
    (job) => job.gitProjectId === Number(params?.project_id)
  );
  const body = job ? job.tags_and_commits : [];
  return { body };
}

function gitReferenceType(req: any) {
  const params = getParameters(
    req,
    "/git_helper/:project_id/reference_type/:git_reference"
  );
  const jobs = jobsData.jobs;
  const body = jobs.find((job) => job.gitReference === params?.git_reference);
  return { body };
}

function renew() {
  const token = "FADEDEAD";
  return {
    body: { token },
    headers: {
      "Set-Cookie": `ce-deploy-auth=${token}`
    }
  };
}

function listIOCs() {
  return { pagedResponseData };
}

function createIoc() {
  return { createIocData };
}

function getIOC(req: any) {
  const params = getParameters(req, "/iocs/:id");
  const data = iocDetailsData;
  const body = data.find((x) => x.id === Number(params?.id));
  console.debug("getIOC", data, params, body);
  const status = body ? 200 : 404;
  return { body, status };
}

function deleteIOC(req: any) {
  return req;
}

function getIocDescription(req: AnalyserNode) {
  const params = getParameters(req, "/iocs/:id/description");
  const data = iocDescriptionData;
  const body = data.find((x) => x.id === Number(params?.id));
  console.debug("getIocDescription", data, params, body);
  const status = body ? 200 : 404;
  return { body, status };
}

function findAllRecords() {
  return { record_response: recordResponseData };
}

function fetchJobStatus(req: any) {
  const params = getParameters(req, "/jobs/:job_id/awx/status");
  const data = awxDetailsData;
  const body = data.find((x) => x.jobId === Number(params?.job_id));
  const status = body ? 200 : 404;
  return { body, status };
}

function fetchDeploymentJobLog(req: any) {
  const params = getParameters(req, "/jobs/:id/awx/log");
  const jobDetailsData = awxDetailsData;
  const body = jobDetailsData.find((x) => x.jobId === Number(params?.id));
  const status = body ? 200 : 404;
  return { body, status };
}

function listJobs(req: any) {
  const data = jobsData;
  const body = withMockPagination({ req, data, key: "jobs" });
  return { body };
}

function fetchJob(req: any) {
  const params = getParameters(req, "/jobs/:operation_id");
  const data = jobsData;

  const body = data?.jobs?.find((x) => x?.id === Number(params?.operation_id));
  const status = body ? 200 : 404;
  return { body, status };
}

function findNetBoxHostByFqdn(req: any) {
  const params = getParameters(req, "/hosts/:fqdn/info_by_fqdn");
  const hosts = hostsData;
  const body = hosts.netBoxHosts.find((x) => x.fqdn === params?.fqdn);
  const status = body ? 200 : 404;
  return { body, status };
}

function listHosts(req: any) {
  const data = hostsData;
  const body = withMockPagination({ req, data, key: "netBoxHosts" });
  return { body };
}

function findHostStatus(req: any) {
  const params = getParameters(req, "/hosts/:host_id/status");
  const data = hostStatusData;
  const body = data.find((x) => x.hostId === params?.host_id);
  const status = body ? 200 : 404;
  return { body, status };
}

function findHostAlerts(req: any) {
  const params = getParameters(req, "/hosts/:host_id/alerts");
  const data = hostAlertsData;
  const body = data.find((x) => x.hostId === params?.host_id);
  const status = body ? 200 : 404;
  return { body, status };
}

function checkHostExists(req: any) {
  const params = getParameters(req, "/hosts/:hostId/exists");
  const data = jobsData;
  const body = data.jobs.find((job: any) => job.host.hostId === params?.hostId);
  const status = body?.host?.deleted ? 404 : 200;
  return { status };
}

function fetchSysLogLines() {
  return { hostSyslogsData };
}

function fetchProcServLogLines() {
  return { iocProcLogsData };
}

function fetchIocStatus(req: any) {
  const params = getParameters(req, "/iocs/:ioc_id/status");
  const data = iocStatusData;
  console.debug("fetchIocStatus ", params);
  const body = data.find((x) => `${x.iocId}` === params?.ioc_id);
  const status = body ? 200 : 404;

  return { body, status };
}

function fetchIocAlerts(req: any) {
  const params = getParameters(req, "/iocs/:ioc_id/alerts");
  const data = iocAlertsData;
  const body = data.find((x) => `${x.iocId}` === params?.ioc_id);
  const status = body ? 200 : 404;

  return { body, status };
}

function fetchIOCByName() {
  const body = namingNamesData;
  return { body };
}

function getCurrentMode() {
  const body = maintenanceData;
  return Math.random() < 0.5 ? { body } : null;
}

// just to organize
const mockAPI = {
  spec: spec,
  authentication: {
    login,
    logout,
    renew,
    getUserRoles
  },
  iocs: {
    listIOCs,
    getIOC,
    createIoc,
    deleteIOC,
    getIocDescription,
    fetchIocAlerts,
    fetchIocStatus,
    fetchProcServLogLines
  },
  maintenance: {
    getCurrentMode
  },
  jobs: {
    listJobs,
    fetchJob,
    fetchJobStatus,
    fetchDeploymentJobLog
  },
  hosts: {
    findNetBoxHostByFqdn,
    listHosts,
    findHostStatus,
    checkHostExists,
    findHostAlerts,
    fetchSysLogLines
  },
  git_helper: {
    infoFromUserName,
    listProjects,
    listTagsAndCommitIds,
    gitProjectDetails,
    gitReferenceType
  },
  names: {
    fetchIOCByName
  },
  records: {
    findAllRecords
  }
};

// Enough info to create handlers in cypress or MSW
const makeHandler = (
  method: any,
  matcher: any,
  requestHandler: any,
  wrapper?: (routeHandler: any) => (req: any) => any
) => ({
  method,
  matcher,
  requestHandler: wrapper ? wrapper(requestHandler) : requestHandler,
  name: requestHandler.name // use caution when changing api function names
});

const queryPattern = "(\\?.*)?$";
const qRegExp = (pattern: any) => RegExp(pattern + queryPattern);

// Handlers for our whole API
export const apiHandlers = [
  // api spec
  makeHandler("GET", qRegExp(".*/api/spec"), mockAPI.spec),

  // authentication
  makeHandler(
    "POST",
    qRegExp(".*/authentication/login"),
    mockAPI.authentication.login
  ),
  makeHandler(
    "POST",
    qRegExp(".*/authentication/logout"),
    mockAPI.authentication.logout,
    auth
  ),
  makeHandler("POST", qRegExp(".*/renew"), mockAPI.authentication.renew, auth),
  makeHandler(
    "GET",
    qRegExp(".*/authentication/roles"),
    mockAPI.authentication.getUserRoles,
    auth
  ),

  // iocs
  makeHandler("GET", qRegExp(".*/iocs"), mockAPI.iocs.listIOCs),
  makeHandler("POST", qRegExp(".*/iocs"), mockAPI.iocs.createIoc),
  makeHandler("GET", qRegExp(".*/iocs/[0-9]+"), mockAPI.iocs.getIOC),
  makeHandler("DELETE", qRegExp(".*/iocs/[0-9]+"), mockAPI.iocs.deleteIOC),
  makeHandler(
    "GET",
    qRegExp(".*/iocs/[0-9]+/description"),
    mockAPI.iocs.getIocDescription
  ),

  makeHandler(
    "GET",
    qRegExp(".*/iocs/[0-9]+/status"),
    mockAPI.iocs.fetchIocStatus
  ),

  makeHandler(
    "GET",
    qRegExp(".*/iocs/[0-9]+/alerts"),
    mockAPI.iocs.fetchIocAlerts
  ),

  makeHandler(
    "GET",
    qRegExp(".*/iocs/.*/.*/log"),
    mockAPI.iocs.fetchProcServLogLines
  ),

  // records
  makeHandler("GET", qRegExp(".*/records"), mockAPI.records.findAllRecords),

  // git helper
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/user_info"),
    mockAPI.git_helper.infoFromUserName,
    auth
  ),
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/projects"),
    mockAPI.git_helper.listProjects
  ),
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/project_info/[0-9]+"),
    mockAPI.git_helper.gitProjectDetails
  ),
  makeHandler(
    "GET",
    qRegExp(".*/git_helper/[0-9]+/tags_and_commits"),
    mockAPI.git_helper.listTagsAndCommitIds
  ),

  makeHandler(
    "GET",
    qRegExp(".*/git_helper/[0-9]+/reference_type/[a-zA-Z0-9_.-/]+"),
    mockAPI.git_helper.gitReferenceType
  ),

  makeHandler(
    "GET",
    qRegExp(".*/maintenance_mode/current"),
    mockAPI.maintenance.getCurrentMode
  ),
  // operations
  makeHandler("GET", qRegExp(".*/jobs"), mockAPI.jobs.listJobs),
  makeHandler("GET", qRegExp(".*/jobs/[0-9]+"), mockAPI.jobs.fetchJob, auth),
  makeHandler(
    "GET",
    qRegExp(".*/jobs/[0-9]+/awx/status"),
    mockAPI.jobs.fetchJobStatus
  ),
  makeHandler(
    "GET",
    qRegExp(".*/jobs/[0-9]+/awx/log"),
    mockAPI.jobs.fetchDeploymentJobLog,
    auth
  ),

  // hosts
  makeHandler("GET", qRegExp(".*/hosts"), mockAPI.hosts.listHosts),
  makeHandler(
    "GET",
    qRegExp(".*/hosts/[A-Za-z0-9+/]+/status"),
    mockAPI.hosts.findHostStatus
  ),
  makeHandler(
    "GET",
    qRegExp(".*/hosts/[A-Za-z0-9+/]+/alerts"),
    mockAPI.hosts.findHostAlerts
  ),
  makeHandler(
    "GET",
    qRegExp(".*/hosts/.*/info_by_fqdn"),
    mockAPI.hosts.findNetBoxHostByFqdn
  ),
  makeHandler(
    "GET",
    qRegExp(".*/hosts/.*/exists"),
    mockAPI.hosts.checkHostExists
  ),
  makeHandler(
    "GET",
    qRegExp(".*/hosts/.*/log"),
    mockAPI.hosts.fetchSysLogLines
  ),
  // naming
  makeHandler(
    "GET",
    qRegExp(".*/names/ioc_names_by_name"),
    mockAPI.names.fetchIOCByName
  )
];
