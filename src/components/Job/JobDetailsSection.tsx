import { useState } from "react";
import { Paper, Box, Typography } from "@mui/material";
import {
  KeyValueTable,
  ExternalLink,
  InternalLink,
  formatDateAndTime,
  EmptyValue,
  Duration,
  SimpleAccordion
} from "@ess-ics/ce-ui-common";
import { JobDetailsBatchJob } from "./JobDetailsBatchJob";
import { JobDetailsSingleJob } from "./JobDetailsSingleJob";
import { ActionTypeIconText } from "./JobIcons";
import { isBatchJob } from "./JobUtils";
import { JobDetails } from "../../store/deployApi";

interface JobDetailsSectionProps {
  job: JobDetails;
}

const getTimeDetails = (job: JobDetails) => {
  return {
    started: job?.createdAt ? formatDateAndTime(job.createdAt) : <EmptyValue />,
    completed: job?.finishedAt ? (
      formatDateAndTime(job.finishedAt)
    ) : (
      <EmptyValue />
    ),
    duration: job?.createdAt ? (
      <Duration
        createOrStartDate={job.createdAt && new Date(job.createdAt)}
        finishedAt={
          job.finishedAt
            ? job.finishedAt && new Date(job.finishedAt)
            : Date.now()
        }
        textOnly
      />
    ) : (
      <EmptyValue />
    )
  };
};

const getDeploymentDetails = (job: JobDetails) => {
  return {
    user: (
      <InternalLink
        to={`/user/${job.createdBy}`}
        label={`User profile, ${job.createdBy}`}
      >
        {job.createdBy}
      </InternalLink>
    ),
    "AWX job link": job?.jobUrl ? (
      <ExternalLink
        href={job.jobUrl}
        label="AWX job"
      >
        {job.jobUrl}
      </ExternalLink>
    ) : (
      <EmptyValue />
    )
  };
};

export const JobDetailsSection = ({ job }: JobDetailsSectionProps) => {
  const [expanded, setExpanded] = useState(false);
  return (
    <>
      <Paper sx={{ padding: "12px 12px 0 12px" }}>
        <Box sx={{ paddingLeft: "4px" }}>
          <ActionTypeIconText action={job.action} />
        </Box>
        {isBatchJob(job.action) ? (
          <Box sx={{ marginBottom: "16px" }}>
            <JobDetailsBatchJob job={job} />
          </Box>
        ) : (
          <Box sx={{ marginBottom: "16px" }}>
            <JobDetailsSingleJob job={job} />
          </Box>
        )}
      </Paper>

      <SimpleAccordion
        expanded={expanded}
        onChange={() => setExpanded((prev) => !prev)}
        summary={
          <Typography
            component="h2"
            variant="h3"
            sx={{ marginRight: "8px" }}
          >
            Job details
          </Typography>
        }
      >
        <KeyValueTable
          obj={getDeploymentDetails(job)}
          variant="overline"
          sx={{
            border: 0,
            "& .MuiBox-root": {
              width: "100%"
            }
          }}
          valueOptions={{ headerName: "" }}
        />
        <Paper sx={{ marginTop: "8px", padding: "12px" }}>
          <KeyValueTable
            obj={getTimeDetails(job)}
            variant="overline"
            sx={{ border: 0 }}
            valueOptions={{ headerName: "" }}
          />
        </Paper>
      </SimpleAccordion>
    </>
  );
};
