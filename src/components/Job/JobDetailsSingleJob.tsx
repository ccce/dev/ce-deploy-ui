import { InternalLink, KeyValueTable } from "@ess-ics/ce-ui-common";
import { isDeploymentJob } from "./JobUtils";
import { JobRevisionChip } from "./JobRevisionChip";
import { JobHost } from "./JobHost";
import { JobDetails } from "../../store/deployApi";

interface JobDetailsSingleJobProps {
  job: JobDetails;
}

const getRevision = (job: JobDetails) => {
  return {
    revision:
      job.gitReference && job.gitProjectId ? (
        <JobRevisionChip
          gitReference={job.gitReference}
          gitProjectId={job.gitProjectId}
        />
      ) : (
        "Unknown"
      )
  };
};

const getSingleOperationFields = (job: JobDetails, isDeployJob: boolean) => {
  return {
    ioc: (
      <InternalLink
        to={`/iocs/${job.iocId}`}
        label={`IOC details, ${job.iocName}`}
      >
        {job.iocName}
      </InternalLink>
    ),
    ...(isDeployJob && getRevision(job)),
    host: (
      <JobHost
        url={job.host?.externalIdValid ? job.host.hostId : undefined}
        label={job.host?.fqdn}
      />
    )
  };
};

export const JobDetailsSingleJob = ({ job }: JobDetailsSingleJobProps) => {
  const isDeployJob = isDeploymentJob(job.action);
  return (
    <KeyValueTable
      obj={getSingleOperationFields(job, isDeployJob)}
      variant="overline"
      sx={{ border: 0 }}
      valueOptions={{ headerName: "" }}
    />
  );
};
