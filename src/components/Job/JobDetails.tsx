import { useEffect, useState, useMemo } from "react";
import { Typography, Stack } from "@mui/material";
import {
  SimpleAccordion,
  AlertBannerList,
  SingleStateStepper,
  STEPPER_STATES,
  AccessControl
} from "@ess-ics/ce-ui-common";
import { JobDetailsSection } from "./JobDetailsSection";
import { DeploymentJobOutput } from "../deployments/DeploymentJobOutput";
import { AWXJobDetails, Status } from "../../api/DataTypes";
import { JobDetails } from "../../store/deployApi";

interface JobDetailsProps {
  jobDetail: JobDetails;
}

const createAlert = (type: Status, message: string) => {
  return {
    type,
    message
  };
};

export const JobsDetails = ({ jobDetail: operation }: JobDetailsProps) => {
  const [alerts, setAlerts] = useState<
    { type?: Status; message?: string; link?: string }[]
  >(operation.alerts ?? []);
  const [expanded, setExpanded] = useState(false);

  const awxJob = useMemo(
    () => new AWXJobDetails(operation, operation.action),
    [operation]
  );

  useEffect(() => {
    const operationAlerts = operation.alerts ? operation.alerts : [];
    setAlerts([
      createAlert(awxJob.severity(), awxJob.message()),
      ...operationAlerts
    ]);
  }, [operation, awxJob]);

  return (
    <Stack spacing={2}>
      <Stack>{<AlertBannerList alerts={alerts} />}</Stack>
      <JobDetailsSection job={operation} />
      <AccessControl
        allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
        renderNoAccess={() => <></>}
      >
        <SimpleAccordion
          expanded={expanded}
          onChange={() => setExpanded((prev) => !prev)}
          summary={
            <Stack
              flexDirection="row"
              alignItems="center"
            >
              <Typography
                component="h2"
                variant="h3"
                sx={{ marginRight: "8px" }}
              >
                Job log stream
              </Typography>
              <SingleStateStepper
                showToolTip
                step={{
                  state: operation?.status || STEPPER_STATES.UNKNOWN,
                  label: awxJob.message()
                }}
              />
            </Stack>
          }
        >
          <DeploymentJobOutput
            job={operation}
            isExpanded={expanded}
          />
        </SimpleAccordion>
      </AccessControl>
    </Stack>
  );
};
