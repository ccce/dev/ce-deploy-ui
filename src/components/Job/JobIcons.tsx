import { Stack, Typography } from "@mui/material";
import {
  PlayCircleFilled,
  PauseCircleFilled,
  AddCircleOutline,
  RemoveCircleOutline
} from "@mui/icons-material";
import { BatchDeploySymbol } from "../../icons/BatchDeploySymbol";
import { BatchUndeploySymbol } from "../../icons/BatchUndeploySymbol";

const batchIconDefaultProps = { width: "25px", height: "25px" };

export const StartIcon = ({ ...props }) => <PlayCircleFilled {...props} />;
export const StopIcon = ({ ...props }) => <PauseCircleFilled {...props} />;
export const DeployIcon = ({ ...props }) => <AddCircleOutline {...props} />;
export const UndeployIcon = ({ ...props }) => (
  <RemoveCircleOutline {...props} />
);
export const BatchDeployIcon = ({ ...props }) => (
  <BatchDeploySymbol
    {...batchIconDefaultProps}
    {...props}
  />
);
export const BatchUndeployIcon = ({ ...props }) => (
  <BatchUndeploySymbol
    {...batchIconDefaultProps}
    {...props}
  />
);

interface ActionTypeIconTextProps {
  action: string | undefined;
}

export interface ActionType {
  DEPLOY: string;
  UNDEPLOY: string;
  BATCH_DEPLOY: string;
  BATCH_UNDEPLOY: string;
  START: string;
  STOP: string;
}

const ActionTypesComponent = {
  DEPLOY: {
    label: "Deployment",
    icon: DeployIcon
  },
  UNDEPLOY: {
    label: "Undeployment",
    icon: UndeployIcon
  },
  BATCH_DEPLOY: {
    label: "Batch deployment",
    icon: BatchDeployIcon
  },
  BATCH_UNDEPLOY: {
    label: "Batch undeployment",
    icon: BatchUndeployIcon
  },
  START: {
    label: "Set active",
    icon: StartIcon
  },
  STOP: {
    label: "Set inactive",
    icon: StopIcon
  }
};

export const ActionTypeIconText = ({ action }: ActionTypeIconTextProps) => {
  const Icon = ActionTypesComponent[action as keyof ActionType].icon;

  return (
    <Stack
      flexDirection="row"
      alignItems="center"
      gap="5px"
      sx={{ width: "100%" }}
    >
      <Icon
        width="24px"
        height="24px"
      />
      <Typography>
        {ActionTypesComponent[action as keyof ActionType].label}
      </Typography>
    </Stack>
  );
};
