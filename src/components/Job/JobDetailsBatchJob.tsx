import { useMemo, useEffect, useState } from "react";
import {
  EllipsisText,
  InternalLink,
  Table,
  SimpleAccordion,
  usePagination
} from "@ess-ics/ce-ui-common";
import { Typography, Stack } from "@mui/material";
import { getNoOfIOCs, getNoOfHosts, isDeploymentJob } from "./JobUtils";
import { JobHost } from "./JobHost";
import { JobRevisionChip } from "./JobRevisionChip";
import { ROWS_PER_PAGE } from "../../constants";
import { JobDetails, JobDetailsEntry } from "../../store/deployApi";

interface JobDetailsBatchJobProps {
  job: JobDetails;
}

const columns = [
  {
    field: "ioc",
    headerName: "IOC",
    flex: 0.7
  },
  {
    field: "host",
    headerName: "Host",
    flex: 1
  }
];

const createRow = (job: JobDetails, action: string | undefined) => {
  return {
    id: `${job.host?.hostId}${job.iocId}`,
    ioc: (
      <Stack
        sx={{ padding: "8px 16px 8px 0" }}
        gap={2}
      >
        <Stack
          key={job.iocId}
          flexDirection="row"
          gap={1}
        >
          <InternalLink
            to={`/iocs/${job.iocId}`}
            label={`Ioc details, ${job.iocName}`}
          >
            <EllipsisText>{job.iocName}</EllipsisText>
          </InternalLink>
          {isDeploymentJob(action) && job.gitReference && job.gitProjectId && (
            <JobRevisionChip
              gitReference={job.gitReference}
              gitProjectId={job.gitProjectId}
            />
          )}
        </Stack>
      </Stack>
    ),
    host: (
      <JobHost
        url={job.host?.externalIdValid ? job.host.hostId : undefined}
        label={job.host?.fqdn}
      />
    )
  };
};

export const JobDetailsBatchJob = ({ job }: JobDetailsBatchJobProps) => {
  const [currentRows, setCurrentRows] = useState<JobDetailsEntry[]>([]);
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0,
    initTotalCount: job.jobs?.length || 0
  });

  const noOfIOCs = useMemo(() => {
    return getNoOfIOCs(job.jobs);
  }, [job]);

  const noOfHosts = useMemo(() => {
    return getNoOfHosts(job.jobs);
  }, [job]);

  useEffect(() => {
    const { limit, page } = pagination;
    const jobs = (job.jobs as JobDetailsEntry[]) || [];
    const list = jobs.reduce((acc: JobDetailsEntry[][], _, index) => {
      if (index % limit === 0) {
        acc.push(jobs.slice(index, index + limit));
      }
      return acc;
    }, []);

    setCurrentRows(list ? list[page] : []);
  }, [pagination, setPagination, job]);

  return (
    <SimpleAccordion
      summary={
        <Stack
          flexDirection="row"
          alignItems="end"
          sx={{ width: "100%" }}
          gap={1}
        >
          {" "}
          <Typography
            component="h2"
            variant="h3"
          >
            Batch
          </Typography>
          <Typography
            variant="body2"
            sx={{ fontWeight: "600", marginRight: "10px" }}
          >
            {noOfIOCs} {noOfIOCs > 1 ? "IOCs" : "IOC"}
            {", "}
            {noOfHosts} {noOfHosts > 1 ? "Hosts" : "Host"}
          </Typography>
        </Stack>
      }
    >
      <Table
        columns={columns}
        disableColumnSorting
        rows={currentRows.map((batchJob) => createRow(batchJob, job.action))}
        pagination={pagination}
        onPage={setPagination}
      />
    </SimpleAccordion>
  );
};
