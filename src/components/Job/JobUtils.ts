import { Job, JobEntry } from "../../store/deployApi";

export const ACTION_TYPES = {
  DEPLOY: "DEPLOY",
  UNDEPLOY: "UNDEPLOY",
  BATCH_DEPLOY: "BATCH_DEPLOY",
  BATCH_UNDEPLOY: "BATCH_UNDEPLOY",
  START: "START",
  STOP: "STOP"
};

export const isBatchJob = (type: Job["action"]) =>
  type === ACTION_TYPES.BATCH_DEPLOY || type === ACTION_TYPES.BATCH_UNDEPLOY;

export const isDeploymentJob = (action: string | undefined) =>
  action === ACTION_TYPES.BATCH_DEPLOY || action === ACTION_TYPES.DEPLOY;

export const getNoOfIOCs = (jobs?: JobEntry[]) =>
  [...new Set(jobs?.map((job) => job.iocId))].length;

export const getNoOfHosts = (jobs?: JobEntry[]) =>
  [...new Set(jobs?.map((job) => job?.host?.hostId))].length;
