import { EllipsisText, InternalLink } from "@ess-ics/ce-ui-common";
import { Typography } from "@mui/material";

interface JobHostProps {
  url: string | undefined;
  label: string | undefined;
}

export const JobHost = ({ url, label }: JobHostProps) => {
  // host is resolvable => show link for users
  if (url) {
    return (
      <InternalLink
        to={`/hosts/${url}`}
        label={`Host Details, ${label}`}
        width="100%"
      >
        <EllipsisText>{label}</EllipsisText>
      </InternalLink>
    );
  }
  return (
    <Typography color="gray">
      <EllipsisText>{label}</EllipsisText>
    </Typography>
  );
};
