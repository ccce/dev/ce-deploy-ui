import { Skeleton } from "@mui/material";
import {
  useGitProjectDetailsQuery,
  useListTagsAndCommitIdsQuery
} from "../../store/deployApi";
import { GitRefLink } from "../common/Git/GitRefLink";

interface JobGitRefLinkProps {
  gitReference: string;
  gitProjectId: number;
  disableExternalLinkIcon: boolean;
}

const SHORT_GIT_HASH_LENGTH = 8;

export const JobGitRefLink = ({
  gitReference,
  gitProjectId,
  disableExternalLinkIcon = false
}: JobGitRefLinkProps) => {
  const { data: projectDetails, isLoading: projectDetailsLoading } =
    useGitProjectDetailsQuery({ projectId: gitProjectId });

  const { data: tagsAndCommitsList, isLoading: tagsAndCommitsListLoading } =
    useListTagsAndCommitIdsQuery({
      projectId: gitProjectId,
      reference: gitReference
    });

  if (projectDetailsLoading || tagsAndCommitsListLoading) {
    return <Skeleton width={80} />;
  }

  const firstCommit = tagsAndCommitsList?.at(0);
  const gitRef = firstCommit?.short_reference ?? gitReference;
  const displayReference =
    firstCommit?.type === "TAG"
      ? firstCommit?.reference
      : firstCommit?.short_reference;

  return (
    <>
      {displayReference ? (
        <GitRefLink
          url={projectDetails?.projectUrl}
          revision={gitRef}
          displayReference={displayReference}
          disableExternalLinkIcon={disableExternalLinkIcon}
        />
      ) : (
        <span>{gitRef.slice(0, SHORT_GIT_HASH_LENGTH)}</span>
      )}
    </>
  );
};
