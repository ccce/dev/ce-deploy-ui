import {
  STEPPER_STATES,
  LabeledIcon,
  IconRunning,
  IconSuccessful,
  IconFailed,
  IconUnknown
} from "@ess-ics/ce-ui-common";
import { theme } from "../../style/Theme";
import { JobDetails } from "../../store/deployApi";

interface JobStatusProps {
  status: JobDetails["status"];
}

const getStatus = (status: JobDetails["status"]) => {
  switch (status) {
    case STEPPER_STATES.QUEUED:
      return {
        label: "Queued",
        icon: IconRunning
      };
    case STEPPER_STATES.RUNNING:
      return {
        label: "Running",
        icon: IconRunning
      };
    case STEPPER_STATES.SUCCESSFUL:
      return {
        label: "Successful",
        icon: IconSuccessful
      };
    case STEPPER_STATES.FAILED:
      return {
        label: "Failed",
        icon: IconFailed
      };
    default:
      return {
        label: "Unknown",
        icon: () => <IconUnknown size="small" />
      };
  }
};

export const JobStatus = ({ status }: JobStatusProps) => {
  const activeStep = getStatus(status);
  const Icon = activeStep.icon;

  return (
    <LabeledIcon
      label={activeStep.label}
      labelPosition="right"
      LabelProps={{
        color:
          status === STEPPER_STATES.FAILED
            ? theme.palette.status.fail.main
            : "inherit"
      }}
      Icon={() => <Icon size="medium" />}
    />
  );
};
