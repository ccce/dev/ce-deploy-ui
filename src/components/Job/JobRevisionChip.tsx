import { Chip } from "@mui/material";
import { JobGitRefIcon } from "./JobGitRefIcon";
import { JobGitRefLink } from "./JobGitRefLink";

interface JobRevisionChipProps {
  gitReference: string;
  gitProjectId: number;
}

export const JobRevisionChip = ({
  gitReference,
  gitProjectId
}: JobRevisionChipProps) => {
  return (
    <Chip
      sx={{
        paddingLeft: "8px",
        paddingRight: "8px",
        gap: "4px",
        "& .MuiChip-label": {
          lineHeight: "normal",
          paddingLeft: "0",
          paddingRight: "0"
        }
      }}
      size="small"
      icon={
        <JobGitRefIcon
          gitReference={gitReference}
          gitProjectId={gitProjectId}
        />
      }
      label={
        <JobGitRefLink
          gitReference={gitReference}
          gitProjectId={gitProjectId}
          disableExternalLinkIcon
        />
      }
    />
  );
};
