import { Skeleton } from "@mui/material";
import { useGitReferenceTypeQuery } from "../../store/deployApi";
import { GitRefTypeIcon } from "../common/Git/GitRefTypeIcon";

interface JobGitRefIconProps {
  gitReference: string;
  gitProjectId: number;
}

export const JobGitRefIcon = ({
  gitReference,
  gitProjectId
}: JobGitRefIconProps) => {
  const { data: gitReferenceType, isLoading } = useGitReferenceTypeQuery({
    projectId: gitProjectId,
    gitReference
  });

  return (
    <>
      {isLoading ? (
        <Skeleton
          sx={{ marginLeft: "4px" }}
          variant="circular"
          width={20}
          height={20}
        />
      ) : (
        <GitRefTypeIcon type={gitReferenceType?.reference_type} />
      )}
    </>
  );
};
