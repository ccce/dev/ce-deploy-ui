import { Stack } from "@mui/material";
import { StartAndDuration } from "@ess-ics/ce-ui-common";
import { JobStatus } from "../JobStatus";
import { JobDetails } from "../../../store/deployApi";

interface JobStatusColumnProps {
  job: JobDetails;
}

export const JobStatusColumn = ({ job }: JobStatusColumnProps) => (
  <Stack>
    <Stack gap={0.5}>
      <JobStatus status={job.status} />
      <StartAndDuration
        startTime={job.createdAt && new Date(job.createdAt)}
        finishedAt={job.finishedAt && new Date(job.finishedAt)}
      />
    </Stack>
  </Stack>
);
