import { useMemo, useEffect } from "react";
import { Box, Stack, Typography } from "@mui/material";
import { InternalLink } from "@ess-ics/ce-ui-common";
import { useLazyCheckHostExistsQuery, Job } from "../../../store/deployApi";
import {
  ACTION_TYPES,
  getNoOfIOCs,
  getNoOfHosts,
  isBatchJob
} from "../JobUtils";
import { ActionTypeIconText } from "../JobIcons";
import { JobRevisionChip } from "../JobRevisionChip";
import { JobHost } from "../JobHost";

export const JobDetailsColumn = ({ job }: { job: Job }) => {
  const isBatchOperation = isBatchJob(job.action);
  const [
    callCheckHostExists,
    { isLoading: checkHostExistsLoading, isSuccess, isError }
  ] = useLazyCheckHostExistsQuery();

  const noOfIOCs = useMemo(() => {
    if (isBatchOperation) {
      return getNoOfIOCs(job.jobs);
    }
  }, [job, isBatchOperation]);

  const noOfHosts = useMemo(() => {
    if (isBatchOperation) {
      return getNoOfHosts(job.jobs);
    }
  }, [job, isBatchOperation]);

  useEffect(() => {
    if (!isBatchOperation && job.host?.hostId) {
      callCheckHostExists({ hostId: job.host.hostId });
    }
  }, [isBatchOperation, job?.host, callCheckHostExists]);

  return (
    <Stack>
      <ActionTypeIconText action={job.action} />
      {!isBatchOperation ? (
        <Stack>
          <Stack
            flexDirection="row"
            gap={1}
            alignItems="center"
            flexWrap="wrap"
          >
            <InternalLink
              to={`/iocs/${job.iocId}`}
              label={`IOC Details, ${job.iocName}`}
            >
              {job.iocName}
            </InternalLink>
            {job?.action === ACTION_TYPES.DEPLOY &&
              job?.gitReference &&
              job?.gitProjectId && (
                <JobRevisionChip
                  gitReference={job.gitReference}
                  gitProjectId={job.gitProjectId}
                />
              )}
          </Stack>
          <Box sx={{ height: "24px" }}>
            {!checkHostExistsLoading && (isSuccess || isError) && (
              <Stack
                flexDirection="row"
                gap={1}
                alignItems="baseline"
                flexWrap="wrap"
              >
                <span>
                  <JobHost
                    url={isSuccess ? job?.host?.hostId : undefined}
                    label={job.host?.hostName}
                  />
                </span>
                <Typography variant="body2">{job?.host?.network}</Typography>
              </Stack>
            )}
          </Box>
        </Stack>
      ) : (
        <Stack>
          <Typography variant="body2">
            {noOfIOCs} {noOfIOCs && noOfIOCs > 1 ? "IOCs" : "IOC"}
          </Typography>
          <Typography variant="body2">
            {noOfHosts} {noOfHosts && noOfHosts > 1 ? "Hosts" : "Host"}
          </Typography>
        </Stack>
      )}
    </Stack>
  );
};
