import { useState, useEffect } from "react";
import { Table, InternalLink } from "@ess-ics/ce-ui-common";
import { GridColDef } from "@mui/x-data-grid";
import { JobStatusColumn } from "./JobStatusColumn";
import { JobDetailsColumn } from "./JobDetailsColumn";
import { UserAvatar } from "../../common/User/UserAvatar";
import { Job } from "../../../store/deployApi";
import { Pagination } from "../../../types/common";

const defaultColumns: GridColDef[] = [
  {
    field: "status",
    headerName: "Status",
    flex: 0.5
  },
  {
    field: "jobId",
    headerName: "ID",
    flex: 0.5
  },
  {
    field: "job",
    headerName: "Details",
    flex: 1
  },
  {
    field: "user",
    headerName: "User",
    align: "center",
    headerAlign: "center",
    flex: 0.5
  }
];

const shapeColumns = (customColumns: typeof defaultColumns) => {
  return customColumns
    .map((column) => {
      const mappedCol = defaultColumns.find(
        (col) => col.field === column.field
      );
      return mappedCol ? { ...mappedCol, ...column } : null;
    })
    .filter((column) => column !== null);
};

const createTableRow = (job: Job) => ({
  id: job.id,
  status: <JobStatusColumn {...{ job }} />,
  jobId: (
    <InternalLink
      to={`/jobs/${job?.id}`}
      label={`Job Details, ID ${job.id}`}
    >
      {`#${job.id}`}
    </InternalLink>
  ),
  job: <JobDetailsColumn {...{ job }} />,
  user: <UserAvatar username={job.createdBy} />
});

interface JobTableProps {
  jobs?: Job[];
  customColumns?: GridColDef[];
  pagination: Pagination;
  onPage: (params: Pagination) => void;
  loading: boolean;
}

export const JobTable = ({
  jobs,
  customColumns,
  pagination,
  onPage,
  loading
}: JobTableProps) => {
  const [columns, setColumns] = useState<GridColDef[] | null>(null);

  useEffect(() => {
    if (customColumns) {
      setColumns(shapeColumns(customColumns));
    } else {
      setColumns(defaultColumns);
    }
  }, [customColumns, setColumns]);

  return (
    <>
      {columns ? (
        <Table
          columns={columns}
          rows={(columns && jobs?.map((job) => createTableRow(job))) || []}
          pagination={pagination}
          onPage={onPage}
          loading={loading}
          getEstimatedRowHeight={() => 260}
          getRowHeight={() => "auto"}
          sx={{
            "&.MuiDataGrid-root--densityCompact .MuiDataGrid-cell": {
              py: 1
            },
            "&.MuiDataGrid-root--densityStandard .MuiDataGrid-cell": {
              py: "15px"
            },
            "&.MuiDataGrid-root--densityComfortable .MuiDataGrid-cell": {
              py: "22px"
            }
          }}
        />
      ) : null}
    </>
  );
};
