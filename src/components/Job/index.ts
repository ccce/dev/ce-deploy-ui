import { JobsDetails } from "./JobDetails";
import { JobTable } from "./JobTable";

export { JobsDetails as JobDetails, JobTable };
