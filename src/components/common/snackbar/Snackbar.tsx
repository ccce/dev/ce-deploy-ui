import { useCallback } from "react";
import { useSnackbar, SnackbarKey } from "notistack";
import { SnackbarButton } from "./SnackbarButton";

const AUTO_HIDE_DURATION = 3000;

export function useCustomSnackbar() {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const showError = useCallback(
    (message: string) => {
      const action = (key: SnackbarKey) => (
        <SnackbarButton
          variant="text"
          onClick={() => {
            closeSnackbar(key);
          }}
        >
          Dismiss
        </SnackbarButton>
      );

      return enqueueSnackbar(message, {
        variant: "error",
        autoHideDuration: null,
        action
      });
    },
    [enqueueSnackbar, closeSnackbar]
  );

  const showSuccess = useCallback(
    (message: string) => {
      return enqueueSnackbar(message, {
        variant: "success",
        autoHideDuration: AUTO_HIDE_DURATION
      });
    },
    [enqueueSnackbar]
  );

  const showWarning = useCallback(
    (message: string) => {
      return enqueueSnackbar(message, {
        variant: "warning",
        autoHideDuration: AUTO_HIDE_DURATION
      });
    },
    [enqueueSnackbar]
  );

  return { showSuccess, showError, showWarning };
}
