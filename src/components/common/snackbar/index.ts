import { useCustomSnackbar } from "./Snackbar";

const MAX_SNACK = 5;

export { useCustomSnackbar, MAX_SNACK };
export default useCustomSnackbar;
