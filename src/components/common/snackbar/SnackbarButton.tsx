import { Button, styled } from "@mui/material";

export const SnackbarButton = styled(Button)(() => ({
  color: "#f0f0f0",
  borderColor: "#f0f0f0",
  "&:hover": {
    backgroundColor: "#8d8d8d",
    borderColor: "#8d8d8d",
    boxShadow: "none"
  }
}));
