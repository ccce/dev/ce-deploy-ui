import { Button } from "@mui/material";
import { Launch } from "@mui/icons-material";

interface PopoutButtonProps {
  title: string;
  onButtonClick: () => void;
}

export const PopoutButton = ({ title, onButtonClick }: PopoutButtonProps) => (
  <Button
    color="inherit"
    variant="outlined"
    endIcon={<Launch />}
    onClick={onButtonClick}
  >
    {title}
  </Button>
);
