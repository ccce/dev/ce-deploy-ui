import {
  useState,
  useEffect,
  useCallback,
  useMemo,
  Dispatch,
  SetStateAction
} from "react";
import { Stack, LinearProgress, Box, SelectChangeEvent } from "@mui/material";
import { SnackbarKey, closeSnackbar } from "notistack";
import { formatDateAndTime } from "@ess-ics/ce-ui-common";
import Convert from "ansi-to-html";
import {
  useLazyFetchSyslogLinesQuery,
  useLazyFetchProcServLogLinesQuery,
  LokiResponse,
  LokiMessage
} from "../../../store/deployApi";
import { ApiAlertError } from "../Alerts/ApiAlertError";
import { LogStreamConsole } from "../LogStream/LogStreamConsole";
import { LogStreamConsoleDialog } from "../LogStream/LogStreamConsoleDialog";
import { TimeRange } from "../Inputs/TimeRange";
import { PopoutButton } from "../Buttons/PopoutButton";
import { useCustomSnackbar } from "../snackbar/Snackbar";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../../constants";

const TIME_RANGE_VALUES = [
  {
    value: 720,
    text: "12 hours"
  },
  {
    value: 10080,
    text: "7 days"
  }
];

/**
 * hostName: name of the host (mandatory) which will be the qery parameter for backend call
 * iocName: name of ioc (optional). Used when querying procServ log
 * isSyslog: boolean, if true -> SysLog will be queried, if false -> procServLog will be queried
 */

interface LokiPanelProps {
  hostName?: string;
  iocName?: string;
  isSyslog?: boolean;
  isExpanded: boolean;
}

interface PreprocessLogProps {
  logData: LokiResponse | undefined;
  showWarning: (message: string) => SnackbarKey;
  timeRange: number;
  alertIds: SnackbarKey[];
  setAlertIds: Dispatch<SetStateAction<SnackbarKey[]>>;
}

export function LokiPanel({
  hostName,
  iocName,
  isSyslog,
  isExpanded
}: LokiPanelProps) {
  const { showWarning } = useCustomSnackbar();
  const [timeRange, setTimeRange] = useState(720);
  const [logDialogOpen, setLogDialogOpen] = useState(false);
  const [periodChange, setPeriodChange] = useState(false);
  const [alertIds, setAlertIds] = useState<SnackbarKey[]>([]);
  const [html, setHtml] = useState("");

  const params = useMemo(
    () => ({
      hostName: hostName || "",
      iocName: iocName ?? "",
      timeRange: timeRange
    }),
    [hostName, iocName, timeRange]
  );

  const [
    getSysLogData,
    { data: sysLogData, error: sysLogError, isLoading: sysDataIsLoading }
  ] = useLazyFetchSyslogLinesQuery({
    pollingInterval: isExpanded ? DEFAULT_POLLING_INTERVAL_MILLIS : 0
  });

  const [
    getProcServLog,
    { data: procServLog, error: procServLogError, isLoading: procDataIsLoading }
  ] = useLazyFetchProcServLogLinesQuery({
    pollingInterval: isExpanded ? DEFAULT_POLLING_INTERVAL_MILLIS : 0
  });

  const preprocessLog = useCallback(
    ({
      logData,
      showWarning,
      timeRange,
      alertIds,
      setAlertIds
    }: PreprocessLogProps) => {
      if (!logData) {
        return "";
      }

      const logLines = logData.lines;

      if (logData?.warning && alertIds?.length === 0) {
        const warningId = showWarning(logData.warning);
        setAlertIds((prev) => [...prev, warningId]);
      }

      if (logLines && logLines.length > 0) {
        const logHtml = logLines.map((line) => formatLogLine(line));
        return `<html>
                  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <style type="text/css">.logdate { color: #5cb85c; } body.pre{font-family: Monaco, Menlo, Consolas, "Courier New", monospace;}
                    </style>
                  </head>
                  <body>
                    <pre>${logHtml.join("")}</pre>
                  </body>
                </html>`;
      }

      return `<html><body><pre> - No messages found for ${TIME_RANGE_VALUES.find(({ value }: { value: number }) => value === timeRange)?.text} period - </pre></body></html>`;
    },
    []
  );

  const logsToPreprocess = useCallback(() => {
    if (isSyslog === true) {
      return preprocessLog({
        logData: sysLogData,
        showWarning,
        timeRange,
        alertIds,
        setAlertIds
      });
    }

    return preprocessLog({
      logData: procServLog,
      showWarning,
      timeRange,
      alertIds,
      setAlertIds
    });
  }, [
    alertIds,
    isSyslog,
    preprocessLog,
    procServLog,
    showWarning,
    sysLogData,
    timeRange
  ]);

  const hasLogError = !!sysLogError || !!procServLogError;

  const handleTimeRangeChange = (event: SelectChangeEvent<number>) => {
    setPeriodChange(true);
    setTimeRange(Number(event?.target?.value));
  };

  // remove progressBar if intervall has been changed, and data received
  useEffect(() => {
    setPeriodChange(false);
  }, [sysLogData, procServLog]);

  // remove the logline limit exceed warning when user leaves component
  useEffect(() => {
    return () => {
      alertIds.forEach((id) => {
        closeSnackbar(id);
      });
    };
  }, [alertIds]);

  useEffect(() => {
    if (isExpanded) {
      if (isSyslog === true) {
        getSysLogData(params);
      } else {
        getProcServLog(params);
      }
    }
  }, [getSysLogData, getProcServLog, isSyslog, isExpanded, params]);

  useEffect(() => {
    setHtml(logsToPreprocess());
  }, [logsToPreprocess]);

  const dataReady = !!sysLogData || !!procServLog;

  if (sysDataIsLoading || procDataIsLoading) {
    return (
      <div style={{ width: "100%" }}>
        <LinearProgress color="primary" />
      </div>
    );
  }

  return (
    <Box>
      <Stack
        display="flex"
        flexDirection="row"
        justifyContent="space-between"
        sx={{ marginBottom: (theme) => theme.spacing(2) }}
      >
        <TimeRange
          selected={timeRange}
          values={TIME_RANGE_VALUES}
          onTimeRangeChange={handleTimeRangeChange}
        />
        <PopoutButton
          title="Popout log"
          onButtonClick={() => setLogDialogOpen(true)}
        />
      </Stack>
      <LogStreamConsoleDialog
        title={isSyslog ? "Host log stream" : "IOC log stream"}
        log={html}
        open={logDialogOpen}
        dataReady={dataReady}
        loading={periodChange}
        onDialogClose={() => setLogDialogOpen(false)}
      >
        <Box sx={{ marginBottom: (theme) => theme.spacing(2) }}>
          <TimeRange
            selected={timeRange}
            values={TIME_RANGE_VALUES}
            onTimeRangeChange={handleTimeRangeChange}
          />
        </Box>
      </LogStreamConsoleDialog>
      <Box
        sx={{
          position: "relative"
        }}
      >
        {hasLogError && (
          <Box
            sx={{
              position: "absolute",
              width: "auto",
              maxWidth: "40%",
              right: "1rem",
              top: "1rem"
            }}
          >
            <ApiAlertError error={sysLogError || procServLogError} />
          </Box>
        )}
        <LogStreamConsole
          log={html}
          dataReady={dataReady}
          height="500px"
          loading={periodChange}
        />
      </Box>
    </Box>
  );
}

function formatLogLine(logLine: LokiMessage) {
  const convert = new Convert();

  return (
    "<span><span class=logdate>" +
    formatDateAndTime(logLine.logDate) +
    "</span> " +
    convert.toHtml(logLine.logMessage ?? "") +
    "<br/></span>"
  );
}
