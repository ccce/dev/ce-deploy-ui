import { Avatar, Box, Typography, Stack } from "@mui/material";
import { UserInfoResponse } from "../../../store/deployApi";

interface UserProfileProps {
  userInfo: UserInfoResponse;
}

export const UserProfile = ({ userInfo }: UserProfileProps) => {
  return (
    <Stack
      flexDirection="row"
      gap={4}
      sx={{ padding: 2 }}
    >
      <Avatar
        sx={{ width: 96, height: 96 }}
        src={userInfo?.avatar}
      />
      <Box>
        <Typography
          component="h2"
          variant="h2"
        >
          {userInfo?.fullName}
        </Typography>
        <Typography
          component="h3"
          variant="h3"
          fontWeight="bold"
        >
          {`@${userInfo?.loginName}`}
        </Typography>
      </Box>
    </Stack>
  );
};
