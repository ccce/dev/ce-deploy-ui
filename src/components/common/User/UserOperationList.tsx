import { useCallback, useEffect } from "react";
import { Typography } from "@mui/material";
import { usePagination } from "@ess-ics/ce-ui-common";
import { initRequestParams } from "../../../api/initRequestParams";
import { JobTable } from "../../Job";
import {
  DEFAULT_POLLING_INTERVAL_MILLIS,
  ROWS_PER_PAGE
} from "../../../constants";
import { useLazyListJobsQuery } from "../../../store/deployApi";
import { OnPageParams } from "../../../types/common";

interface UserOperationListProps {
  userName?: string;
}

export function UserOperationList({ userName }: UserOperationListProps) {
  const [getJobs, { data: jobs, isLoading }] = useLazyListJobsQuery({
    pollingInterval: DEFAULT_POLLING_INTERVAL_MILLIS
  });

  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setPagination({ totalCount: jobs?.totalCount ?? 0 });
  }, [setPagination, jobs?.totalCount]);

  const callGetjobs = useCallback(() => {
    const requestParams = initRequestParams({ pagination });

    getJobs({ ...requestParams, user: userName });
  }, [getJobs, pagination, userName]);

  useEffect(() => {
    callGetjobs();
  }, [callGetjobs]);

  // Invoked by Table on change to pagination
  const onPage = (params: OnPageParams) => {
    setPagination(params);
  };

  return (
    <>
      <Typography
        p={2}
        variant="h2"
      >
        Jobs
      </Typography>
      <JobTable
        jobs={jobs?.jobs}
        customColumns={[
          { field: "status" },
          { field: "jobId" },
          { field: "job" }
        ]}
        loading={isLoading || !jobs}
        pagination={pagination}
        onPage={onPage}
      />
    </>
  );
}
