import { Avatar, Tooltip, styled } from "@mui/material";
import { Link } from "react-router-dom";
import { useInfoFromUserNameQuery } from "../../../store/deployApi";

interface UserAvatarProps {
  username?: string;
  size?: number;
}

export const UserAvatar = styled(({ username, size = 48 }: UserAvatarProps) => {
  const {
    data: user,
    isLoading,
    error
  } = useInfoFromUserNameQuery({ userName: username });

  return (
    <Tooltip title={username}>
      <Link
        to={`/user/${username}`}
        style={{ textDecoration: "none" }}
        aria-label={`User Profile ${username}`}
      >
        {error || isLoading || !user?.avatar ? (
          <Avatar
            sx={{ width: size, height: size }}
            alt={username}
            variant="circular"
          >
            {username?.toUpperCase().slice(0, 2)}
          </Avatar>
        ) : (
          <Avatar
            sx={{ width: size, height: size }}
            src={user?.avatar}
            alt={username}
            variant="circular"
          />
        )}
      </Link>
    </Tooltip>
  );
})({});
