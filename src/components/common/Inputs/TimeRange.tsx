import {
  FormControl,
  MenuItem,
  InputLabel,
  Select,
  SelectChangeEvent
} from "@mui/material";

interface TimeRangeProps {
  selected: number;
  values: { value: number; text: string }[];
  onTimeRangeChange: (event: SelectChangeEvent<number>) => void;
}

export const TimeRange = ({
  values,
  selected,
  onTimeRangeChange
}: TimeRangeProps) => {
  return (
    <FormControl
      variant="filled"
      sx={{ minWidth: 120 }}
    >
      <InputLabel id="time-range-select">Time range</InputLabel>
      <Select
        variant="standard"
        labelId="time-range-select"
        value={selected}
        onChange={onTimeRangeChange}
      >
        {values.map((range) => {
          return (
            <MenuItem
              key={range.value}
              value={range.value}
            >
              {range.text}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};
