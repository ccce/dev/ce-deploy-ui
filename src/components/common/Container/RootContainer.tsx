import { styled } from "@mui/material/styles";
import Container from "@mui/material/Container";

export const RootContainer = styled(Container)({
  paddingBottom: "200px"
});
