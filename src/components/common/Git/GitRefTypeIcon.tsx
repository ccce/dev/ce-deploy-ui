import CommitIcon from "@mui/icons-material/Commit";
import LabelOutlinedIcon from "@mui/icons-material/LabelOutlined";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";

interface GitRefTypeIconProps {
  type: string | undefined;
}

export const GitRefTypeIcon = ({ type }: GitRefTypeIconProps) => {
  if (type === "TAG") {
    return <LabelOutlinedIcon fontSize="small" />;
  }

  if (type === "COMMIT") {
    return <CommitIcon fontSize="medium" />;
  }

  return <QuestionMarkIcon fontSize="small" />;
};
