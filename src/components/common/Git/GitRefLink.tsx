import { ExternalLink } from "@ess-ics/ce-ui-common";

interface GitRefLinkProps {
  url?: string;
  revision?: string;
  displayReference?: string;
  disableExternalLinkIcon?: boolean;
}

export const GitRefLink = ({
  url,
  revision,
  displayReference,
  disableExternalLinkIcon
}: GitRefLinkProps) => {
  // if no git reference has been entered
  if (!url) {
    return <></>;
  }

  // if user enters GIT url with '.git' postfix -> remove it
  if (url.toLowerCase().endsWith(".git")) {
    url = url.toLowerCase().split(".git")[0];
  }

  // add trailing '/' if needed for GIT server address
  if (!url.endsWith("/")) {
    url += "/";
  }

  // calculate the endpoint for tag/commitId
  url = url + "-/tree/" + revision;

  return (
    <ExternalLink
      href={url}
      label={`External Git Link, revision ${revision}`}
      disableExternalLinkIcon={disableExternalLinkIcon}
    >
      {displayReference}
    </ExternalLink>
  );
};
