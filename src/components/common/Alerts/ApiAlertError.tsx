import { Alert } from "@mui/material";
import { getErrorState } from "./AlertsData";
import { ApiError } from "../../../types/common";

interface ApiAlertErrorProps {
  error: ApiError;
}

export const ApiAlertError = ({ error }: ApiAlertErrorProps) => (
  <Alert severity="error">{getErrorState(error).message}</Alert>
);
