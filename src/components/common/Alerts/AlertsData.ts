import { SerializedError } from "@reduxjs/toolkit";
import { FetchBaseQueryError } from "@reduxjs/toolkit/query";
import { ErrorMessage } from "../../../types/common";

interface BrowserError {
  error: string;
  status: string;
}

const isBrowserError = (
  error: BrowserError | FetchBaseQueryError | SerializedError | unknown
) => {
  return (
    error != null &&
    typeof error === "object" &&
    "error" in error &&
    typeof (error as BrowserError).status === "string"
  );
};

export const isFetchBaseQueryError = (
  error: FetchBaseQueryError | SerializedError | BrowserError | unknown
): error is FetchBaseQueryError => {
  return (
    error != null &&
    typeof error === "object" &&
    "status" in error &&
    typeof (error as FetchBaseQueryError).status === "number"
  );
};

export const isSerializedError = (
  error: FetchBaseQueryError | SerializedError | BrowserError | unknown
): error is SerializedError => {
  return (
    error != null &&
    typeof error === "object" &&
    "message" in error &&
    typeof (error as SerializedError).message === "string"
  );
};

export const getErrorState = (
  error: FetchBaseQueryError | SerializedError | BrowserError | unknown
) => {
  if (error) {
    if (isFetchBaseQueryError(error)) {
      const customError = error.data as ErrorMessage;
      if (customError) {
        return {
          message: customError.description || customError.error,
          status: error.status
        };
      }
    } else if (isSerializedError(error)) {
      return {
        message: error.message,
        status: error.code
      };
    } else if (isBrowserError(error)) {
      const browserError = error as BrowserError;
      return {
        message: browserError.error,
        status: browserError.status
      };
    }
    return {
      message: "Unknown Error",
      status: 0
    };
  }
  return {
    message: "",
    status: null
  };
};
