import {
  Brightness1,
  StopCircle,
  RadioButtonUnchecked,
  HelpOutline
} from "@mui/icons-material";

export const STATUS = {
  active: "active",
  disabled: "disabled",
  disabledAlert: "disabled alert",
  disabledWarning: "disabled warning",
  alert: "alert",
  warning: "warning",
  inactive: "inactive",
  inactiveAlert: "inactive alert",
  inactiveWarning: "inactive warning"
};

export const SEVERITY = {
  info: "info",
  alert: "error",
  warning: "warning"
};

export const statusesWithAlerts = [
  STATUS.alert,
  STATUS.warning,
  STATUS.disabledAlert,
  STATUS.disabledWarning,
  STATUS.inactiveAlert,
  STATUS.inactiveWarning
];

export const statusConfig = {
  [STATUS.active]: {
    title: "Active",
    icon: Brightness1
  },
  [STATUS.disabled]: {
    title: "Disabled",
    icon: StopCircle
  },
  [STATUS.disabledAlert]: {
    title: "Disabled",
    icon: StopCircle
  },
  [STATUS.disabledWarning]: {
    title: "Disabled",
    icon: StopCircle
  },
  [STATUS.alert]: {
    title: "Active",
    icon: Brightness1
  },
  [STATUS.warning]: {
    title: "Active",
    icon: Brightness1
  },
  [STATUS.inactive]: {
    title: "Inactive",
    icon: RadioButtonUnchecked
  },
  [STATUS.inactiveAlert]: {
    title: "Inactive",
    icon: RadioButtonUnchecked
  },
  [STATUS.inactiveWarning]: {
    title: "Inactive",
    icon: RadioButtonUnchecked
  },
  null: {
    title: "Unknown",
    icon: HelpOutline
  }
};
