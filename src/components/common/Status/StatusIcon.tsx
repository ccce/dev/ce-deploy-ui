import { array, string } from "prop-types";
import { statusConfig, statusesWithAlerts } from "./StatusData";
import { Alert } from "../../../store/deployApi";

const propTypes = {
  alerts: array,
  status: string
};

interface StatusIconProps {
  alerts: Alert[];
  status: string;
}

const getLabel = ({ alerts, status }: StatusIconProps) => {
  if (statusesWithAlerts.includes(status)) {
    const warnings = alerts.filter((alert) => alert.type === "WARNING");
    const errors = alerts.filter((alert) => alert.type === "ERROR");
    const hasWarnings = Boolean(warnings.length);
    const hasErrors = Boolean(errors.length);
    return `${statusConfig[status].title} ${
      hasWarnings ? warnings.length + " warnings " : ""
    }${hasErrors ? errors.length + " errors" : ""}!`;
  }
  return statusConfig[status].title;
};

export const StatusIcon = ({ alerts, status }: StatusIconProps) => {
  const Icon = statusConfig[status].icon;

  return <Icon aria-label={getLabel({ alerts, status })} />;
};

StatusIcon.propTypes = propTypes;
