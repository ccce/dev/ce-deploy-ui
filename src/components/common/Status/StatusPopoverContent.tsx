import { string, arrayOf, object } from "prop-types";
import { Typography, Stack } from "@mui/material";
import { AlertBanner, useUniqueKeys } from "@ess-ics/ce-ui-common";
import { SEVERITY } from "./StatusData";
import { Alert } from "../../../store/deployApi";

const propsTypes = {
  title: string,
  alerts: arrayOf(object),
  object
};

interface StatusPopoverContentProps {
  title: string;
  alerts: Alert[];
}
export const StatusPopoverContent = ({
  title,
  alerts
}: StatusPopoverContentProps) => {
  // Filter out INFO level alerts
  // And for now filter out links on alerts due to issues with
  // focus behavior of popovers
  // Also normalize the type to lowercase since AlertBanner expects lowercase
  const sanitizedAlerts = (
    alerts?.filter((alert) => alert?.type?.toLowerCase() !== SEVERITY.info) ||
    []
  ).map((alert) => ({
    ...alert,
    type: alert?.type?.toLowerCase(),
    link: undefined
  }));
  const alertsKeys = useUniqueKeys(sanitizedAlerts);

  return (
    <Stack
      gap={1}
      sx={{ minWidth: 0, maxWidth: "50vw" }}
    >
      <Typography
        sx={{
          fontWeight: sanitizedAlerts.length > 0 ? "bold" : "normal"
        }}
      >
        {title}
      </Typography>
      {sanitizedAlerts.length > 0
        ? sanitizedAlerts.map((alert, i) => (
            <AlertBanner
              {...alert}
              key={alertsKeys[i]}
            />
          ))
        : null}
    </Stack>
  );
};

StatusPopoverContent.propsTypes = propsTypes;
