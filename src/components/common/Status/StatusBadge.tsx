import { ReactNode } from "react";
import { string, object, arrayOf, oneOfType, node } from "prop-types";
import { Stack } from "@mui/material";
import WarningAmberIcon from "@mui/icons-material/WarningAmber";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import { STATUS } from "./StatusData";

const propTypes = {
  status: string,
  theme: object,
  children: oneOfType([arrayOf(node), node])
};

const commonStyles = {
  position: "absolute",
  top: "-10px",
  left: "14px",
  fontSize: "20px"
};

interface StatusBadgeProps {
  status: string;
  children: ReactNode;
}

export const StatusBadge = ({ status, children }: StatusBadgeProps) => {
  const showWarning =
    status === STATUS.warning ||
    status === STATUS.inactiveWarning ||
    status === STATUS.disabledWarning;

  const showError =
    status === STATUS.alert ||
    status === STATUS.inactiveAlert ||
    status === STATUS.disabledAlert;

  return (
    <Stack sx={{ position: "relative" }}>
      {children}
      {showWarning && (
        <WarningAmberIcon
          sx={{ fill: (theme) => theme.palette.warning.main, ...commonStyles }}
        />
      )}
      {showError && (
        <ErrorOutlineIcon
          sx={{ fill: (theme) => theme.palette.error.main, ...commonStyles }}
        />
      )}
    </Stack>
  );
};

StatusBadge.propTypes = propTypes;
