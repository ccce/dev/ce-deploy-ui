import { useState, useEffect } from "react";
import { object, bool, array, func } from "prop-types";
import { useTheme, Skeleton } from "@mui/material";
import { StatusPopoverContent } from "./StatusPopoverContent";
import { StatusBadge } from "./StatusBadge";
import { StatusIcon } from "./StatusIcon";
import { statusesWithAlerts, statusConfig } from "./StatusData";
import { Popover } from "../../common/Popover";
import {
  HostStatusResponse,
  HostAlertResponse,
  IocStatusResponse,
  IocAlertResponse
} from "../../../store/deployApi";
import { getHostStatus } from "../../host/HostStatus";
import { getIOCStatus } from "../../IOC/IOCStatus/IOCStatusData";

const propsTypes = {
  state: object,
  alert: array,
  hideAlerts: bool,
  getStatusFcn: func
};

interface StatusProps {
  id?: string | number | undefined;
  state: HostStatusResponse | IocStatusResponse | undefined;
  alert: HostAlertResponse | IocAlertResponse | undefined;
  hideAlerts?: boolean;
  getStatusFcn: typeof getHostStatus | typeof getIOCStatus;
}

export function Status({
  id,
  state,
  alert,
  hideAlerts = false,
  getStatusFcn
}: StatusProps) {
  const theme = useTheme();
  const [status, setStatus] = useState<string | null>(null);

  useEffect(() => {
    if (state) {
      setStatus(getStatusFcn(state, alert));
    }
  }, [state, alert, getStatusFcn]);

  return (
    <>
      {status ? (
        <Popover
          id={`ioc-status-popover-${id}`}
          renderOwningComponent={({
            ariaOwns,
            ariaHasPopup,
            handlePopoverOpen,
            handlePopoverClose
          }) => (
            <div
              aria-owns={ariaOwns}
              aria-haspopup={ariaHasPopup}
              onMouseEnter={handlePopoverOpen}
              onMouseLeave={handlePopoverClose}
              style={{ color: theme.palette.status.icons }}
            >
              {!hideAlerts && statusesWithAlerts.includes(status) ? (
                <StatusBadge status={status}>
                  <StatusIcon
                    alerts={alert?.alerts ?? []}
                    status={status}
                  />
                </StatusBadge>
              ) : (
                <StatusIcon
                  alerts={alert?.alerts ?? []}
                  status={status}
                />
              )}
            </div>
          )}
          popoverContents={
            <StatusPopoverContent
              title={statusConfig[status].title}
              alerts={hideAlerts ? [] : (alert?.alerts ?? [])}
            />
          }
          anchorOrigin={{
            vertical: "top",
            horizontal: "right"
          }}
        />
      ) : (
        <Skeleton
          variant="circular"
          height={24}
          width={24}
        />
      )}
    </>
  );
}

Status.propsTypes = propsTypes;
