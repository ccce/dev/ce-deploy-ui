import { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import { LinearProgress } from "@mui/material";

/* Log data must contain a wrapping <pre> </pre>
around content that should be displayed*/

interface LogStreamConsoleProps {
  log?: string;
  dataReady: boolean;
  height: string;
  loading?: boolean;
}

const ConsoleElement = styled("div")(({ theme }) => ({
  width: "100%",
  background: "black",

  ["pre"]: {
    color: "white",
    overflow: "auto",
    whiteSpace: "pre-wrap",
    padding: theme.spacing(4),
    marginTop: 0,
    fontSize: "0.8rem"
  }
}));

export const LogStreamConsole = ({
  log,
  dataReady,
  height,
  loading
}: LogStreamConsoleProps) => {
  const [autoScrollEnabled, setAutoScrollEnabled] = useState(true);
  const [preElementYPosition, setPreElementYPosition] = useState(0);

  // eslint-disable-next-line
  const handleScroll = (e: any) => {
    const preElement = e.currentTarget;
    if (preElementYPosition > preElement.scrollTop) {
      // Scroll up event
      setAutoScrollEnabled(false);
    } else if (
      preElement.scrollHeight - preElement.scrollTop ===
      preElement.clientHeight
    ) {
      // Scroll to bottom
      setAutoScrollEnabled(true);
    }
    setPreElementYPosition(preElement.scrollTop);
  };

  useEffect(() => {
    if (dataReady) {
      const parent = document.getElementsByClassName("console");
      if (parent) {
        Array.from(parent).forEach((par) => {
          const preElements = par.getElementsByTagName("pre");
          if (preElements.length > 0 && preElements[0]) {
            if (log && autoScrollEnabled) {
              preElements[0].onscroll = handleScroll;
              preElements[0].scrollTop = preElements[0].scrollHeight;
            }
          }
        });
      }
    }
  }, [dataReady, autoScrollEnabled, handleScroll, log]);

  return (
    <>
      {!loading ? (
        <ConsoleElement
          className="console"
          sx={{
            maxHeight: height,
            minHeight: height,
            "& pre": { height: height }
          }}
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: log || "" }}
        />
      ) : (
        <div style={{ width: "100%" }}>
          <LinearProgress color="primary" />
        </div>
      )}
    </>
  );
};
