import { Dialog } from "@ess-ics/ce-ui-common";
import { Typography, LinearProgress, Container } from "@mui/material";
import { LogStreamConsole } from "./LogStreamConsole";

interface LogStreamConsoleDialogProps {
  title: string;
  loading: boolean;
  open: boolean;
  log?: string;
  dataReady: boolean;
  children?: JSX.Element;
  onDialogClose: () => void;
}

export const LogStreamConsoleDialog = ({
  title,
  loading,
  open,
  log,
  dataReady,
  onDialogClose,
  children
}: LogStreamConsoleDialogProps) => {
  return (
    <Dialog
      title={
        <Typography
          variant="h2"
          marginY={1}
        >
          {title}
        </Typography>
      }
      content={
        loading ? (
          <div style={{ width: "100%", height: "600px" }}>
            <LinearProgress color="primary" />
          </div>
        ) : (
          <Container maxWidth="xl">
            {children}
            <LogStreamConsole
              log={log}
              dataReady={dataReady}
              height="600px"
            />
          </Container>
        )
      }
      open={open}
      onClose={onDialogClose}
      DialogProps={{
        fullWidth: true,
        maxWidth: "xl"
      }}
    />
  );
};
