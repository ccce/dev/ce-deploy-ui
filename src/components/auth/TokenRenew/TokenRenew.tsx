import { useContext, useEffect } from "react";
import { userContext } from "@ess-ics/ce-ui-common";
import { env } from "../../../config/env";
import { useTokenRenewMutation } from "../../../store/deployApi";
import { UserContext } from "../../../types/common";

export const TokenRenew = () => {
  const { user } = useContext<UserContext>(userContext);

  const [renewToken] = useTokenRenewMutation();

  useEffect(() => {
    if (user) {
      const intervalId = setInterval(() => {
        renewToken();
      }, env.TOKEN_RENEW_INTERVAL);

      return () => clearInterval(intervalId);
    }
  }, [user, renewToken]);

  return null;
};
