import { ServerErrorPage } from "@ess-ics/ce-ui-common";
import { env } from "../../../config/env";

export const AccessDenied = () => {
  return (
    <ServerErrorPage
      message="You do not have permission to access this page. Please contact an administrator to request access"
      status={401}
      supportHref={env.SUPPORT_URL}
    />
  );
};
