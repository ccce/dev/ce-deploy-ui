import { useContext, ReactNode } from "react";
import { userContext } from "@ess-ics/ce-ui-common";
import { AccessDenied } from "../AccessDenied";
import { UserContext } from "../../../types/common";

interface AllowedUserWithRoles {
  user: string;
  role: string;
}

interface AccessControlProps {
  allowedRoles: string[];
  allowedUsersWithRoles?: AllowedUserWithRoles[];
  children: ReactNode;
  renderNoAccess?: () => ReactNode;
}

const checkPermissions = (
  userRoles: UserContext["userRoles"],
  allowedRoles: string[]
) => {
  if (allowedRoles.length === 0) {
    return true;
  }

  return userRoles?.some((role) => allowedRoles.includes(role));
};

const checkUser = (
  user: UserContext["user"],
  userRoles: UserContext["userRoles"],
  allowedUsersWithRoles?: AllowedUserWithRoles[]
) => {
  if (
    !allowedUsersWithRoles ||
    allowedUsersWithRoles.length === 0 ||
    (userRoles && userRoles.length === 0)
  ) {
    return false;
  }

  return allowedUsersWithRoles.find(
    (userWithRole) =>
      userWithRole.user === user?.loginName &&
      userRoles?.includes(userWithRole.role)
  );
};

export const AccessControl = ({
  allowedRoles,
  allowedUsersWithRoles,
  children,
  renderNoAccess
}: AccessControlProps) => {
  const { user, userRoles } = useContext<UserContext>(userContext);

  const permitted =
    checkPermissions(userRoles, allowedRoles) ||
    checkUser(user, userRoles, allowedUsersWithRoles);
  if (permitted) {
    return children;
  }
  if (!renderNoAccess) {
    return <AccessDenied />;
  }
  return renderNoAccess();
};
