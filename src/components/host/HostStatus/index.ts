import { HostStatus } from "./HostStatus";
import { getHostStatus } from "./HostStatusData";

export { HostStatus, getHostStatus };
export default HostStatus;
