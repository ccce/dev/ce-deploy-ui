import { useEffect } from "react";
import { getHostStatus } from "./HostStatusData";
import { Status } from "../../common/Status";
import {
  useFindHostStatusQuery,
  useLazyFindHostAlertsQuery
} from "../../../store/deployApi";

interface HostStatusProps {
  hostId?: string;
  hideAlerts: boolean;
}

export const HostStatus = ({ hostId, hideAlerts }: HostStatusProps) => {
  const [callFindHostAlerts, { data: hostAlert }] =
    useLazyFindHostAlertsQuery();

  const { data: hostStateStatus } = useFindHostStatusQuery(
    {
      hostId: hostId ?? ""
    },
    { skip: !hostId }
  );

  useEffect(() => {
    if (!hideAlerts && hostId) {
      callFindHostAlerts({ hostId });
    }
  }, [callFindHostAlerts, hideAlerts, hostId]);

  return (
    <>
      {hostStateStatus && (
        <Status
          id={hostStateStatus.hostId}
          state={hostStateStatus}
          alert={hostAlert}
          hideAlerts={hideAlerts}
          getStatusFcn={getHostStatus}
        />
      )}
    </>
  );
};
