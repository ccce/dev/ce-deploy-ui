import {
  HostAlertResponse,
  HostStatusResponse
} from "../../../store/deployApi";
import { SEVERITY, STATUS } from "../../common/Status";

export const HOST_STATUS = {
  available: "AVAILABLE",
  unreachable: "UNREACHABLE"
};

export const getHostStatus = (
  state: HostStatusResponse,
  alert: HostAlertResponse | undefined
) => {
  const { status } = state;
  const alertSeverity = alert?.alertSeverity?.toLowerCase();

  if (
    status === HOST_STATUS.available &&
    (!alertSeverity || alertSeverity === SEVERITY.info)
  ) {
    return STATUS.active;
  } else if (
    status === HOST_STATUS.available &&
    alertSeverity === SEVERITY.alert
  ) {
    return STATUS.alert;
  } else if (
    status === HOST_STATUS.available &&
    alertSeverity === SEVERITY.warning
  ) {
    return STATUS.warning;
  } else if (
    status === HOST_STATUS.unreachable &&
    (!alertSeverity || alertSeverity === SEVERITY.info)
  ) {
    return STATUS.inactive;
  } else if (
    status === HOST_STATUS.unreachable &&
    alertSeverity === SEVERITY.alert
  ) {
    return STATUS.inactiveAlert;
  } else if (
    status === HOST_STATUS.unreachable &&
    alertSeverity === SEVERITY.warning
  ) {
    return STATUS.inactiveWarning;
  } else {
    // unknown fallback state
    console.debug(
      "getHostStatusInfo status " + status + " alertSeverity " + alertSeverity
    );
    return null;
  }
};
