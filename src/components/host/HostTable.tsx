import { Table, InternalLink, EllipsisText } from "@ess-ics/ce-ui-common";
import { HostStatus } from "./HostStatus";
import { HostInfoWithId } from "../../store/deployApi";
import { OnPageParams, Pagination } from "../../types/common";

const columns = [
  {
    field: "status",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  { field: "hostName", headerName: "Host" },
  { field: "description", headerName: "Description" },
  { field: "network", headerName: "Network" },
  { field: "scope", headerName: "Network scope" }
];

function CreateRow(host: HostInfoWithId) {
  return {
    id: host.hostId,
    status: (
      <HostStatus
        hostId={host?.hostId}
        hideAlerts={false}
      />
    ),
    hostName: (
      <InternalLink
        to={`/hosts/${host.hostId}`}
        label={`Host details, ${host.name}`}
        width="100%"
      >
        <EllipsisText>{host.name}</EllipsisText>
      </InternalLink>
    ),
    host: host,
    description: <EllipsisText>{host.description}</EllipsisText>,
    network: <EllipsisText>{host.network}</EllipsisText>,
    scope: <EllipsisText>{host.scope}</EllipsisText>
  };
}

interface HostTableProps {
  hosts: HostInfoWithId[] | undefined;
  pagination?: Pagination;
  onPage?: (page: OnPageParams) => void;
  loading: boolean;
}

export function HostTable({
  hosts,
  pagination,
  onPage,
  loading
}: HostTableProps) {
  return (
    <Table
      columns={columns}
      rows={hosts?.map((host) => CreateRow(host))}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
    />
  );
}
