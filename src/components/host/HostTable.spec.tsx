import { composeStories } from "@storybook/react";
import * as stories from "../../stories/components/common/host/HostTable.stories";

const { AfterAsync } = composeStories(stories);
const textColumns = ["host", "description", "network", "scope"];
const columns = ["status"].concat(textColumns);
const firstRowData = [
  "Active",
  "pbi-bcm03-mtca-ioc01",
  "",
  "BeamDiagLabNetworks"
];

describe("HostTable", () => {
  context("Populated Table", () => {
    beforeEach(() => {
      cy.mount(<AfterAsync />);
    });

    it("Has the correct columns", () => {
      cy.findAllByRole("columnheader").each(($el, index) => {
        console.debug(index, columns[index]);
        cy.wrap($el).contains(columns[index], { matchCase: false });
      });
    });

    it("Truncates all text content", () => {
      cy.findAllByRole("row")
        .eq(1) // first row is headers, so get next index
        .find(".MuiTypography-noWrap")
        .should("have.length", textColumns.length);
    });

    it("Displays correct content in first row", () => {
      cy.findAllByRole("row")
        .eq(1) // first row is headers, so get next index
        .each(($el, index) => {
          if (index === 0) {
            cy.get('[data-testid="Brightness1Icon"]')
              .eq(0)
              .should("have.attr", "aria-label", firstRowData[index]);
          } else {
            cy.wrap($el)
              .find("p")
              .should(($el) => {
                const text = $el.text().trim().toLowerCase();
                const expected = firstRowData[index].trim().toLowerCase();
                expect(text).to.equal(expected);
              });
          }
        });
    });
  });
});
