import {
  Button,
  LinearProgress,
  Grid,
  Typography,
  Tooltip
} from "@mui/material";
import { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { ConfirmationDialog } from "@ess-ics/ce-ui-common";
import { AccessControl } from "../../auth/AccessControl";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { IocDetails, Job } from "../../../store/deployApi";
import {
  useIocStartJobMutation,
  useIocStopJobMutation
} from "../../../store/enhancedApi";

export function IOCService({ ioc }: { ioc: IocDetails }) {
  const navigate = useNavigate();
  const [startModalOpen, setStartModalOpen] = useState(false);
  const [stopModalOpen, setStopModalOpen] = useState(false);
  const [command, setCommand] = useState<Job | null>(null);

  const [
    startJob,
    { error: startJobError, data: startJobData, isLoading: isStartJobLoading }
  ] = useIocStartJobMutation();

  const [
    stopJob,
    { error: stopJobError, data: stopJobData, isLoading: isStopJobLoading }
  ] = useIocStopJobMutation();

  useEffect(() => {
    if (startJobData && (!command || command?.id !== startJobData.id)) {
      navigate(`/jobs/${startJobData.id}`);
      setCommand(startJobData);
    }
  }, [startJobData, command, navigate]);

  useEffect(() => {
    if (stopJobData && (!command || command?.id !== stopJobData.id)) {
      navigate(`/jobs/${stopJobData.id}`);
      setCommand(stopJobData);
    }
  }, [stopJobData, command, navigate]);

  const resetUI = useCallback(() => {
    setCommand(null);
  }, []);

  const start = useCallback(() => {
    if (ioc.id) {
      resetUI();
      startJob({
        iocId: ioc.id
      });
    }
  }, [resetUI, startJob, ioc.id]);

  const stop = useCallback(() => {
    if (ioc.id) {
      resetUI();
      stopJob({
        iocId: ioc.id
      });
    }
  }, [resetUI, stopJob, ioc.id]);

  const onStartModalClose = useCallback(() => {
    setStartModalOpen(false);
  }, [setStartModalOpen]);

  const onStartModalConfirm = useCallback(() => {
    start();
  }, [start]);

  const onStopModalClose = useCallback(() => {
    setStopModalOpen(false);
  }, [setStopModalOpen]);

  const onStopModalConfirm = useCallback(() => {
    stop();
  }, [stop]);

  let disabledStartButtonTitle = "";
  if (isStartJobLoading || ioc.operationInProgress) {
    disabledStartButtonTitle =
      "There is an ongoing operation, you can not Start the IOC right now";
  }

  let disabledStopButtonTitle = "";
  if (isStopJobLoading || ioc.operationInProgress) {
    disabledStopButtonTitle =
      "There is an ongoing operation, you can not Stop the IOC right now";
  }

  return (
    <AccessControl
      allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
      renderNoAccess={() => "You are not authorized to control this IOC"}
    >
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Start IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to start{" "}
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Start IOC"
        cancelText="Cancel"
        open={startModalOpen}
        onClose={onStartModalClose}
        onConfirm={onStartModalConfirm}
      />
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Stop IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to stop{" "}
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Stop IOC"
        cancelText="Cancel"
        open={stopModalOpen}
        onClose={onStopModalClose}
        onConfirm={onStopModalConfirm}
      />
      <Grid
        container
        spacing={1}
      >
        <Grid item>
          <Tooltip title={disabledStopButtonTitle}>
            <span>
              <Button
                color="secondary"
                disabled={ioc.operationInProgress || isStartJobLoading}
                variant="contained"
                sx={{
                  ":hover": {
                    bgcolor: "secondary.light"
                  }
                }}
                onClick={() => setStopModalOpen(true)}
              >
                Stop
              </Button>
            </span>
          </Tooltip>
        </Grid>
        <Grid item>
          <Tooltip title={disabledStartButtonTitle}>
            <span>
              <Button
                color="primary"
                variant="contained"
                disabled={ioc.operationInProgress || isStartJobLoading}
                sx={{
                  color: "essWhite.main",
                  bgcolor: "essGrass.main",
                  "&:hover": {
                    bgcolor: "essGrass.dark"
                  }
                }}
                onClick={() => setStartModalOpen(true)}
              >
                Start
              </Button>
            </span>
          </Tooltip>
        </Grid>
        <Grid
          item
          xs={12}
          md={12}
        >
          {(startJobError || stopJobError) && (
            <ApiAlertError error={startJobError || stopJobError} />
          )}
          {(isStartJobLoading || isStopJobLoading) && (
            <LinearProgress color="primary" />
          )}
        </Grid>
      </Grid>
    </AccessControl>
  );
}
