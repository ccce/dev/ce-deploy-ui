import { Button, Stack, Tooltip } from "@mui/material";
import { useState, useEffect, useContext, useCallback } from "react";
import { userContext, ExternalLink, InternalLink } from "@ess-ics/ce-ui-common";
import { JobSection } from "./JobSection";
import { IOCService } from "./IOCService";
import { IOCDetails } from "../IOCDetails";
import { DeployIOC } from "../DeployIOC";
import { UndeployIOC } from "../UndeployIOC";
import { AccessControl } from "../../auth/AccessControl";
import { DeploymentStatus } from "../../../api/DataTypes";
import {
  IocDetails,
  useLazyFetchAwxJobStatusQuery
} from "../../../store/deployApi";
import { env } from "../../../config/env";
import { UserContext } from "../../../types/common";

interface Subset {
  [key: string]: JSX.Element | undefined;
}

export const IOCManage = ({ ioc }: { ioc: IocDetails }) => {
  const { user } = useContext<UserContext>(userContext);
  const [deployDialogOpen, setDeployDialogOpen] = useState(false);
  const [undeployDialogOpen, setUndeployDialogOpen] = useState(false);
  const [getJobStatus, { data: jobStatus }] = useLazyFetchAwxJobStatusQuery();

  useEffect(() => {
    if (ioc.activeDeployment?.jobId) {
      getJobStatus({ jobId: ioc.activeDeployment?.jobId });
    }
  }, [getJobStatus, ioc.activeDeployment?.jobId]);

  const getSubset = useCallback(
    (ioc: IocDetails) => {
      ioc = { ...ioc };
      const { sourceUrl: git } = ioc;

      const subset: Subset = {
        "Naming service record": (
          <ExternalLink
            href={`${env.NAMING_ADDRESS}/devices.xhtml?i=2&deviceName=${ioc.namingName}`}
            label="Naming Service Record"
          >
            {ioc.namingName}
          </ExternalLink>
        ),
        Repository: (
          <ExternalLink
            href={git}
            label="Git Repository"
          >
            {git}
          </ExternalLink>
        ),
        "Created by": (
          <InternalLink
            to={`/user/${ioc.createdBy}`}
            label={`Created by, user profile ${ioc.createdBy}`}
          >
            {ioc.createdBy}
          </InternalLink>
        )
      };

      // Show START/STOP components only when IOC was deployed SUCCESSFULLY
      const deploymentStatus = new DeploymentStatus(
        ioc?.activeDeployment,
        jobStatus
      );

      const showControls = deploymentStatus.wasSuccessful();
      if (user) {
        subset["IOC Service Controls"] = showControls ? (
          <IOCService {...{ ioc }} />
        ) : (
          <>IOC is not currently deployed</>
        );
      }

      return subset;
    },
    [jobStatus, user]
  );

  if (ioc) {
    const managedIOC = { ...ioc };

    const deployIocFormDefaults = {
      name: ioc.namingName || "",
      description: ioc.description || "",
      git: ioc.sourceUrl || "",
      gitProjectId: ioc.gitProjectId,
      reference: "",
      short_reference: "",
      netBoxHost: { fqdn: "", hostId: "" }
    };

    if (ioc.activeDeployment) {
      deployIocFormDefaults.reference =
        ioc.activeDeployment.sourceVersion || "";
      deployIocFormDefaults.short_reference =
        ioc.activeDeployment.sourceVersionShort || "";
      deployIocFormDefaults.netBoxHost = {
        fqdn: ioc?.activeDeployment?.host?.fqdn || "",
        hostId: ioc?.activeDeployment?.host?.hostId || ""
      };
    }

    let disabledDebployButtonTitle = "";
    if (ioc.operationInProgress) {
      disabledDebployButtonTitle =
        "There is an ongoing operation, you can not deploy the IOC right now";
    }

    let disabledUndeployButtonTitle = "";
    if (ioc.operationInProgress) {
      disabledUndeployButtonTitle =
        "There is an ongoing operation, you can not undeploy the IOC right now";
    }

    return (
      <Stack gap={2}>
        <IOCDetails
          ioc={managedIOC}
          getSubset={getSubset}
          buttons={
            <>
              <Tooltip title={disabledDebployButtonTitle}>
                <span>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      setDeployDialogOpen(true);
                    }}
                    disabled={ioc.operationInProgress}
                  >
                    {ioc.activeDeployment ? "Deploy revision" : "Deploy"}
                  </Button>
                </span>
              </Tooltip>
              {ioc.activeDeployment ? (
                <Tooltip title={disabledUndeployButtonTitle}>
                  <span>
                    <Button
                      variant="contained"
                      color="primary"
                      style={{ marginRight: 20 }}
                      onClick={() => {
                        setUndeployDialogOpen(true);
                      }}
                      disabled={ioc.operationInProgress}
                    >
                      Undeploy
                    </Button>
                  </span>
                </Tooltip>
              ) : (
                <></>
              )}
            </>
          }
        />
        <AccessControl
          allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
          renderNoAccess={() => <></>}
        >
          <JobSection ioc={ioc} />
          <DeployIOC
            open={deployDialogOpen}
            setOpen={setDeployDialogOpen}
            deployIocFormDefaults={deployIocFormDefaults}
            ioc={ioc}
            hasActiveDeployment={Boolean(ioc.activeDeployment)}
          />
          <UndeployIOC
            open={undeployDialogOpen}
            setOpen={setUndeployDialogOpen}
            ioc={ioc}
          />
        </AccessControl>
      </Stack>
    );
  }
  return null;
};
