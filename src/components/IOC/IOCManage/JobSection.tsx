import { useEffect } from "react";
import { Stack, Typography } from "@mui/material";
import { usePagination } from "@ess-ics/ce-ui-common";
import { useListJobsQuery, IocDetails } from "../../../store/deployApi";
import { initRequestParams } from "../../../api/initRequestParams";
import {
  DEFAULT_POLLING_INTERVAL_MILLIS,
  ROWS_PER_PAGE
} from "../../../constants";
import { JobTable } from "../../Job";

interface JobSectionProps {
  ioc: IocDetails;
}

export const JobSection = ({ ioc }: JobSectionProps) => {
  const { pagination, setPagination } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  const { data: jobs, isLoading } = useListJobsQuery(
    {
      ...initRequestParams({ pagination }),
      iocId: ioc.id
    },
    { pollingInterval: DEFAULT_POLLING_INTERVAL_MILLIS }
  );

  useEffect(() => {
    setPagination({ totalCount: jobs?.totalCount ?? 0 });
  }, [setPagination, jobs?.totalCount]);

  return (
    <Stack gap={2}>
      <Typography
        component="h2"
        variant="h3"
      >
        Jobs
      </Typography>
      <JobTable
        jobs={jobs?.jobs}
        pagination={pagination}
        onPage={setPagination}
        loading={isLoading}
      />
    </Stack>
  );
};
