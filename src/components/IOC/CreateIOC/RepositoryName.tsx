import { useState, useCallback, ChangeEvent } from "react";
import { Box, Stack, TextField, Typography } from "@mui/material";
import { string, func } from "prop-types";

const propTypes = {
  repoName: string,
  onRepoNameChange: func
};

// match from beginning to end
// starts with an alpha numeric character (a-Z 0-9)
// followed by 0 or more combinations of alphanumeric characters or - or _
// ends with an alpha numeric character (a-Z 0-9)
const REPO_NAME_REGEX = "^[a-z0-9](?:[a-z0-9_-]*[a-z0-9])?$";

interface RepositoryNameProps {
  repoName: string;
  onRepoNameChange: (name: string) => void;
}

export const RepositoryName = ({
  repoName,
  onRepoNameChange
}: RepositoryNameProps) => {
  const [valid, setValid] = useState(repoName || repoName.length === 0);

  const handleNameChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const reg = new RegExp(REPO_NAME_REGEX);
      const hasValidCharacters = reg.test(e.target.value);
      const hasValidLength =
        e.target.value.length >= 4 && e.target.value.length <= 20;
      const valid = hasValidCharacters && hasValidLength;
      setValid(valid);

      if (valid) {
        onRepoNameChange(e.target.value);
      } else {
        onRepoNameChange("");
      }
    },
    [onRepoNameChange]
  );

  return (
    <Stack
      width="100%"
      gap={1}
    >
      <Box>
        <TextField
          autoComplete="off"
          id="repositoryName"
          label="Git repository name"
          variant="outlined"
          fullWidth
          onChange={handleNameChange}
          error={!valid}
          helperText={
            !valid
              ? "Only lowercase alphanumeric chars, hyphens and underscores are allowed in Git repository name (min 4 and max 20 chars)"
              : null
          }
        />
        <Typography
          variant="body2"
          fontStyle="italic"
          id="iocTypeName-name-preview"
        >
          The Git repository name will follow the pattern: &nbsp;
          <Box
            sx={{ fontFamily: "Monospace" }}
            component="span"
          >
            e3-ioc-
            <Box
              sx={{ fontWeight: "bold", display: "inline" }}
              component="span"
            >
              {repoName ? repoName : "{git repository name}"}
            </Box>
          </Box>
        </Typography>
      </Box>
    </Stack>
  );
};

RepositoryName.propTypes = propTypes;
