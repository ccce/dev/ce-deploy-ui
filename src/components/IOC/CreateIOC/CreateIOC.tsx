import { useEffect, useState, KeyboardEvent } from "react";
import { useNavigate } from "react-router-dom";
import { RootPaper } from "@ess-ics/ce-ui-common";
import {
  Autocomplete,
  Button,
  CircularProgress,
  LinearProgress,
  Stack,
  TextField,
  Typography
} from "@mui/material";
import { RepositoryOptions } from "./RepositoryOptions";
import { WITHOUT_REPO } from "./RepositoryType";
import { RepositoryName } from "./RepositoryName";
import { useTypingTimer } from "../../../hooks/useTypingTimer";
import { useCustomSnackbar } from "../../common/snackbar";
import {
  GitProject,
  NameResponse,
  useCreateIocMutation,
  useLazyFetchIocByNameQuery,
  useLazyListProjectsQuery
} from "../../../store/deployApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";

export function CreateIOC() {
  const navigate = useNavigate();
  const { showSuccess } = useCustomSnackbar();
  const [namingEntity, setNamingEntity] = useState<NameResponse | null>(null);
  const [gitProject, setGitProject] = useState<GitProject | null>(null);
  const { value: nameQuery, onKeyUp: onNameKeyUp } = useTypingTimer({
    interval: 500
  });
  const { value: repoQuery, onKeyUp: onRepoKeyUp } = useTypingTimer({
    interval: 500
  });
  const [selectedRepoOption, setSelectedRepoOption] = useState(WITHOUT_REPO);
  const [repoName, setRepoName] = useState("");

  const [
    getAllowedGitProjects,
    { data: allowedGitProjects, isLoading: loadingAllowedGitProjects }
  ] = useLazyListProjectsQuery();

  const [createIoc, { data: ioc, isLoading, error }] = useCreateIocMutation();

  const [getNames, { data: names, isLoading: loadingNames }] =
    useLazyFetchIocByNameQuery();

  // create the ioc on form submit
  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const params = {
      createIoc: {
        gitProjectId: gitProject?.id,
        namingUuid: namingEntity ? namingEntity.uuid : undefined,
        repository_name: repoName ? repoName : undefined
      }
    };
    createIoc(params);
  };

  const handleSelectRepoOption = (option: string) => {
    setSelectedRepoOption(option);
    setGitProject({});
    setRepoName("");
  };

  // fetch new names when name query changes
  useEffect(() => {
    if (nameQuery) {
      getNames({ iocName: nameQuery });
    }
  }, [nameQuery, getNames]);

  useEffect(() => {
    if (repoQuery && repoQuery.length > 2) {
      getAllowedGitProjects({ query: repoQuery });
    }
  }, [repoQuery, getAllowedGitProjects]);

  // navigate home once ioc created
  useEffect(() => {
    if (ioc) {
      showSuccess(
        selectedRepoOption === WITHOUT_REPO
          ? "IOC created"
          : "IOC created with a repository"
      );
      navigate(`/iocs/${ioc.id}?&tab=Management`);
    }
  }, [ioc, showSuccess, selectedRepoOption, navigate]);

  return (
    <RootPaper
      sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      <Stack
        component="form"
        onSubmit={onSubmit}
        gap={2}
        aria-live="polite"
        width="600px"
      >
        <Typography variant="h2">Create new IOC</Typography>
        <RepositoryOptions
          selectedRepoOption={selectedRepoOption}
          onSelectRepoOption={handleSelectRepoOption}
        />
        <Autocomplete
          autoHighlight
          id="nameAutocomplete"
          value={namingEntity}
          options={nameQuery ? (names ?? []) : []}
          loading={loadingNames}
          clearOnBlur={false}
          getOptionLabel={(option) => option?.name ?? ""}
          renderInput={(params) => (
            <TextField
              {...params}
              label="IOC name"
              variant="outlined"
              required
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loadingNames ? (
                      <CircularProgress
                        color="inherit"
                        size={20}
                      />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </>
                )
              }}
              disabled={isLoading}
              helperText={namingEntity ? namingEntity.description : ""}
            />
          )}
          onChange={(_, value) => setNamingEntity(value)}
          onInputChange={(event) => {
            if (event) {
              onNameKeyUp(event as KeyboardEvent<HTMLInputElement>);
            }
          }}
          autoSelect
        />
        {selectedRepoOption === WITHOUT_REPO ? (
          <Autocomplete
            autoHighlight
            id="gitId"
            value={gitProject}
            options={repoQuery || gitProject ? (allowedGitProjects ?? []) : []}
            loading={loadingAllowedGitProjects}
            clearOnBlur={false}
            getOptionLabel={(option) => {
              return option?.url ?? "";
            }}
            onChange={(_, value) => setGitProject(value)}
            onInputChange={(event) => {
              if (event) {
                onRepoKeyUp(event as KeyboardEvent<HTMLInputElement>);
              }
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Git repository"
                variant="outlined"
                fullWidth
                required
                InputProps={{
                  ...params.InputProps,
                  endAdornment: (
                    <>
                      {loadingAllowedGitProjects ? (
                        <CircularProgress
                          color="inherit"
                          size={20}
                        />
                      ) : null}
                      {params.InputProps.endAdornment}
                    </>
                  )
                }}
                disabled={isLoading}
              />
            )}
            autoSelect
          />
        ) : (
          <RepositoryName
            repoName={repoName}
            onRepoNameChange={(name) => setRepoName(name)}
          />
        )}

        {error && <ApiAlertError error={error} />}
        {isLoading ? (
          <LinearProgress
            aria-busy="true"
            aria-label="Creating IOC, please wait"
          />
        ) : null}
        <Stack
          direction="row"
          justifyContent="flex-end"
        >
          <Button
            onClick={() => navigate("/")}
            color="primary"
            disabled={isLoading}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            type="submit"
            disabled={
              isLoading || !namingEntity || selectedRepoOption === WITHOUT_REPO
                ? !gitProject
                : !repoName
            }
          >
            Create
          </Button>
        </Stack>
      </Stack>
    </RootPaper>
  );
}
