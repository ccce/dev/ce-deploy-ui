import { string, func } from "prop-types";
import {
  FormControl,
  FormControlLabel,
  RadioGroup,
  Radio
} from "@mui/material";
import { WITHOUT_REPO, WITH_REPO } from "./RepositoryType";

const propTypes = {
  selectedRepoOption: string,
  onSelectRepoOption: func
};

interface RepositoryOptionsProps {
  selectedRepoOption: string;
  onSelectRepoOption: (option: string) => void;
}

export const RepositoryOptions = ({
  selectedRepoOption,
  onSelectRepoOption
}: RepositoryOptionsProps) => (
  <FormControl>
    <RadioGroup
      row
      aria-label="Choose option to create IOC with or without existing repository"
      name="repositoryOptions"
      value={selectedRepoOption}
      onChange={(event) => onSelectRepoOption(event.target.value)}
    >
      <FormControlLabel
        value={WITHOUT_REPO}
        control={<Radio />}
        label="Create IOC with existing repository"
      />
      <FormControlLabel
        value={WITH_REPO}
        control={<Radio />}
        label="Create IOC and a repository"
      />
    </RadioGroup>
  </FormControl>
);

RepositoryOptions.propTypes = propTypes;
