import { Navigate } from "react-router-dom";
import { IOCUndeployDialog } from "../IOCUndeployDialog";
import { IocDetails } from "../../../store/deployApi";
import { useIocUndeployJobMutation } from "../../../store/enhancedApi";

interface UndeployIOCProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  ioc: IocDetails;
}

export const UndeployIOC = ({ open, setOpen, ioc }: UndeployIOCProps) => {
  const [
    undeploy,
    { data: undeployment, error: unDeploymentError, isLoading, reset }
  ] = useIocUndeployJobMutation();

  if (!undeployment) {
    return (
      <IOCUndeployDialog
        open={open}
        setOpen={setOpen}
        submitCallback={undeploy}
        ioc={ioc}
        error={unDeploymentError}
        resetError={() => reset()}
        buttonDisabled={ioc.operationInProgress || isLoading}
      />
    );
  } else {
    return (
      <Navigate
        to={`/jobs/${undeployment.id}`}
        replace
      />
    );
  }
};
