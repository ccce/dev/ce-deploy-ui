import { composeStories } from "@storybook/react";
import * as stories from "../../../stories/components/common/IOC/IOCTable.stories";

const { AfterAsync } = composeStories(stories);
const textColumns = ["IOC name", "Description", "Host", "Network"];
const columns = ["Status"].concat(textColumns);
const firstRowData = [
  "Active 1 warnings 1 errors!",
  "CCCE:MOCK4",
  "---",
  "ccce-test-ioc-02",
  "CSLab-GeneralLab"
];

describe("IOCTable", () => {
  context("Populated Table", () => {
    beforeEach(() => {
      cy.mount(<AfterAsync />);
    });

    it("Has the correct columns", () => {
      cy.findAllByRole("columnheader").each(($el, index) => {
        cy.wrap($el).contains(columns[index], { matchCase: false });
      });
    });

    it("Truncates all text content", () => {
      cy.findAllByRole("row")
        .eq(1) // first row is headers, so get next index
        .find(".MuiTypography-noWrap")
        .should("have.length", textColumns.length - 1);
    });

    it("Displays correct content in third row", () => {
      cy.findAllByRole("row")
        .eq(4) // first row is headers, so get next index
        .each(($el, index) => {
          if (index === 0) {
            cy.get('[data-testid="Brightness1Icon"]')
              .eq(1)
              .should("have.attr", "aria-label", firstRowData[index]);
          } else {
            cy.wrap($el)
              .find("p")
              .should(($el) => {
                const text = $el.text().trim().toLowerCase();
                const expected = firstRowData[index].trim().toLowerCase();
                expect(text).to.equal(expected);
              });
          }
        });
    });
  });
});
