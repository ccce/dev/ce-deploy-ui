import {
  Table,
  InternalLink,
  EllipsisText,
  EmptyValue
} from "@ess-ics/ce-ui-common";
import {
  GridRenderCellParams,
  GridValueGetterParams
} from "@mui/x-data-grid-pro";
import { IOCDescription } from "./IOCDescription";
import { IOCStatus } from "../IOCStatus";
import { Host, Ioc, IocDetails } from "../../../store/deployApi";
import { OnPageParams, Pagination } from "../../../types/common";

const exploreIocsColumns = [
  {
    field: "status",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  {
    field: "namingName",
    headerName: "IOC name"
  },
  {
    field: "description",
    headerName: "Description"
  },
  {
    field: "host",
    headerName: "Host",
    width: "10ch",
    sortable: false,
    filterable: false
  },
  {
    field: "network",
    headerName: "Network",
    width: "10ch",
    renderCell: (params: GridRenderCellParams) => {
      return params.value ? (
        <EllipsisText>{params.value}</EllipsisText>
      ) : (
        <EmptyValue />
      );
    },
    valueGetter: (params: GridValueGetterParams) => {
      return params.value;
    }
  }
];

const hostDetailsColumns = [
  {
    field: "status",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  {
    field: "namingName",
    headerName: "IOC name",
    sortable: false
  },
  {
    field: "description",
    headerName: "Description",
    sortable: false,
    flex: 3
  }
];

function createHostLink(host?: Host) {
  if (host?.hostId) {
    return (
      <InternalLink
        to={`/hosts/${host?.hostId}`}
        label={`Host details, ${host?.hostName}`}
        width="100%"
      >
        {host?.hostName ? (
          <EllipsisText>{host.hostName}</EllipsisText>
        ) : (
          <EmptyValue />
        )}
      </InternalLink>
    );
  }

  return <EmptyValue />;
}

function createIocLink(id?: number, name?: string) {
  return (
    <InternalLink
      to={`/iocs/${id}`}
      label={`IOC details, ${name}`}
      width="100%"
    >
      <EllipsisText>{name}</EllipsisText>
    </InternalLink>
  );
}

function createTableRowForHostDetails(ioc: IocDetails) {
  const { id, namingName } = ioc;

  return {
    id: id,
    status: <IOCStatus id={ioc.id} />,
    namingName: createIocLink(id, namingName),
    description: <IOCDescription id={ioc.id} />
  };
}

function createTableRowForExploreIocs(ioc: IocDetails) {
  const { id, namingName, activeDeployment } = ioc;

  return {
    id: id,
    status: <IOCStatus id={id} />,
    namingName: createIocLink(id, namingName),
    description: <IOCDescription id={ioc.id} />,
    host: createHostLink(activeDeployment?.host),
    network: activeDeployment?.host?.network
  };
}

interface IOCTableProps {
  iocs: Ioc[];
  rowType?: "host" | "explore";
  loading: boolean;
  pagination: Pagination;
  onPage: (page: OnPageParams) => void;
}

type TableTypeSpecificsType = Record<
  "host" | "explore",
  [
    typeof hostDetailsColumns | typeof exploreIocsColumns,
    typeof createTableRowForHostDetails | typeof createTableRowForExploreIocs
  ]
>;

export const IOCTable = ({
  iocs,
  rowType = "explore",
  loading,
  pagination,
  onPage
}: IOCTableProps) => {
  const tableTypeSpecifics: TableTypeSpecificsType = {
    host: [hostDetailsColumns, createTableRowForHostDetails],
    explore: [exploreIocsColumns, createTableRowForExploreIocs]
  };
  const [columns, createRow] = tableTypeSpecifics[rowType];
  const rows = iocs?.map((ioc) => createRow(ioc));

  return (
    <Table
      columns={columns}
      rows={rows}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
    />
  );
};
