import { Skeleton } from "@mui/material";
import { EllipsisText, EmptyValue } from "@ess-ics/ce-ui-common";
import { useGetIocDescriptionQuery } from "../../../store/deployApi";

export const IOCDescription = ({ id }: { id?: number }) => {
  const { data: iocDescriptionResponse, isLoading } = useGetIocDescriptionQuery(
    { iocId: id || 0 },
    { skip: !id }
  );
  const description = iocDescriptionResponse?.description;

  if (isLoading || !iocDescriptionResponse) {
    return <Skeleton width="100%" />;
  }

  return (
    <>
      {description ? (
        <EllipsisText>{description}</EllipsisText>
      ) : (
        <EmptyValue />
      )}
    </>
  );
};
