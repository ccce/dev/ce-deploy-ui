import { composeStories } from "@storybook/react";
import * as stories from "../../../stories/components/common/IOC/IOCLiveStatus.stories";

const composeStory = composeStories(stories);

// @ts-expect-error TS2339
const { Default, LoggedIn } = composeStory;

function commonTests() {
  // Expect IOC name in badge
  cy.contains("CCCE:MOCK2");
  // Expected in Info table:
  cy.contains("correct_ioc"); // git reference
  cy.contains("Test IOC for CCCE"); // description
  cy.contains("ccce-test-ioc-01.cslab.esss.lu.se"); // deployed on

  // Expect alerts to be displayed
  cy.get("#ioc-alerts")
    .should("contain", "This is an error message")
    .and("contain", "This is a warning message");

  cy.get('#ioc-alerts a[href="https://www.google.com"]').contains("More Info", {
    matchCase: false
  });
}

describe("IOCLiveStatus", () => {
  context("when not logged in", () => {
    beforeEach(() => {
      cy.mount(<Default />);
    });

    it("displays the common components", () => {
      commonTests();
    });
  });

  context("when logged in", () => {
    beforeEach(() => {
      cy.mount(<LoggedIn />);
    });

    it("displays the logged in view", () => {
      commonTests();
      cy.contains("IOC log stream");
      cy.contains("Records");
    });
  });
});
