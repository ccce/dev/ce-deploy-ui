import { useCallback, useState, ChangeEvent } from "react";
import { Typography } from "@mui/material";
import { SimpleAccordion, EmptyValue } from "@ess-ics/ce-ui-common";
import { useSearchParams } from "react-router-dom";
import { IOCDetails } from "../IOCDetails";
import { LokiPanel } from "../../common/Loki/LokiPanel";
import { RecordSearch } from "../../records/RecordSearch";
import { GitRefLink } from "../../common/Git/GitRefLink";
import { AccessControl } from "../../auth/AccessControl";
import { IocDetails } from "../../../store/deployApi";

export function IOCLiveStatus({ ioc }: { ioc: IocDetails }) {
  const [searchParams] = useSearchParams();
  const [accordionState, setAccordionState] = useState({
    logStreamOpen: false,
    recordsOpen: !!searchParams.get("query")
  });

  const hostName = ioc.activeDeployment?.host?.hostName;
  const externalIdValid = ioc.activeDeployment?.host?.externalIdValid;

  const getSubset = useCallback((ioc: IocDetails) => {
    const subset = {
      Description: ioc.description,
      Revision: ioc.activeDeployment ? (
        <GitRefLink
          url={ioc.activeDeployment?.sourceUrl}
          displayReference={ioc?.activeDeployment?.sourceVersion}
          revision={ioc?.activeDeployment?.sourceVersion}
        />
      ) : (
        <EmptyValue />
      )
    };

    return subset;
  }, []);

  return (
    <>
      <IOCDetails
        ioc={ioc}
        getSubset={getSubset}
      />

      <AccessControl
        allowedRoles={["DeploymentToolAdmin", "DeploymentToolIntegrator"]}
        renderNoAccess={() => <></>}
      >
        {externalIdValid && (
          <SimpleAccordion
            summary={
              <Typography
                component="h2"
                variant="h3"
              >
                IOC log stream
              </Typography>
            }
            expanded={accordionState.logStreamOpen}
            onChange={(_: ChangeEvent, expanded: boolean) =>
              setAccordionState({ ...accordionState, logStreamOpen: expanded })
            }
          >
            {hostName && (
              <LokiPanel
                hostName={hostName}
                iocName={ioc.namingName}
                isExpanded={accordionState.logStreamOpen}
              />
            )}
          </SimpleAccordion>
        )}
        <SimpleAccordion
          summary={
            <Typography
              component="h2"
              variant="h3"
            >
              Records
            </Typography>
          }
          expanded={accordionState.recordsOpen}
          onChange={(_: ChangeEvent, expanded: boolean) =>
            setAccordionState({ ...accordionState, recordsOpen: expanded })
          }
        >
          <RecordSearch
            iocName={ioc.namingName}
            rowType="iocDetails"
            isExpanded={accordionState.recordsOpen}
          />
        </SimpleAccordion>
      </AccessControl>
    </>
  );
}
