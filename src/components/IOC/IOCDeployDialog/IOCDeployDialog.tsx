import {
  useCallback,
  useEffect,
  useState,
  useMemo,
  FormEvent,
  SyntheticEvent
} from "react";
import {
  Button,
  TextField,
  Typography,
  CircularProgress,
  Autocomplete,
  Stack
} from "@mui/material";
import { Dialog, GitRefAutocomplete } from "@ess-ics/ce-ui-common";
import {
  GitReference,
  HostInfoWithId,
  useLazyListHostsQuery,
  useLazyListTagsAndCommitIdsQuery
} from "../../../store/deployApi";
import { getErrorState } from "../../common/Alerts/AlertsData";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { ApiError, DeployIocFormDefaults } from "../../../types/common";
import { useIocDeployJobMutation } from "../../../store/enhancedApi";

export type StartJobMutationTrigger = ReturnType<
  typeof useIocDeployJobMutation
>[0];
interface IOCDeployDialogProps {
  iocId?: number;
  open: boolean;
  setOpen: (open: boolean) => void;
  submitCallback: StartJobMutationTrigger;
  hasActiveDeployment: boolean;
  deployIocFormDefaults?: DeployIocFormDefaults;
  error: ApiError;
  resetError: () => void;
  buttonDisabled: boolean;
}

type SearchMethodConstants = "EQUALS" | "CONTAINS";

export function IOCDeployDialog({
  open,
  setOpen,
  iocId,
  submitCallback,
  hasActiveDeployment,
  deployIocFormDefaults,
  error,
  resetError,
  buttonDisabled
}: IOCDeployDialogProps) {
  const [getHosts, { data: hosts, isFetching: loadingHosts }] =
    useLazyListHostsQuery();
  const [
    callgetTagOrCommitIds,
    {
      data: tagOrCommitIds = [],
      isFetching: loadingTagsAndCommitIds,
      error: tagOrCommitIdsError
    }
  ] = useLazyListTagsAndCommitIdsQuery();

  const getTagOrCommitIds = useCallback(
    (
      gitProjectId: number,
      reference: string,
      searchMethod: SearchMethodConstants
    ) => {
      callgetTagOrCommitIds({
        projectId: gitProjectId,
        reference,
        searchMethod
      });
    },
    [callgetTagOrCommitIds]
  );

  const initHost = useMemo(
    () => ({
      fqdn: deployIocFormDefaults?.netBoxHost?.fqdn,
      hostId: deployIocFormDefaults?.netBoxHost?.hostId
    }),
    [deployIocFormDefaults?.netBoxHost]
  );

  const [host, setHost] = useState(initHost);
  const [query, setQuery] = useState("");
  const [gitRepo, setGitRepo] = useState(deployIocFormDefaults?.git);
  const [revision, setRevision] = useState(deployIocFormDefaults?.reference);
  const gitProjectId = deployIocFormDefaults?.gitProjectId;
  const hasHostData = host?.hostId || host?.fqdn;

  const handleClose = () => {
    setOpen(false);
    setHost(initHost);
    resetError();
  };

  useEffect(() => {
    if (!hasHostData && query && query.length > 2) {
      getHosts({ text: `${query}` });
    }
  }, [query, getHosts, hasHostData]);

  useEffect(() => {
    if (gitProjectId) {
      getTagOrCommitIds(gitProjectId, "", "CONTAINS");
    }
  }, [gitProjectId, getTagOrCommitIds]);

  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (iocId) {
      submitCallback({
        iocId: iocId,
        deployJobRequest: {
          sourceVersion: revision,
          hostId: host.hostId
        }
      });
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      title={
        <Typography
          variant="h2"
          marginY={1}
        >
          {hasActiveDeployment ? "Deploy revision" : "Deploy"}
        </Typography>
      }
      content={
        <Stack
          component="form"
          onSubmit={onSubmit}
          gap={1}
        >
          <TextField
            autoComplete="off"
            id="git"
            label="Git repository"
            variant="standard"
            defaultValue={deployIocFormDefaults?.git || ""}
            fullWidth
            onChange={(event) => {
              setGitRepo(event.target.value);
              resetError();
            }}
            disabled
            required
          />
          <GitRefAutocomplete
            id="version"
            label="Revision"
            options={tagOrCommitIds}
            loading={loadingTagsAndCommitIds}
            disabled={!gitRepo || gitRepo.trim() === ""}
            autocompleteProps={{
              autoSelect: false,
              defaultValue: tagOrCommitIds.find(
                (tagOrCommit) =>
                  tagOrCommit.reference === deployIocFormDefaults?.reference
              )
            }}
            onGitQueryValueSelect={(
              _: SyntheticEvent<Element, Event>,
              value: GitReference
            ) => {
              setRevision(value?.reference);
              resetError();
            }}
            textFieldProps={{
              helperText:
                tagOrCommitIdsError &&
                getErrorState(tagOrCommitIdsError).message
            }}
          />

          <Autocomplete
            autoHighlight
            id="host"
            options={query ? (hosts?.netBoxHosts ?? []) : []}
            loading={loadingHosts}
            clearOnBlur={false}
            value={host}
            getOptionLabel={(option) => {
              return option?.fqdn ?? "";
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Host"
                variant="outlined"
                required
                InputProps={{
                  ...params.InputProps,
                  endAdornment: (
                    <>
                      {loadingHosts ? (
                        <CircularProgress
                          color="inherit"
                          size={20}
                        />
                      ) : null}
                      {params.InputProps.endAdornment}
                    </>
                  )
                }}
              />
            )}
            onChange={(_, value: HostInfoWithId | null) => {
              setHost({ fqdn: value?.fqdn, hostId: value?.hostId });
              resetError();
            }}
            onInputChange={(_, value) => setQuery(value)}
            disabled={hasActiveDeployment}
            autoSelect
            filterOptions={(options) => options}
          />

          {hasActiveDeployment ? (
            <Typography variant="body2">
              Hint: First undeploy IOC if you want to move to another host.
            </Typography>
          ) : (
            <></>
          )}

          {error && <ApiAlertError error={error} />}
          <Stack
            flexDirection="row"
            justifyContent="flex-end"
            gap={2}
            marginTop={1}
          >
            <Button
              onClick={handleClose}
              color="primary"
            >
              Cancel
            </Button>
            <Button
              color="primary"
              variant="contained"
              type="submit"
              disabled={!host || !revision || buttonDisabled}
            >
              Deploy
            </Button>
          </Stack>
        </Stack>
      }
    />
  );
}
