import { Grid, Box, Stack, Typography } from "@mui/material";
import {
  KeyValueTable,
  AlertBannerList,
  InternalLink,
  EmptyValue
} from "@ess-ics/ce-ui-common";
import { IOCStatus } from "../IOCStatus";
import { AccessControl } from "../../auth/AccessControl";
import { IocDetails, useFetchIocAlertsQuery } from "../../../store/deployApi";

interface IOCDetailsProps {
  ioc: IocDetails;
  getSubset: (
    ioc: IocDetails
  ) => Record<string, string | JSX.Element | undefined> | undefined;
  buttons?: JSX.Element;
}

export function IOCDetails({ ioc, getSubset, buttons }: IOCDetailsProps) {
  const subset = getSubset(ioc);

  const { data: alert } = useFetchIocAlertsQuery(
    { iocId: ioc?.id ?? 0 },
    { skip: !ioc.id }
  );

  return (
    <>
      <Grid
        container
        spacing={1}
      >
        {alert?.alerts && alert.alerts.length > 0 && (
          <Box sx={{ mt: (theme) => theme.spacing(2), width: "100%" }}>
            <AlertBannerList alerts={alert.alerts} />
          </Box>
        )}
        {buttons && (
          <Grid
            item
            xs={12}
          >
            <Box
              display="flex"
              flexDirection="row-reverse"
              p={2}
              m={1}
            >
              <AccessControl
                allowedRoles={[
                  "DeploymentToolAdmin",
                  "DeploymentToolIntegrator"
                ]}
                renderNoAccess={() => <> </>}
              >
                <>{buttons}</>
              </AccessControl>
            </Box>
          </Grid>
        )}
        <Grid
          item
          xs={12}
        >
          <Stack
            flexDirection="row"
            alignItems="center"
          >
            <Box
              sx={{
                marginRight: (theme) => theme.spacing(4),
                marginLeft: (theme) => theme.spacing(6)
              }}
            >
              <IOCStatus
                id={ioc.id}
                hideAlerts
              />
            </Box>
            <Box sx={{ flex: 1 }}>
              <Typography
                noWrap
                variant="subtitle1"
                component="p"
                color="textPrimary"
              >
                {ioc.namingName}
              </Typography>
              {ioc?.activeDeployment ? (
                <InternalLink
                  to={`/hosts/${ioc.activeDeployment?.host?.hostId}`}
                  label={`Host details, ${ioc.activeDeployment?.host?.fqdn}`}
                >
                  {ioc.activeDeployment?.host?.fqdn || "---"}
                </InternalLink>
              ) : (
                <EmptyValue />
              )}
            </Box>
          </Stack>

          <KeyValueTable
            obj={subset}
            variant="overline"
            sx={{ border: 0 }}
            valueOptions={{ headerName: "" }}
          />
        </Grid>
      </Grid>
    </>
  );
}
