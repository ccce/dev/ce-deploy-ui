import { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  Typography,
  Grid,
  Tooltip,
  LinearProgress
} from "@mui/material";
import { ConfirmationDialog } from "@ess-ics/ce-ui-common";
import { AccessControl } from "../../auth/AccessControl";
import { useUnDeployInDbMutation } from "../../../store/enhancedApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { IocDetails } from "../../../store/deployApi";

interface AdministerUndeploymentProps {
  ioc: IocDetails;
  buttonDisabled: boolean;
  setButtonDisabled: (value: boolean) => void;
}

export const AdministerUndeployment = ({
  ioc,
  buttonDisabled,
  setButtonDisabled
}: AdministerUndeploymentProps) => {
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);

  const [
    undeployInDB,
    { data: undeployResponse, error: undeployInDBError, isLoading }
  ] = useUnDeployInDbMutation();

  const onClose = useCallback(() => {
    setOpen(false);
  }, []);

  const onConfirm = useCallback(() => {
    if (ioc.id) {
      setButtonDisabled(true);
      undeployInDB({
        iocId: ioc.id
      });
      setButtonDisabled(false);
    }
  }, [undeployInDB, ioc, setButtonDisabled]);

  useEffect(() => {
    if (undeployResponse) {
      setOpen(false);
      navigate(-1);
    }
  }, [undeployResponse, navigate]);

  let disabledButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you cannot 'undeploy' the IOC right now";
  } else {
    if (!ioc.activeDeployment) {
      disabledButtonTitle = "IOC is not deployed";
    }
  }

  return (
    <AccessControl
      allowedRoles={["DeploymentToolAdmin"]}
      renderNoAccess={() => <></>}
    >
      <>
        <ConfirmationDialog
          title={
            <Typography
              variant="h2"
              marginY={1}
            >
              Administratively undeploy IOC
            </Typography>
          }
          content={
            <>
              <Typography component="span">
                Are you sure want to administer IOC as undeployed
                <Typography
                  fontFamily="monospace"
                  component="span"
                >
                  {" "}
                  {ioc.namingName}
                </Typography>{" "}
                ?
              </Typography>
              {isLoading && <LinearProgress />}
            </>
          }
          confirmText="Undeploy IOC"
          cancelText="Cancel"
          open={open}
          onClose={onClose}
          onConfirm={onConfirm}
        />
        <Box sx={{ pt: 2 }}>
          <Typography
            sx={{ my: 2.5 }}
            component="h2"
            variant="h3"
          >
            Change deployment status to not deployed (e.g. if IOC has been
            manually removed)
          </Typography>
          <Grid
            container
            spacing={1}
          >
            {undeployInDBError && (
              <Grid
                item
                xs={12}
              >
                <ApiAlertError error={undeployInDBError} />
              </Grid>
            )}
            <Grid
              item
              xs={12}
            >
              <Tooltip title={disabledButtonTitle}>
                <span>
                  <Button
                    onClick={() => setOpen(true)}
                    disabled={buttonDisabled || !ioc.activeDeployment}
                    color="error"
                    variant="contained"
                  >
                    SET DEPLOYMENT STATE TO NOT DEPLOYED
                  </Button>
                </span>
              </Tooltip>
            </Grid>
          </Grid>
        </Box>
      </>
    </AccessControl>
  );
};
