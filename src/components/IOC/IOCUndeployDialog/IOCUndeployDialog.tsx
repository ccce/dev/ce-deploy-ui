import { SyntheticEvent } from "react";
import { Stack, Typography, Button } from "@mui/material";
import { Dialog } from "@ess-ics/ce-ui-common";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";
import { IocDetails, IocUndeployJobApiArg } from "../../../store/deployApi";
import { ApiError } from "../../../types/common";

interface IOCUndeployDialogProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  submitCallback: (data: IocUndeployJobApiArg) => void;
  ioc: IocDetails;
  error: ApiError;
  resetError: () => void;
  buttonDisabled: boolean;
}

export function IOCUndeployDialog({
  open,
  setOpen,
  submitCallback,
  ioc,
  error,
  resetError,
  buttonDisabled
}: IOCUndeployDialogProps) {
  const handleClose = () => {
    setOpen(false);
    resetError();
  };

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    if (ioc.id) {
      submitCallback({
        iocId: ioc.id
      });
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      title={
        <Typography
          variant="h2"
          marginY={1}
        >
          Undeploy
        </Typography>
      }
      content={
        <Stack
          component="form"
          onSubmit={onSubmit}
          gap={1}
        >
          <Typography
            fontSize="small"
            fontStyle="italic"
            sx={{ marginBottom: "0.5rem" }}
          >
            Autosave files will not be affected and might need manual handling.
          </Typography>
          <Typography>
            Do you really want to undeploy {ioc.namingName} from{" "}
            {ioc?.activeDeployment?.host?.fqdn}?
          </Typography>
          {error && <ApiAlertError error={error} />}
          <Stack
            flexDirection="row"
            justifyContent="flex-end"
            gap={2}
            marginTop={1}
          >
            <Button
              onClick={handleClose}
              color="primary"
            >
              Cancel
            </Button>
            <Button
              color="primary"
              variant="contained"
              type="submit"
              disabled={!ioc || !!error || buttonDisabled}
            >
              Undeploy
            </Button>
          </Stack>
        </Stack>
      }
    />
  );
}
