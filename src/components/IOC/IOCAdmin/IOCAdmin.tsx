import { useState } from "react";
import { AdministerUndeployment } from "../AdministerUndeployment";
import { IOCDelete } from "../IOCDelete";
import { IOCDetailAdmin } from "../IOCDetailAdmin";
import { ChangeHostAdmin } from "../ChangeHostAdmin";
import { IocDetails } from "../../../store/deployApi";

export const IOCAdmin = ({ ioc }: { ioc: IocDetails }) => {
  const [buttonDisabled, setButtonDisabled] = useState(
    ioc.operationInProgress || false
  );
  return (
    <>
      <IOCDetailAdmin
        ioc={ioc}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={(value: boolean) => setButtonDisabled(value)}
      />
      {ioc.activeDeployment ? (
        <ChangeHostAdmin
          ioc={ioc}
          buttonDisabled={buttonDisabled}
          setButtonDisabled={(value) => setButtonDisabled(value)}
        />
      ) : null}
      <AdministerUndeployment
        ioc={ioc}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={(value) => setButtonDisabled(value)}
      />
      <IOCDelete
        ioc={ioc}
        buttonDisabled={buttonDisabled}
        setButtonDisabled={(value: boolean) => setButtonDisabled(value)}
      />
    </>
  );
};
