import { useEffect } from "react";
import { Grid } from "@mui/material";
import { getIOCStatus } from "./IOCStatusData";
import { Status } from "../../common/Status";
import {
  useFetchIocStatusQuery,
  useLazyFetchIocAlertsQuery
} from "../../../store/deployApi";

interface IOCStatusProps {
  id?: number;
  hideAlerts?: boolean;
}

export const IOCStatus = ({ id, hideAlerts }: IOCStatusProps) => {
  const [callFetchIocAlerts, { data: iocAlert }] = useLazyFetchIocAlertsQuery();

  const { data: iocStateStatus } = useFetchIocStatusQuery(
    { iocId: id || 0 },
    { skip: !id }
  );

  useEffect(() => {
    if (!hideAlerts && id) {
      callFetchIocAlerts({ iocId: id });
    }
  }, [callFetchIocAlerts, hideAlerts, id]);

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
    >
      {iocStateStatus && (
        <Status
          id={iocStateStatus.iocId}
          state={iocStateStatus}
          alert={iocAlert}
          hideAlerts={hideAlerts}
          getStatusFcn={getIOCStatus}
        />
      )}
    </Grid>
  );
};
