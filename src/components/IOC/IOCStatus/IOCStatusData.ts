import { IocAlertResponse, IocStatusResponse } from "../../../store/deployApi";
import { STATUS, SEVERITY } from "../../common/Status";

export const getIOCStatus = (
  state: IocStatusResponse,
  alert: IocAlertResponse | undefined
) => {
  const { isActive } = state;
  const alertSeverity = alert?.alertSeverity?.toLowerCase();

  if (
    isActive &&
    (alertSeverity === undefined || alertSeverity === SEVERITY.info)
  ) {
    // Active status / running
    return STATUS.active;
  } else if (
    isActive === false &&
    (alertSeverity === undefined || alertSeverity === SEVERITY.info)
  ) {
    // stopped/paused
    return STATUS.disabled;
  } else if (
    isActive &&
    alertSeverity !== undefined &&
    alertSeverity === SEVERITY.alert
  ) {
    // error
    return STATUS.alert;
  } else if (
    isActive &&
    alertSeverity !== undefined &&
    alertSeverity === SEVERITY.warning
  ) {
    // warning
    return STATUS.warning;
  } else if (
    (!isActive || isActive === null) &&
    (alertSeverity === undefined || alertSeverity === SEVERITY.info)
  ) {
    // Inactive status
    return STATUS.inactive;
  } else if (
    (!isActive || isActive === null) &&
    alertSeverity === SEVERITY.alert
  ) {
    // inactive with warning/error
    return STATUS.inactiveAlert;
  } else if (
    (!isActive || isActive === null) &&
    alertSeverity === SEVERITY.warning
  ) {
    return STATUS.inactiveWarning;
  } else {
    // unknown fallback state
    return null;
  }
};
