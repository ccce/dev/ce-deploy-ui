import { Navigate } from "react-router-dom";
import { IOCDeployDialog } from "../IOCDeployDialog";
import { IocDetails } from "../../../store/deployApi";
import { DeployIocFormDefaults } from "../../../types/common";
import { useIocDeployJobMutation } from "../../../store/enhancedApi";

interface DeployIOCProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  ioc: IocDetails;
  hasActiveDeployment: boolean;
  deployIocFormDefaults?: DeployIocFormDefaults;
}

export function DeployIOC({
  open,
  setOpen,
  ioc,
  hasActiveDeployment,
  deployIocFormDefaults
}: DeployIOCProps) {
  const [deploy, { data: deployment, error: deployError, isLoading, reset }] =
    useIocDeployJobMutation();

  if (!deployment) {
    return (
      <IOCDeployDialog
        open={open}
        setOpen={setOpen}
        iocId={ioc?.id}
        submitCallback={deploy}
        deployIocFormDefaults={deployIocFormDefaults}
        hasActiveDeployment={hasActiveDeployment}
        error={deployError}
        resetError={() => reset()}
        buttonDisabled={ioc.operationInProgress || isLoading}
      />
    );
  } else {
    return (
      <Navigate
        to={`/jobs/${deployment.id}`}
        replace
      />
    );
  }
}
