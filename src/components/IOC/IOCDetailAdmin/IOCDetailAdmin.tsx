import { useState, useEffect, useCallback, KeyboardEvent } from "react";
import { ConfirmationDialog } from "@ess-ics/ce-ui-common";
import {
  Box,
  Button,
  CircularProgress,
  TextField,
  Tooltip,
  Typography,
  Autocomplete
} from "@mui/material";
import { useTypingTimer } from "../../../hooks/useTypingTimer";
import { AccessControl } from "../../auth/AccessControl";
import {
  useLazyListProjectsQuery,
  useLazyFetchIocByNameQuery,
  IocDetails,
  NameResponse
} from "../../../store/deployApi";
import { useUpdateIocMutation } from "../../../store/enhancedApi";
import { ApiAlertError } from "../../common/Alerts/ApiAlertError";

interface IOCDetailAdminProps {
  ioc: IocDetails;
  buttonDisabled: boolean;
  setButtonDisabled: (value: boolean) => void;
}

export const IOCDetailAdmin = ({
  ioc,
  buttonDisabled,
  setButtonDisabled
}: IOCDetailAdminProps) => {
  const iocIsDeployed = Boolean(ioc.activeDeployment);
  const [open, setOpen] = useState(false);
  const [gitId, setGitId] = useState(ioc.gitProjectId);
  const { value: nameQuery, onKeyUp: onNameKeyUp } = useTypingTimer({
    interval: 500
  });
  const { value: repoQuery, onKeyUp: onRepoKeyUp } = useTypingTimer({
    interval: 500
  });
  const [name, setName] = useState<NameResponse | null>({
    uuid: ioc?.namingUuid,
    description: ioc?.description,
    name: ioc?.namingName
  });

  const [
    getAllowedGitProjects,
    { data: allowedGitProjects, isLoading: loadingAllowedGitProjects }
  ] = useLazyListProjectsQuery();

  const [getNames, { data: names, isLoading: loadingNames }] =
    useLazyFetchIocByNameQuery();

  const [updateIOC, { data: updatedIOC, error: updateError, reset }] =
    useUpdateIocMutation();

  useEffect(() => {
    if (updateError) {
      setButtonDisabled(false);
    }
  }, [updateError, setButtonDisabled]);

  const requiredDataMissing = useCallback(() => !gitId || !name, [gitId, name]);

  const noModification = useCallback(
    () =>
      gitId === ioc.gitProjectId &&
      name?.uuid === ioc.namingUuid &&
      name?.description === ioc.description &&
      name?.name === ioc.namingName,
    [gitId, name, ioc]
  );

  // when user clicks Submit button a dialog should open
  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setOpen(true);
  };

  const onConfirm = useCallback(() => {
    if (ioc?.id) {
      setButtonDisabled(true);
      updateIOC({
        iocId: ioc.id,
        iocUpdateRequest: {
          gitProjectId: gitId,
          namingUuid: name?.uuid
        }
      });
    }
  }, [updateIOC, ioc, name, gitId, setButtonDisabled]);

  useEffect(() => {
    if (updatedIOC) {
      setButtonDisabled(false);
    }
  }, [updatedIOC, setButtonDisabled]);

  useEffect(() => {
    // fetch git repos only if user has entered a text and it wasn't previously fetched
    if (repoQuery && repoQuery.length > 2) {
      getAllowedGitProjects({ query: repoQuery });
    }
  }, [repoQuery, getAllowedGitProjects]);

  useEffect(() => {
    if (nameQuery) {
      getNames({ iocName: nameQuery });
    }
  }, [nameQuery, getNames]);

  let nameDisabledTitle = "";
  if (iocIsDeployed) {
    nameDisabledTitle =
      "IOC cannot be renamed while deployed - please undeploy first";
  }

  function nameAutocomplete(disabled: boolean) {
    const isDisabled = disabled || iocIsDeployed;
    const loading = loadingNames && !isDisabled;
    return (
      <Tooltip title={nameDisabledTitle}>
        <Autocomplete
          autoHighlight
          id="namingName"
          options={nameQuery ? (names ?? []) : []}
          loading={loading}
          clearOnBlur={false}
          defaultValue={name}
          getOptionLabel={(option) => {
            return option?.name ?? "";
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="IOC name"
              variant="outlined"
              required
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loading ? (
                      <CircularProgress
                        color="inherit"
                        size={20}
                      />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </>
                )
              }}
            />
          )}
          onChange={(_event, value: NameResponse | null) => {
            setName(value);
            reset();
          }}
          onInputChange={(event) => {
            if (event) {
              onNameKeyUp(event as KeyboardEvent<HTMLInputElement>);
            }
          }}
          disabled={isDisabled}
          autoSelect
        />
      </Tooltip>
    );
  }

  function gitRepoAutocomplete(disabled: boolean) {
    const isDisabled = disabled || iocIsDeployed;
    const loading = loadingAllowedGitProjects && !isDisabled;
    return (
      <Autocomplete
        autoHighlight
        id="gitId"
        options={repoQuery || gitId ? (allowedGitProjects ?? []) : []}
        loading={loading}
        clearOnBlur={false}
        defaultValue={{
          id: gitId,
          url: ioc.sourceUrl
        }}
        getOptionLabel={(option) => {
          return option?.url ?? "";
        }}
        onChange={(_, value) => {
          setGitId(value?.id);
          reset();
        }}
        onInputChange={(event) => {
          if (event) {
            onRepoKeyUp(event as KeyboardEvent<HTMLInputElement>);
          }
        }}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Git repository"
            variant="outlined"
            fullWidth
            required
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {loading ? (
                    <CircularProgress
                      color="inherit"
                      size={20}
                    />
                  ) : null}
                  {params.InputProps.endAdornment}
                </>
              )
            }}
          />
        )}
        disabled={isDisabled}
        autoSelect
      />
    );
  }

  let disabledButtonTitle = "";
  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you can not modify the IOC right now";
  }

  return (
    <>
      <ConfirmationDialog
        title={
          <Typography
            variant="h2"
            marginY={1}
          >
            Modifying IOC
          </Typography>
        }
        content={
          <>
            <Typography component="span">
              Are you sure want to modify
              <Typography
                component="span"
                fontFamily="monospace"
              >
                {ioc.namingName}
              </Typography>{" "}
              ?
            </Typography>
          </>
        }
        confirmText="Modify IOC"
        cancelText="Cancel"
        open={open}
        onClose={() => setOpen(false)}
        onConfirm={onConfirm}
      />
      <Box sx={{ pt: 2 }}>
        <Typography
          sx={{ my: 2.5 }}
          component="h2"
          variant="h3"
        >
          Modify IOC
        </Typography>
        <form
          onSubmit={onSubmit}
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "column",
            gap: "0.5rem"
          }}
        >
          <AccessControl
            allowedRoles={["DeploymentToolAdmin"]}
            renderNoAccess={() => nameAutocomplete(true)}
          >
            {nameAutocomplete(iocIsDeployed)}
          </AccessControl>

          <AccessControl
            allowedRoles={["DeploymentToolAdmin"]}
            allowedUsersWithRoles={[
              { user: ioc.createdBy ?? "", role: "DeploymentToolIntegrator" }
            ]}
            renderNoAccess={() => gitRepoAutocomplete(true)}
          >
            {gitRepoAutocomplete(false)}
          </AccessControl>

          {updateError && <ApiAlertError error={updateError} />}

          <Tooltip title={disabledButtonTitle}>
            <span>
              <Button
                color="primary"
                variant="contained"
                disabled={
                  requiredDataMissing() ||
                  noModification() ||
                  buttonDisabled ||
                  ioc.operationInProgress
                }
                type="submit"
              >
                Modify IOC
              </Button>
            </span>
          </Tooltip>
        </form>
      </Box>
    </>
  );
};
