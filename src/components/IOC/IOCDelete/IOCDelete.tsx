import { useState, useCallback, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Box, Button, Grid, Tooltip, Typography } from "@mui/material";
import { ConfirmDangerActionDialog } from "@ess-ics/ce-ui-common";
import { useCustomSnackbar } from "../../common/snackbar";
import { AccessControl } from "../../auth/AccessControl";
import { useDeleteIocMutation } from "../../../store/enhancedApi";
import { IocDetails } from "../../../store/deployApi";
import { getErrorState } from "../../common/Alerts/AlertsData";

interface IOCDeleteProps {
  ioc: IocDetails;
  buttonDisabled: boolean;
  setButtonDisabled: (value: boolean) => void;
}

export const IOCDelete = ({
  ioc,
  buttonDisabled,
  setButtonDisabled
}: IOCDeleteProps) => {
  const navigate = useNavigate();
  const { showSuccess } = useCustomSnackbar();
  const [open, setOpen] = useState(false);
  const [deleteIOC, { isLoading, error, reset }] = useDeleteIocMutation();

  let disabledButtonTitle = "";

  if (buttonDisabled || ioc.operationInProgress) {
    disabledButtonTitle =
      "There is an ongoing operation, you can not delete the IOC right now";
  }

  const onClose = useCallback(() => {
    reset();
    setOpen(false);
  }, [setOpen, reset]);

  useEffect(() => {
    if (error) {
      setButtonDisabled(false);
    }
  }, [error, setButtonDisabled]);

  const onConfirm = useCallback(() => {
    if (ioc.id) {
      reset();
      setButtonDisabled(true);
      deleteIOC({ iocId: ioc.id })
        .unwrap()
        .then(() => {
          showSuccess(`IOC ${ioc.namingName} deleted`);
          navigate("/iocs", { replace: true });
        })
        .catch(() => setButtonDisabled(false));
    }
  }, [
    reset,
    setButtonDisabled,
    deleteIOC,
    ioc.id,
    ioc.namingName,
    showSuccess,
    navigate
  ]);

  return (
    <>
      <ConfirmDangerActionDialog
        title="Delete IOC"
        text="Type in the complete IOC name to confirm that you really want to delete the IOC."
        confirmText="Delete IOC"
        open={open}
        valueToCheck={ioc.namingName}
        onClose={onClose}
        onConfirm={onConfirm}
        isLoading={isLoading}
        error={error && getErrorState(error).message}
      />

      <Box sx={{ pt: 2 }}>
        <Typography
          sx={{ my: 2.5 }}
          component="h2"
          variant="h3"
        >
          Delete IOC
        </Typography>
        <Grid
          container
          spacing={1}
        >
          <Grid
            item
            xs={12}
          >
            <AccessControl
              allowedRoles={["DeploymentToolAdmin"]}
              renderNoAccess={() => (
                <Tooltip title={"Only admins can delete the IOC"}>
                  <span>
                    <Button
                      color="error"
                      variant="contained"
                      disabled
                    >
                      Delete IOC
                    </Button>
                  </span>
                </Tooltip>
              )}
            >
              <Tooltip title={disabledButtonTitle}>
                <span>
                  <Button
                    onClick={() => setOpen(true)}
                    disabled={
                      buttonDisabled ||
                      ioc.operationInProgress ||
                      Boolean(ioc.activeDeployment) ||
                      isLoading
                    }
                    color="error"
                    variant="contained"
                  >
                    Delete IOC
                  </Button>
                </span>
              </Tooltip>
            </AccessControl>
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
