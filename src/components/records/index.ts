import { RecordTable } from "./RecordTable";
import { RecordBadge } from "./RecordBadge";
import { RecordStatusIcon } from "./RecordIcons";
import { RecordSearch } from "./RecordSearch";

export { RecordTable, RecordBadge, RecordStatusIcon, RecordSearch };
export default RecordTable;
