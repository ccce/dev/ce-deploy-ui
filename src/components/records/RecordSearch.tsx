import { useEffect, useCallback, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { Grid, Tabs, Tab } from "@mui/material";
import { usePagination, SearchBar } from "@ess-ics/ce-ui-common";
import { RecordTable } from "./RecordTable";
import { initRequestParams } from "../../api/initRequestParams";
import { ROWS_PER_PAGE } from "../../constants";
import { useLazyFindAllRecordsQuery } from "../../store/deployApi";
import { Pagination } from "../../types/common";

interface RecordSearchProps {
  iocName?: string;
  rowType: "records" | "iocDetails" | undefined;
  isExpanded: boolean;
}

export function RecordSearch({
  iocName,
  rowType,
  isExpanded
}: RecordSearchProps) {
  const [getRecords, { data: records, isFetching }] =
    useLazyFindAllRecordsQuery();

  const [searchParams, setSearchParams] = useSearchParams({ query: "" });
  const [recordFilter, setRecordFilter] = useState<
    "ACTIVE" | "INACTIVE" | undefined
  >(undefined);
  const [tabIndex, setTabIndex] = useState(0);

  const handleTabChange = (tab: number) => {
    if (tab === 0) {
      setRecordFilter(undefined);
    } else if (tab === 1) {
      setRecordFilter("ACTIVE");
    } else if (tab === 2) {
      setRecordFilter("INACTIVE");
    }
    setTabIndex(tab);
  };

  const { pagination, setPagination, setTotalCount } = usePagination({
    rowsPerPageOptions: ROWS_PER_PAGE,
    initLimit: ROWS_PER_PAGE[0],
    initPage: 0
  });

  // update pagination whenever search result total pages change
  useEffect(() => {
    setTotalCount(records?.totalCount ?? 0);
  }, [records?.totalCount, setTotalCount]);

  // Request new search results whenever search or pagination changes
  useEffect(() => {
    if (isExpanded) {
      const requestParams = initRequestParams({ pagination });

      getRecords({
        pvStatus: recordFilter,
        text: searchParams.get("query") || undefined,
        iocName: iocName,
        ...requestParams,
        limit: requestParams.limit.toString()
      });
    }
  }, [getRecords, recordFilter, searchParams, pagination, iocName, isExpanded]);

  // Callback for searchbar, called whenever user updates search
  const setSearch = useCallback(
    (query: string) => {
      setSearchParams({ query }, { replace: true });
    },
    [setSearchParams]
  );

  // Invoked by Table on change to pagination
  const onPage = (params: Pagination) => {
    setPagination(params);
  };

  return (
    <Grid
      container
      spacing={1}
      flexDirection={"column"}
    >
      <Grid item>
        <Tabs
          value={tabIndex}
          onChange={(_, tab) => handleTabChange(tab)}
        >
          <Tab label="All" />
          <Tab label="Only active" />
          <Tab label="Only inactive" />
        </Tabs>
      </Grid>
      <Grid
        mt={1}
        item
      >
        <SearchBar
          search={setSearch}
          query={searchParams.get("query")}
          loading={isFetching}
        >
          <RecordTable
            records={records}
            loading={isFetching}
            rowType={rowType}
            pagination={pagination}
            onPage={onPage}
          />
        </SearchBar>
      </Grid>
    </Grid>
  );
}
