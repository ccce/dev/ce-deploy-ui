import {
  Table,
  InternalLink,
  EllipsisText,
  EmptyValue
} from "@ess-ics/ce-ui-common";
import { Grid } from "@mui/material";
import { RecordStatusIcon } from "./RecordIcons";
import { RecordHostLink } from "./RecordHostLink";
import { Pagination } from "../../types/common";
import {
  PagedRecordResponse,
  Record as ApiRecord
} from "../../store/deployApi";

const recordsColumns = [
  {
    field: "bulb",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  { field: "name", headerName: "Record", minWidth: 200, sortable: false },
  {
    field: "description",
    headerName: "Description",
    width: "20ch",
    sortable: false
  },
  { field: "iocName", headerName: "IOC name", width: "10ch", sortable: false },
  { field: "hostName", headerName: "Host", width: "10ch", sortable: false }
];

const iocDetailsColumns = [
  {
    field: "bulb",
    headerName: "Status",
    flex: 0,
    headerAlign: "center",
    align: "center"
  },
  {
    field: "name",
    headerName: "Record",
    minWidth: 200,
    sortable: false
  },
  {
    field: "description",
    headerName: "Description",
    width: "20ch",
    sortable: false
  },
  { field: "iocVersion", headerName: "Version", width: "15ch", sortable: false }
];

function createRecordName(record: ApiRecord) {
  return (
    <InternalLink
      to={`/records/${encodeURIComponent(record?.name || "")}`}
      label={`Record details, ${record.name}`}
      width="100%"
    >
      <EllipsisText
        TooltipProps={{
          PopperProps: {
            modifiers: [
              {
                name: "offset",
                options: {
                  offset: [100, 0]
                }
              }
            ]
          }
        }}
      >
        {record.name}
      </EllipsisText>
    </InternalLink>
  );
}

function createRecordDescription(description?: string) {
  return (
    <>
      {description ? (
        <EllipsisText>{description}</EllipsisText>
      ) : (
        <EmptyValue />
      )}
    </>
  );
}

function createIocLink(record: ApiRecord) {
  // IOC ID only exists if it has been created in the DB -> avoid linking if it hasn't been created
  if (record.iocId) {
    return (
      <InternalLink
        to={`/iocs/${record.iocId}`}
        label={`Record IOC Details, ${record.iocName}`}
        width="100%"
      >
        <EllipsisText>{record.iocName}</EllipsisText>
      </InternalLink>
    );
  }

  return record.iocName;
}

function createRecordsRow(record: ApiRecord) {
  return {
    id: record.name,
    bulb: (
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <RecordStatusIcon record={record} />
      </Grid>
    ),
    name: createRecordName(record),
    description: createRecordDescription(record?.description),
    iocName: createIocLink(record),
    hostName: <RecordHostLink fqdn={record.hostName} />
  };
}

function createIocDetailsRow(record: ApiRecord) {
  return {
    id: record.name,
    bulb: (
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <RecordStatusIcon record={record} />
      </Grid>
    ),
    name: createRecordName(record),
    description: createRecordDescription(record.description),
    iocVersion: <EllipsisText>{record.iocVersion}</EllipsisText>
  };
}

interface RecordTableProps {
  records?: PagedRecordResponse;
  rowType?: "records" | "iocDetails";
  loading: boolean;
  pagination: Pagination;
  onPage: (params: Pagination) => void;
}

type TableTypeSpecificsType = {
  records: [typeof recordsColumns, typeof createRecordsRow];
  iocDetails: [typeof iocDetailsColumns, typeof createIocDetailsRow];
};

export function RecordTable({
  records,
  rowType = "records",
  loading,
  pagination,
  onPage
}: RecordTableProps) {
  const tableTypeSpecifics: TableTypeSpecificsType = {
    records: [recordsColumns, createRecordsRow],
    iocDetails: [iocDetailsColumns, createIocDetailsRow]
  };

  const [columns, createRow] = tableTypeSpecifics[rowType];
  const rows = (records?.recordList ?? []).map((record: ApiRecord) =>
    createRow(record)
  );

  return (
    <Table
      columns={columns}
      rows={rows}
      pagination={pagination}
      onPage={onPage}
      loading={loading}
      slotProps={{
        pagination: { showFirstButton: true, showLastButton: false }
      }}
    />
  );
}
