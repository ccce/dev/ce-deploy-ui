import { SvgIconTypeMap, useTheme } from "@mui/material";
import { LabeledIcon } from "@ess-ics/ce-ui-common";
import { OverridableComponent } from "@mui/material/OverridableComponent";
import {
  Brightness1,
  RadioButtonUnchecked,
  HelpOutline
} from "@mui/icons-material";
import { Record as ApiRecord } from "../../store/deployApi";

interface RecordStatusIconProps {
  record: ApiRecord;
}

export function RecordStatusIcon({ record }: RecordStatusIconProps) {
  const theme = useTheme();

  const { pvStatus } = record;

  const iconConfig: Record<
    string,
    {
      title: string;
      icon: OverridableComponent<SvgIconTypeMap<"", "svg">> & {
        muiName: string;
      };
    }
  > = {
    active: {
      title: "Active",
      icon: Brightness1
    },
    inactive: {
      title: "Inactive",
      icon: RadioButtonUnchecked
    },
    unknown: {
      title: "Unknown",
      icon: HelpOutline
    }
  };

  const state = pvStatus ? pvStatus.toLowerCase() : "unknown";
  const iconStyle = { fill: theme.palette.status.icons };
  const iconTitle = iconConfig[state].title;
  const statusIcon = iconConfig[state].icon;

  return (
    <LabeledIcon
      label={iconTitle}
      labelPosition="tooltip"
      Icon={statusIcon}
      IconProps={{ style: iconStyle }}
    />
  );
}
