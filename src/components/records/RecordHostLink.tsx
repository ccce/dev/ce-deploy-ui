import { Grid, Skeleton, Typography } from "@mui/material";
import { InternalLink, EllipsisText, EmptyValue } from "@ess-ics/ce-ui-common";
import {
  HostInfoWithId,
  useFindNetBoxHostByFqdnQuery
} from "../../store/deployApi";

function createHostLink(hostInfo?: HostInfoWithId, fqdn?: string) {
  // if the fqdn field is empty for some reason
  if (fqdn === null || fqdn?.trim() === "") {
    return <EmptyValue />;
  }

  // create host link only if the host exists in CSEntry
  if (hostInfo) {
    return (
      <InternalLink
        to={`/hosts/${hostInfo.hostId}`}
        label={`Record host details, ${fqdn}}`}
        width="100%"
      >
        <EllipsisText>{fqdn?.split(".")[0]}</EllipsisText>
      </InternalLink>
    );
  }

  return <Typography>{fqdn}</Typography>;
}

interface RecordHostLinkProps {
  fqdn?: string;
}

export const RecordHostLink = ({ fqdn }: RecordHostLinkProps) => {
  const { data: hostInfo, isLoading } = useFindNetBoxHostByFqdnQuery(
    { fqdn: fqdn ?? "" },
    { skip: !fqdn }
  );

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
    >
      {isLoading ? <Skeleton width="100%" /> : createHostLink(hostInfo, fqdn)}
    </Grid>
  );
};
