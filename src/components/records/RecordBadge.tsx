import { IconBadge, InternalLink } from "@ess-ics/ce-ui-common";
import { RecordStatusIcon } from "./RecordIcons";
import { Record } from "../../store/deployApi";

interface RecordBadgeProps {
  record: Record;
}

export function RecordBadge({ record }: RecordBadgeProps) {
  return (
    <IconBadge
      icon={<RecordStatusIcon record={record} />}
      title={record.name}
      subtitle={
        <InternalLink
          to={`/iocs/${record.iocId}`}
          label={`IOC Details, ${record?.iocName}`}
        >
          {record?.iocName}
        </InternalLink>
      }
    />
  );
}
