import { useContext } from "react";
import { userContext, IconMenuButton } from "@ess-ics/ce-ui-common";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../../types/common";

export const CreateIOCButton = () => {
  const { user } = useContext<UserContext>(userContext);
  const navigate = useNavigate();

  const iconMenuButtonProps = {
    menuItems: [
      {
        text: "New IOC",
        action: () => navigate("/iocs/create")
      }
    ]
  };

  return user ? <IconMenuButton {...iconMenuButtonProps} /> : null;
};
