import { Fragment, ReactElement, ReactNode, useState } from "react";
import {
  GlobalAppBar,
  IconMenuButton,
  EssIconLogo,
  BodyBox,
  useUniqueKeys,
  MaintenanceModeBanner
} from "@ess-ics/ce-ui-common";
import {
  Assignment,
  HelpCenter,
  SettingsInputComponent,
  Storage
} from "@mui/icons-material";
import {
  Box,
  Divider,
  IconButton,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Tooltip
} from "@mui/material";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { LoginControls } from "./LoginControls";
import { CreateIOCButton } from "./CreateIOCButton";
import { applicationTitle } from "../applicationTitle";
import { CCCEControlSymbol } from "../../../icons/CCCEControlSymbol";
import { theme } from "../../../style/Theme";
import { useGetCurrentModeQuery } from "../../../store/deployApi";

interface MenuListItemProps {
  url: string;
  icon: ReactElement;
  text: string;
  tooltip: boolean;
}

function MenuListItem({ url, icon, text, tooltip }: MenuListItemProps) {
  const currentUrl = `${window.location}`;

  return (
    <Tooltip
      title={tooltip ? text : ""}
      placement="right"
      arrow
    >
      <ListItemButton
        component={Link}
        to={url}
        selected={currentUrl.split("?")[0].endsWith(url)}
      >
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText
          primary={text}
          primaryTypographyProps={{ variant: "button" }}
        />
      </ListItemButton>
    </Tooltip>
  );
}

interface MenuListItemsProps {
  drawerOpen: boolean;
  menuItems: typeof menuItemsAll;
}

function MenuListItems({ drawerOpen, menuItems }: MenuListItemsProps) {
  const menuItemsKeys = useUniqueKeys(menuItems);
  return (
    <Box sx={{ paddingTop: 3 }}>
      {menuItems.map(({ text, url, icon }, i) => (
        <Fragment key={menuItemsKeys[i]}>
          {typeof (text && url && icon) === "undefined" ? (
            <Divider sx={{ marginTop: 5 }} />
          ) : (
            <MenuListItem
              tooltip={!drawerOpen}
              url={url}
              icon={icon}
              text={text}
            />
          )}
        </Fragment>
      ))}
    </Box>
  );
}

const makeLink = (text: string, url: string, icon: ReactElement) => ({
  text,
  url,
  icon
});

const menuItemsAll = [
  makeLink("Records", "/records", <SettingsInputComponent />),
  makeLink(
    "IOCs",
    "/iocs",
    <CCCEControlSymbol
      width="25px"
      height="25px"
    />
  ),
  makeLink("IOC hosts", "/hosts", <Storage />),
  makeLink("Log", "/jobs", <Assignment />)
];

interface NavigationMenuProps {
  children: ReactNode;
}

export const NavigationMenu = ({ children }: NavigationMenuProps) => {
  const [drawerOpen, setDrawerOpen] = useState(false);
  const navigate = useNavigate();
  const goHome = () => {
    navigate("/");
  };

  const helpButtonProps = {
    icon: <HelpCenter />,
    menuItems: [
      {
        text: "Help",
        action: () => navigate("/help")
      }
    ]
  };

  const { data: maintenanceMode } = useGetCurrentModeQuery();

  const args = {
    defaultHomeButton: (
      <IconButton
        edge="start"
        color="inherit"
        onClick={goHome}
      >
        <EssIconLogo />
      </IconButton>
    ),
    defaultTitle: applicationTitle(),
    defaultActionButton: <LoginControls />,
    defaultOpen: false,
    widthOpen: "170px", // default size 240px
    widthClosed: "57px",
    defaultIconMenuButton: <CreateIOCButton />,
    defaultHelpButton: <IconMenuButton {...helpButtonProps} />
  };

  return (
    <GlobalAppBar
      getDrawerOpen={setDrawerOpen}
      menuItems={
        <MenuListItems
          drawerOpen={drawerOpen}
          menuItems={menuItemsAll}
        />
      }
      {...args}
    >
      <>
        {maintenanceMode ? (
          <BodyBox
            sx={{
              width: "100%",
              maxWidth: "1400px",
              paddingBottom: theme.spacing(2)
            }}
          >
            <MaintenanceModeBanner {...maintenanceMode} />
          </BodyBox>
        ) : (
          <></>
        )}
        {children}
      </>
    </GlobalAppBar>
  );
};
