import { useRef, useContext, useCallback, useEffect, useState } from "react";
import { string, arrayOf } from "prop-types";
import { Button, Avatar, Chip } from "@mui/material";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import PersonIcon from "@mui/icons-material/Person";
import {
  userContext,
  LoginDialog,
  IconMenuButton
} from "@ess-ics/ce-ui-common";
import { useNavigate } from "react-router-dom";
import { User, UserContext } from "../../../types/common";

interface ProfileMenuProps {
  user: User;
}

export const ProfileMenu = ({ user }: ProfileMenuProps) => {
  const { userRoles, logout } = useContext<UserContext>(userContext);
  const navigate = useNavigate();

  const objUserRoles = (userRoles ?? [])
    ?.filter((role) => role.includes("DeploymentTool"))
    ?.map((role) => ({
      text: (
        <Chip
          label={role}
          color="primary"
        />
      )
    }));

  function userProfile() {
    navigate(`user/${user.loginName}`);
  }

  const profileMenuProps = {
    /*
        If editing this id, be sure to edit the check for this in
        IconMenuButton.js in ce-ui-common repo as well to
        match
      */
    id: "user-login-profile-",
    icon: (
      <Avatar
        alt={user.fullName}
        src={user.avatar}
        sx={{
          width: (theme) => theme.spacing(3),
          height: (theme) => theme.spacing(3)
        }}
      />
    ),
    menuItems: [
      ...objUserRoles,
      {
        text: "User profile",
        action: () => userProfile(),
        icon: <PersonIcon fontSize="small" />
      },
      {
        text: "Logout",
        action: () => logout(),
        icon: <LockOpenIcon fontSize="small" />
      }
    ]
  };

  return <IconMenuButton {...profileMenuProps} />;
};

ProfileMenu.prototypes = {
  user: {
    fullName: string.isRequired,
    loginName: string.isRequired,
    avatar: string.isRequired,
    email: string,
    gitlabUserName: string.isRequired,
    roles: arrayOf(string).isRequired
  }
};

export const LoginControls = () => {
  const [loginFormOpen, setLoginFormOpen] = useState(false);
  const { user, login, loginError, loginLoading, resetLoginError } =
    useContext<UserContext>(userContext);
  const userRef = useRef(user);

  const navigate = useNavigate();
  const handleClose = useCallback(() => {
    setLoginFormOpen(false);
  }, []);

  // detect logout and redirect to login
  useEffect(() => {
    if (userRef.current && !user) {
      navigate("/login");
    }
    userRef.current = user;
  }, [user, navigate]);

  useEffect(() => {
    if (user) {
      handleClose();
    }
  }, [handleClose, user]);

  return user ? (
    <ProfileMenu user={user} />
  ) : (
    <>
      <Button
        color="inherit"
        onClick={() => {
          resetLoginError();
          setLoginFormOpen(true);
        }}
      >
        Login
      </Button>
      <LoginDialog
        login={login}
        open={loginFormOpen}
        handleClose={handleClose}
        error={loginError}
        loading={loginLoading}
      />
    </>
  );
};
