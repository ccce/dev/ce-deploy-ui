export const isNotFound = (status: string | number | null | undefined) =>
  status === "404" || status === 404;
