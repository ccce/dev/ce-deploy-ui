/**
 * NotFound
 * when page not found go (redirect) home ("/")
 */
import {
  ServerErrorPage,
  RootPaper,
  SidewaysMascot,
  VerticalMascot
} from "@ess-ics/ce-ui-common";
import { env } from "../../../config/env";

interface NotFoundProps {
  /** String containing message of page not found. Otherwise defaults to a generic "Page Not Found" message */
  message?: string;
  status?: string | number;
}

const mascotDefaultProps = { width: "250px", height: "500px" };
const errorPageProps = {
  mascotVariantL: <SidewaysMascot {...mascotDefaultProps} />,
  mascotVariantR: <VerticalMascot {...mascotDefaultProps} />
};

export const NotFoundView = ({
  message = "Page not found",
  status = "404"
}: NotFoundProps) => {
  return (
    <RootPaper>
      <ServerErrorPage
        status={status}
        message={message}
        supportHref={env.SUPPORT_URL}
        errorPageProps={errorPageProps}
      />
    </RootPaper>
  );
};
