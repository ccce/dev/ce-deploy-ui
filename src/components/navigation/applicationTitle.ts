import { env } from "../../config/env";

function applicationSubTitle() {
  const title = `${env.ENVIRONMENT_TITLE}`;

  if (title && title !== "undefined") {
    return " - " + title;
  }

  return "";
}

export function applicationTitle(title: string = "") {
  return [`CE deploy & monitor ${applicationSubTitle()}`, title].join(" / ");
}
