import { useState, useRef, useEffect } from "react";
import { Alert, LinearProgress, Stack } from "@mui/material";
import { LogStreamConsole } from "../common/LogStream/LogStreamConsole";
import { LogStreamConsoleDialog } from "../common/LogStream/LogStreamConsoleDialog";
import { PopoutButton } from "../common/Buttons/PopoutButton";
import { getErrorState } from "../common/Alerts/AlertsData";
import { JobDetails, useLazyFetchAwxJobLogQuery } from "../../store/deployApi";
import { DEFAULT_POLLING_INTERVAL_MILLIS } from "../../constants";

interface DeploymentJobOutputProps {
  job: JobDetails;
  isExpanded: boolean;
}
export function DeploymentJobOutput({
  job,
  isExpanded
}: DeploymentJobOutputProps) {
  const [consoleDialogOpen, setConsoleDialogOpen] = useState(false);
  const finalResultsNeeded = useRef(true);
  const [getLogById, { data: log, isSuccess: logDataReady, error: logError }] =
    useLazyFetchAwxJobLogQuery({
      pollingInterval: isExpanded ? DEFAULT_POLLING_INTERVAL_MILLIS : 0
    });

  useEffect(() => {
    if (
      !isExpanded ||
      (job.finishedAt && !finalResultsNeeded.current) ||
      !job.id
    ) {
      return;
    }

    getLogById({ jobId: job.id });
    finalResultsNeeded.current = !job.finishedAt;
  }, [job.finishedAt, job, isExpanded, getLogById, job.awxJobId]);

  const showLoading = !log || !job.startTime;

  useEffect(() => {
    finalResultsNeeded.current = true;
  }, [job]);

  if (logError) {
    return (
      <Stack>
        <Alert severity="error">{getErrorState(logError).message}</Alert>
      </Stack>
    );
  }

  if (showLoading) {
    return (
      <Stack>
        <LinearProgress color="primary" />
      </Stack>
    );
  }

  return (
    <Stack>
      <Stack
        display="flex"
        flexDirection="row"
        justifyContent="flex-end"
        sx={{ marginBottom: (theme) => theme.spacing(2) }}
      >
        <PopoutButton
          title="Popout log"
          onButtonClick={() => setConsoleDialogOpen(true)}
        />
      </Stack>
      <LogStreamConsoleDialog
        title="Job log stream"
        loading={showLoading}
        open={consoleDialogOpen}
        log={log?.stdoutHtml}
        dataReady={logDataReady}
        onDialogClose={() => setConsoleDialogOpen(false)}
      />
      <LogStreamConsole
        log={log?.stdoutHtml}
        dataReady={logDataReady}
        height="500px"
      />
    </Stack>
  );
}
