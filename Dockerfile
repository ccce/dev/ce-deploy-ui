# build deploy & monitor frontend
FROM node:20.10.0 as builder
ARG REACT_APP_MUI_PRO_LICENSE_KEY
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY . /usr/src/app
RUN npm ci

RUN VITE_APP_MUI_PRO_LICENSE_KEY=${REACT_APP_MUI_PRO_LICENSE_KEY} npm run ci:build

# production environment
FROM nginx:1.19.6-alpine
RUN rm -rf /etc/nginx/conf.d
RUN mkdir /etc/nginx/conf.d
COPY docker/default.conf /etc/nginx/conf.d
COPY --from=builder /usr/src/app/build /usr/share/nginx/html/
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
