# Development

## Installing dependencies

This project includes a private dependency on a library called [ce-ui-common](https://artifactory.esss.lu.se/ui/repos/tree/General/ics-npm/@ess-ics/ce-ui-common/-) in the Artifactory, for which additional configuration is required.

1.  Create an `.npmrc` file based on the example provided in this registry, substituting your email and base64 credentials, and place it in HOME.
2.  Install dependencies with
    ```shell
    npm ci
    ```

Note, we lock dependency versions with [package-lock.json](package-lock.json); please install dependencies with `npm ci` (not npm install) to avoid overwriting the package-lock.

## Updating the API files

This project uses `@rtk-query/codegen-openapi` to auto-generate the deploy api from its OpenAPI spec. To generate the spec:

1. To update the api file, run
    ```
    npm run generate-api
    ```
    See `openapi-config.json` for path information.
2. Commit the changed file.

Note: default path to api spec is localhost. To generate a new spec BE must run locally. It is possible to change path to any other swagger environment, but it is preferred to use locally running backend.

## Proxying backends

For being able to run the backend, and frontend application on the same machine (locally) a proxy has been set in the project.

If you <ins>don't want to run</ins> the backend server and frontend on the same machine, or have different settings, please adjust the [vite.config.ts](vite.config.ts) at the following line:
`target: "http://localhost:8080"`

## Tools

### Frameworks

- React
- Redux Toolkit

### Building

- Vite
- NVM (Optional)

### Linting and formatting

- ESLint
- Prettier
- Pre-commit

### Testing

- Cypress
- Storybook
