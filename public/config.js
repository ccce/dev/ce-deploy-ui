/* eslint-disable */

SERVER_ADDRESS = "";
ENVIRONMENT_TITLE = "LOCAL";
API_BASE_ENDPOINT = "/api/spec";
TOKEN_RENEW_INTERVAL = 180000;
NAMING_ADDRESS = "https://naming.esss.lu.se";
NETBOX_ADDRESS = "https://netbox.esss.lu.se";
FRONTEND_VERSION = "0.0.0-local";
SUPPORT_URL =
  "https://jira.esss.lu.se/plugins/servlet/desk/portal/44?requestGroup=137";
