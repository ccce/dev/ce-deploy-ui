Feature: Display details of an IOC deployment

  Scenario: The user wants to know the configuration the deployment was trying to enact
    Given the user is on the deployment details page
    Then they should see the ioc name
    And the ioc description
    And the ioc git repository
    And the ioc version
    And the target host

  Scenario: The user wants to know when the deployment job ran
    Given the user is on the deployment details page
    Then the user should see the deployment job start time
    And the duration of the job

  Scenario: The user wants to know the status of a finished deployment
    Given the user is on the deployment details page
    Then the user should see an icon indicating the status of the job
    And the user should see a message indicating the status of the job
    And the user should see a stepper indicating the status of the job

  Scenario: The user wants to view the AWX job output for a finished deployment
    Given the user is on the deployment details page
    And the deployment job has finshed
    Then a log of job output should be displayed

  Scenario: The user wants to observe a running deployment
    Given the user is on the deployment details page
    And the deployment job is running
    Then the status icon should update to reflect the live deployment status
    And the status message should update to reflect the live deployment status
    And the stepper should update to reflect the live deployment status
    And the job output log should update with the most recent job events

  Scenario: The user wants to navigate to the IOC the deployment concerns
    Given the user is on the deployment details page
    Then a link to the relevant IOC details page should be displayed

  Scenario: The user wants to navigate to the host the deployment concerns
    Given the user is on the deployment details page
    Then a link to the target host details page should be displayed
