Feature: Browse IOC Deployments

  Scenario: User wants to browse IOC deployments
    Given the user is not logged in and navigates to the deployments page
    Then they should not get Access deniend

#  Scenario: Logged in user wants to browse IOC deployments
#    Given the user is logged in
#    When the user navigates to the deployments page
#    Then they should be presented with a table of IOC deployments

#  Scenario: User wants to see all IOC deployments
#    Given the user is on the deployments page
#    When the user opens the Explore deployments tab
#    Then the table should display all IOC deployments in the facility

  Scenario: User wants to see relevant IOC deployments
    Given the user is on the deployments page
    When the user clicks on the My deployments toogle button
    Then the table should display only deployments which the user has made
    And recent deployments should be at the top

  Scenario: User wants to navigate to a specific IOC deployment
    Given the user is not logged in and tries to navigate any deployment details page
    Then they should not get Access deniend

#  Scenario: Logged in user wants to navigate to a specific IOC deployment
#    Given the user is on the deployments page
#    When the user clicks on the deployment in the table
#    Then the user should be redirected to the deployment details page.

#  Scenario: User wants to highlight copy values from deployments table to clipboard
#    Given the user is on the deployments page
#    When the user highlights content within the table
#    Then they shall not be redirected to the deployment details page

#  Scenario: User wants to search for a deployment
#    Given the user is on the deployments page
#    When the user types a query in the search bar
#    Then the deployments table should be updated with the search results

#  Scenario: User wants to filter the deployments
#    Given the user is on the deployments page
#    When the user types a filter into the search bar
#    Then the deployments in the table should be filtered based on the filter string.

#  Scenario: User wants change the order the IOC deployments are displayed
#    Given the user is on the deployments page
#    When the user clicks on a column header in the table
#    Then the deployments table rows should be sorted by the values of that column

#  Scenario: There are more deployments than can be displayed
#    Given the user is on the deployments page
#    When there are more deployments matching the search query than can be displayed
#    Then a warning should be shown letting the user know that all deployments are not rendered
