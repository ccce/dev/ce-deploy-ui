Feature: Display the live status of deployed IOCs

  Scenario: User wants to navigate to the live status of an IOC
    Given the user is on the IOC details page
    And the IOC has been deployed
    When the user clicks on the Live Status tab
    Then the user should see live status information about the IOC

  Scenario: User wants to understand the live status of an IOC
    Given the user is on the IOC Live Status tab
    Then the user should see the currently deployed IOC configuration
    And the status of the host
    And the status of the IOC systemd service
    And a link to open a Grafana dashboard for the host
