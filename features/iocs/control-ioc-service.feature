Feature: Start/Stop/Restart the IOC service remotely

  Scenario: User wants to navigate to IOC controls
    Given the user has opened the IOC details page
    And the IOC has been deployed
    When the user opens the Live Status page
    Then the IOC service controls should be displayed

  Scenario: User wants to start the IOC
    Given the user is viewing the IOC Live Status
    And the user has permission to make changes on the host
    When the user clicks on the Start button
    Then the IOC service on the host should be started
    And the live status of the IOC should be updated

  Scenario: User wants to stop the IOC
    Given the user is viewing the IOC Live Status
    And the user has permission to make changes on the host
    When the user clicks on the Stop button
    Then the IOC service on the host should be stopped
    And the live status of the IOC should be updated

  Scenario: User wants to restart the IOC
    Given the user is viewing the IOC Live Status
    And the user has permission to make changes on the host
    When the user clicks on the Restart button
    Then the IOC service on the host should be restarted
    And the live status of the IOC should be updated
