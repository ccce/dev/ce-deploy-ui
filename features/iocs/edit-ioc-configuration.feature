Feature: User can view and edit an IOCs configuration

  Scenario: User wants to navigate to the IOC configuration
    Given the user is on the IOC details page
    When the user clicks on the Configuration tab
    Then the user should see the current configuration of the IOC
    And they should see an edit icon in the app bar

  Scenario: User wants to understand the current IOC configuration
    Given the user has opened the IOC configuration tab
    Then they should see the ioc name
    And the ioc description
    And the target host
    And the target git repository
    And the target git version
    And a message telling them if this configuration has been deployed yet

  Scenario: User wants to open a form to edit the IOC configuration
    Given the user has opened the IOC configuration tab
    And they have permission to edit the IOC Configuration
    When they click the edit button
    Then an IOC form should appear
    And the form fields should be prepopulated with the current configuration's values

  Scenario: User want to create a new IOC configuration
    Given the user has opened the IOC configuration edit form
    And they have changed some configuration parameters
    When they submit the form
    Then a new IOC configuration should be created
    And the form should close
    And the user should see the new configuration details

  Scenario: User is viewing a new IOC configuration
    Given the user has opened the IOC configuration tab
    And the user has created a new configuration
    Then the user should see a message telling them the configuration has not been deployed yet
    And they should see a deployment button

  Scenario: User is viewing an already deployed IOC configuration
    Given the user has opened the IOC configuration tab
    And the user has already deployed the current configuration
    Then the user should see a message telling them the configuration has already been deployed
    And they should see a redeployment button

  Scenario: User wants to deploy the configuration
    Given the user has opened the IOC configuration tab
    And they have permission to deploy on the target host
    When the user clicks the deploy button
    Then a new deployment should be created
    And the user should be redirected to the deployment details page
