Feature: Browse IOCs

  Scenario: User wants to browse IOCs
    Given the user is logged in
    When the user navigates to the IOCs page
    Then they should be presented with a table of IOCs

  Scenario: User wants to see all IOCs
    Given the user is on the IOCs page
    When the user opens the Explore IOCs tab
    Then the table should display all IOCs in the facility
    And recently updated IOCs should be at the top

  Scenario: User wants to see relevant IOCs
    Given the user is on the IOCs page
    When the user clicks on the Your IOCs tab
    Then the table should display only IOCs which the user can edit
    And recently updated IOCs should be at the top

  Scenario: User wants to navigate to a specific IOC
    Given the user is on the IOCs page
    When the user clicks (exactly) on the IOC name link
    Then the user should be redirected to the IOC Details page.

  Scenario: User wants to highlight copy values from table to clipboard
    Given the user is on the IOCs page
    When the user highlights content within the table
    Then they shall not be redirected to the IOC Details page

  Scenario: User wants to search for an IOC
    Given the user is on the IOCs page
    When the user types a query in the search bar
    Then the IOCs table should be updated with the search results

  Scenario: User wants to filter the IOCs
    Given the user is on the IOCs page
    When the user types a filter into the search bar
    Then the IOCs in the table should be filtered based on the filter string.

  Scenario: User wants change the order the IOCs are displayed
    Given the user is on the IOCs page
    When the user clicks on a column header in the table
    Then the IOC table rows should be sorted by the values of that column

  Scenario: There are more IOCs than can be displayed
    Given the user is on the IOCs page
    When there are more IOCs matching the search query than can be displayed
    Then a warning should be shown letting the user know that all IOCs are not rendered
