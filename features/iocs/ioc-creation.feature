Feature: Create IOCs

  Scenario: Logged in user opens IOC creation form
    Given the user is on the Home page
    When the user clicks the create IOC button
    Then the user should be presented with a form to create an IOC

 # Scenario: User submits IOC creation form successfully
 #   Given the user has filled out the IOC creation form
 #   And the information is valid
 #   When the user clicks on the Create IOC button
 #   Then the user should be redirected to the new IOC details page
