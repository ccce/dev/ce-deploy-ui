Feature: Display historical IOC Configurations

  Scenario: User wants to navigate to the IOC Configuration history
    Given the user is on the IOC details page
    When the user clicks on the Configuration table
    Then the IOC Configuration history should be displayed

  Scenario: User wants to roll back to an old IOC version
    Given the user is on the IOC Configuration page
    And they have permission to edit the IOC
    When they click on the restore button on a historical configuration
    Then the IOC Configuration edit form will open
    And the fields will be pre-populated with the historical values.
