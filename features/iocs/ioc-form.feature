Feature: Smart IOC form

  Scenario: User searches for host in IOC form
    Given the user has opened the IOC form
    When the user types into the target host field
    Then an autocomplete box should prompt the user with possible matches

  Scenario: User searches for git repository in IOC form
    Given the user has opened the IOC form
    When the user types into the git repository field
    Then an autocomplete box should prompt the user with possible matches

  Scenario: User searches for git version in IOC form
    Given the user has opended the IOC from
    And populated the git repository field with a valid repository
    When the user types into the git version field
    Then an autocomplete box should prompt the user with possible matches

  Scenario: User searches for owners IOC form
    Given the user has opended the IOC from
    When the user types into the owners field
    Then an autocomplete box should prompt the user with possible matches
