Feature: Logging in

  Scenario: Opening application when not authenticated
    Given the user is not authenticated
    When the user opens the application
    Then login button should appear

#  Scenario: Opening application root or login when already authenticated
#    Given the user is already authenticated
#    When the user opens the application to the root or login page
#    Then the user should redirected to the IOCs list

#  Scenario: Opening application path when already authenticated
#    Given the user is already authenticated
#    When the user opens the application with a valid path in the URL
#    Then then the content at that path should be displayed without prompting for login

#  Scenario: Logging in with correct username and password
#    Given the user is on the login page
#    When the user submits a valid LDAP username and password
#    Then the user should be presented with the IOCs list

#  Scenario: Entering an incorrect username or password into Login form
#    Given the user is on the login page
#    When the user enters a username or password which are not valid
#    Then the user should see a descriptive error message
