Feature: Logging out

  Scenario: The user clicks the logout button
    Given the user is logged in
    When the user clicks the logout button
    Then they should be logged out from the application
    And they should be redirected to the login page

  Scenario: The user's session expires
    Given the user is logged in
    When the session time has elapsed
    Then the user should be logged out from the application
    And they should be redirected to the login page
