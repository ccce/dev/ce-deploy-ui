Feature: Browse IOC Hosts

  Scenario: User wants to browse IOC hosts
    Given the user is logged in
    When the user navigates to the hosts page
    Then they should be presented with a table of hosts from the iocs group in NetBox

#  Scenario: User wants to see all IOC Hosts
#    Given the user is on the hosts page
#    When the user opens the Explore Hosts tab
#    Then the table should display all IOC hosts in the facility

#  Scenario: User wants to see relevant IOC Hosts
#    Given the user is on the hosts page
#    When the user clicks on the Your Hosts tab
#    Then the table should display only hosts which the user can edit
#    And recently updated hosts should be at the top

  Scenario: User wants to navigate to a specific IOC Host
    Given the user is on the hosts page
    When the user clicks (exactly) on the host url link
    Then the user should be redirected to the Host Details page

#  Scenario: User wants to highlight copy values from host table to clipboard
#    Given the user is on the hosts page
#    When the user highlights content within the table
#    Then they shall not be redirected to the host details page

  Scenario: User wants to search for an IOC host
    Given the user is on the hosts page
    When the user types a query in the search bar
    Then the hosts table should be updated with the search results

#  Scenario: User wants to filter the IOC host
#    Given the user is on the hosts page
#    When the user types a filter into the search bar
#    Then the hosts in the table should be filtered based on the filter string.

#  Scenario: User wants change the order the IOC hosts are displayed
#    Given the user is on the hosts page
#    When the user clicks on a column header in the table
#    Then the host table rows should be sorted by the values of that column

  Scenario: There are more hosts than can be displayed
    Given the user is on the hosts page
    When there are more hosts matching the search query than can be displayed
    Then a warning should be shown letting the user know that all hosts are not rendered
