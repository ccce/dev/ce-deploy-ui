Feature: Display details of an IOC host

  Scenario: The user wants to see details about the host
    Given the user is on the host details page
    Then they should see the fqdn of the host
    And the name of the host
    And the description of the host
    And the owner of the host
    And the network scope of the Host
    And the network of the host's primary interface
    And the interfaces of the Host
    And the device type of the host
    And when the host was created
    And the ansible groups the host belongs to
    And the ansible host variables
    And a link to open the host's page in NetBox

  Scenario: The user wants to see the live status of the host
    Given the user is on the host details page
    Then they should see a status icon indicating if the host is reachable
    And a text description of the host's status
    And a link to open up a Grafana dashboard for the host

  Scenario: The user wants to see which IOCs are deployed on the host
    Given the user is on the host details page
    Then the IOCs deployed on the host should be displayed in a table

  Scenario: The user wants to see which deployments have run against this host
    Given the user is on the host details page
    Then a list of deployments targeting this host should be displayed
