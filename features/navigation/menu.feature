Feature: Navigation menu

  Scenario: User opens the navigation menu
    Given the user is logged in
    When they click on the menu icon
    Then they will see their user information
    And a link to browse IOCs
    And a link to browse hosts
    And a link to browse deployments
    And a link to logout of the application
