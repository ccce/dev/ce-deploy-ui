Feature: Intuitive Navigation

  Scenario: User navigates between internal pages
    Given the user has opened the application
    When they click on an internal link
    Then the selected content shall be loaded
    And the application source shall not be reloaded

  Scenario: User clicks on link to external page
    Given the user has opened the application
    When they click on a link to an external site
    Then the content will open in a new tab or window

  Scenario: User clicks the browser back button
    Given the user has navigated within the application
    When they click the browser's back button
    Then they shall be taken to the previous internal page they visited
    And the application source shall not be reloaded

  Scenario: User clicks the browser refresh button
    Given the user has opened the application
    When they click the browser refresh button
    Then the application source shall be reloaded
    And the user will be returned to the same page
