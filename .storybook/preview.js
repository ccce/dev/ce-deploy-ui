import { initialize, mswLoader } from "msw-storybook-addon";
import { handlers } from "../src/mocks/handlers";

// Initialize MSW
initialize({}, handlers);

const preview = {
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/
    }
  },

  loaders: [mswLoader],
  tags: ["autodocs"],
  msw: {
    handlers: handlers
  }
};

export default preview;
