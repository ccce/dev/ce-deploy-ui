// eslint-disable-next-line import/no-unresolved
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import svgr from "vite-plugin-svgr";

export default defineConfig({
  server: {
    proxy: {
      "/api": {
        target: "http://localhost:8080",
        changeOrigin: true,
        secure: false
      }
    },
    port: 3000,
    open: true
  },
  build: {
    outDir: "build",
    emptyOutDir: true
  },
  plugins: [svgr(), react()]
});
