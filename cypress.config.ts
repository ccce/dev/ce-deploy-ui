import { defineConfig } from "cypress";

export default defineConfig({
  fixturesFolder: "src/mocks/fixtures",
  viewportWidth: 1280,
  viewportHeight: 720,

  e2e: {
    specPattern: "cypress/e2e/**/*.{feature,features}"
  },

  component: {
    setupNodeEvents() {},
    specPattern: "src/**/*.spec.{js,jsx,ts,tsx}",
    devServer: {
      framework: "react",
      bundler: "vite"
    }
  }
});
